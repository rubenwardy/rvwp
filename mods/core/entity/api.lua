local api = rvwp.entity
api.actions = {}

local id_to_ctr = {}
local id_to_action = {}

local function get_entity_controller(entity)
	local ctr = id_to_ctr[entity:get_id()]
	return ctr
end

local function get_entity_action(entity)
	local ctr = id_to_action[entity:get_id()]
	return ctr
end

local function set_entity_action(entity, action)
	id_to_action[entity:get_id()] = action
	return action
end

EntityController = class()

function EntityController:get_action()
	return get_entity_action(self.entity)
end

function EntityController:set_action(action, ...)
	if action and type(action) == "function" then
		action = action(self.entity, ...)
	end
	return set_entity_action(self.entity, action)
end

function EntityController:set_action_if_not_set(action, ...)
	if action and type(action) == "function" then
		action = action(self.entity, ...)
	end

	local current_action = get_entity_action(self.entity)
	if current_action and current_action:is_same_action(action) then
		return current_action
	end

	return set_entity_action(self.entity, action)
end

api.registered = {}
function api.register(name, ctr)
	assert(not api.registered[name], "Entity already registered")
	assert(isDerivedFrom(ctr, EntityController))

	ctr.name = name
	api.registered[name] = ctr
end

function core.init_entity(entity)
	local typename = entity:get_type()
	local ctr = api.registered[typename]:new()
	ctr.entity = entity

	id_to_ctr[entity:get_id()] = ctr

	ctr:on_create()

	return false
end

function core.update_entity(entity, dtime)
	local ctr = get_entity_controller(entity)
	if not ctr then
		core.init_entity(entity)
		ctr = get_entity_controller(entity)
	end

	local action = get_entity_action(entity)
	if action and action.step then
		action:step(dtime)
	end

	if ctr.step then
		ctr:step(dtime)
	end

	if action and action:is_complete() then
		set_entity_action(entity, nil)
	end

	return false
end
