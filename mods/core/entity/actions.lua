local api = rvwp.entity
api.actions = {}


local EntityAction = class(EntityController)

function EntityAction:constructor(type)
	self.type = type
end

--- Ignoring the target entity, is this the same action?
function EntityAction:is_same_action(other)
	error("Not implemented")
end


--
-- FollowPath
--

local FollowPath = class(EntityAction)

function FollowPath:constructor(entity, type)
	assert(type)
	EntityAction.constructor(self, type)

	self.entity = entity
end

function FollowPath:is_same_action(other)
	error("Unimplemented")
end

function FollowPath:get_affective_target()
	error("Unimplemented")
end

function FollowPath:is_complete()
	if self.failed then
		return true
	end

	local dist = self.entity:get_pos():sqdist(self:get_affective_target())
	return dist < 0.1 or (dist < 1 and self.path and self.path_i > #self.path)
end

function FollowPath:is_successful()
	return not self.failed
end

function FollowPath:calculate_path()
	error("Unimplemented")
end

function FollowPath:step(dtime)
	if not self.path then
		self:calculate_path()

		if not self.path then
			return
		end
	end

	local from = self.entity:get_pos()
	local next = self.path[self.path_i]
	if not next then
		self.entity:set_pos(self:get_affective_target())
		return
	end

	if rvwp.debug and rvwp.debug:is_enabled() then
		rvwp.debug:draw_line(from, next, "fff")
--		for i=self.path_i + 1, #self.path do
--			rvwp.debug:draw_line(self.path[i - 1], self.path[i], "999")
--		end
	end

	local delta = next - from

	local needs_jump = false
	if delta.z > 0 then
		needs_jump = true
		delta.z = 0
	end

	-- If nearing next node, select next node
	local distance = delta:sqlen()
	if not needs_jump and distance < 0.5 and self.path_i < #self.path then
		self.path_i = self.path_i + 1
		self:step(dtime)
		return
	end

	-- If over-jump, find next node in the path
	local step_size = 3 * dtime
	if distance < step_size then
		self.path_i = self.path_i + 1
		return
	end

	local step = delta:normalize() * step_size
	self.entity:set_yaw(step:yaw())

	-- Open doors
	local tile = rvwp.get_tile(from + step)
	local def = tile and rvwp.registered_tiles[tile.name]
	if def and def.collides and def.is_door then
		def.on_interact((from + step):floor(), rvwp.layers.tile, tile, self.entity)
	end

	if needs_jump and self.entity:jump(step) then
		return
	end

	self.entity:move(step)
end


--
-- GoTo
--

local GoTo = class(FollowPath)

function GoTo:constructor(entity, target, type)
	assert(type)
	FollowPath.constructor(self, entity, type)

	self:set_target(target)
end

function GoTo:is_same_action(other)
	return self.type == other.type and self.target == other.target
end

function GoTo:get_affective_target()
	return self.target
end

function GoTo:set_target(target)
	self.target = V(target)
	self.path = nil
end

function GoTo:calculate_path()
	self.path = rvwp.find_path(self.entity:get_pos(), self.target, {
		accept_neighbours = false,
		weights = {
			door = 3,
		},
	})

	if not self.path then
		print("Error! Unable to calculate path")
		self.failed = true
		return
	end

	self.failed = false

	self.path[#self.path] = self.target:copy()
	-- Skip first node as it is the current tile
	self.path_i = 2
end

function rvwp.entity.actions.go_to(entity, target)
	return GoTo:new(entity, target, "GoTo")
end



--
-- GoToNeighbour
--

local GoToNeighbour = class(GoTo)

function GoToNeighbour:constructor(entity, target, type, accept_below)
	GoTo.constructor(self, entity, target, type)

	self.accept_below = accept_below
end

function GoToNeighbour:get_affective_target()
	return self.path and self.path[#self.path] or self.target
end

function GoToNeighbour:calculate_path()
	self.path = rvwp.find_path(self.entity:get_pos(), self.target, {
		accept_neighbours = true,
		accept_exact_target = false,
		accept_below = self.accept_below,
		weights = {
			door = 3,
		},
	})

	if not self.path then
		self.failed = true
		return
	end

	self.failed = false
	self.path_i = 2
end

function rvwp.entity.actions.go_to_neighbour(entity, target, accept_below)
	return GoToNeighbour:new(entity, target, "GoToNeighbour", accept_below)
end


--
-- Follow
--

local Follow = class(GoTo)

function Follow:constructor(entity, target)
	FollowPath.constructor(self, entity, target:get_pos(), "Follow")
	self.target_entity = target
end

function Follow:is_same_action(other)
	return self.type == other.type and self.target_entity == other.target_entity
end

function Follow:is_complete()
	return self.target_entity == nil
end

function Follow:set_target_entity(target)
	self.target_entity = target
	if target then
		self:set_target(target:get_pos())
		self:calculate_path()
	end
end

function Follow:step(dtime)
	assert(self.target)
	local target = self.target_entity:get_pos()
	if target:sqdist(self.target) > 1 then
		self:set_target(target)
	end

	FollowPath.step(self, dtime)
end

function rvwp.entity.actions.follow(entity, target)
	return Follow:new(entity, target)
end
