rvwp.entity = {}

dofile("mods/core/entity/api.lua")
dofile("mods/core/entity/actions.lua")

rvwp.register_chat_command("spawn_entity", commands.ChatCommand:new({
	func = function(self, name, param)
		if param:trim() == "" then
			return false, "LuaEntity name needed"
		end

		local pos = rvwp.get_player_entity(name):get_pos()
		if rvwp.spawn_entity(pos, param) then
			return true, "Done"
		else
			return false, "Failed"
		end
	end
}))
