commands.ChatCommand = class()
local ChatCommand = commands.ChatCommand

function ChatCommand:constructor(obj)
	if not obj then
		return
	end

	for key, value in pairs(obj) do
		self[key] = value
	end
end

function ChatCommand:execute(name, param)
	-- TODO: permissions
	return self:func(name, param)
end

rvwp.registered_chat_commands = {}
function rvwp.register_chat_command(name, def)
	assert(not rvwp.registered_chat_commands[name], "chat command already exists")
	def.name = name
	assert(isDerivedFrom(def, ChatCommand))
	rvwp.registered_chat_commands[name] = def
end

rvwp.register_on_pre_chat_message(function(name, message)
	if message[1] == "/" then
		print("Chat command!")
		local idx = message:find(" ", 1, true)
		local cmd_name = message:sub(2, idx or #message):trim()
		local param = idx and message:sub(idx, #message):trim()

		local _, response = rvwp.execute_chat_command(name, cmd_name, param or "")
		if response then
			rvwp.chat_send_player(name, response)
		end

		print("Name: " .. cmd_name)
		print("param: " .. (param or "<nil>"))

		return true
	end

	return false
end)

function rvwp.execute_chat_command(name, cmd_name, param)
	local def = rvwp.registered_chat_commands[cmd_name]
	if not def then
		return false, cmd_name .. ": command not found"
	end

	return def:execute(name, param)
end
