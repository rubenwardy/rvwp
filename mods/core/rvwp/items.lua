local templates = {}
templates.tile = {
	collides = true,
	light_source = 0,
	footstep_sound = nil,
	light_propagates = false,
	friction = 0.2,
	collision_boxes = {
		{ 0, 0, 1, 1 },
	},
}
templates.floor = {
	collides = true,
	light_source = 0,
	footstep_sound = nil,
	light_propagates = false,
	friction = 0.2,
	collision_boxes = {
		{ 0, 0, 1, 1 },
	},
}
templates.terrain = templates.floor

local raw_register_item = core.register_item

rvwp.registered_items = {}
rvwp.registered_tiles = {}

function rvwp.register_item(name, def)
	if not name:match("^[a-z0-9_]+$") then
		error(name .. " doesn't follow item name convention!")
	end

	def.type = def.type or "item"
	def.name = name

	local template = templates[def.type]
	if template then
		setmetatable(def, { __index = template })
	end

	assert(not rvwp.registered_items[name])
	rvwp.registered_items[name] = def

	raw_register_item(def)
end

function rvwp.register_tile(name, def)
	def.type = def.type or "tile"

	if def.buildable then
		local select_type
		if def.type == "floor" or def.type == "terrain" then
			select_type = "filled"
		elseif def.material.type == "8bit" then
			select_type = "border"
		else
			select_type = "fixed"
		end

		rvwp.register_tool({
			type = "build",
			description = def.title,
			material = def.material,
			select_type = select_type,
			fixed_selection_size = V(1, 1, 1),
			item_name = name,
			layer = def.type,
		})
	end

	rvwp.register_item(name, def)
	rvwp.registered_tiles[name] = def
end

function rvwp.register_floor(name, def)
	def.type = def.type or "floor"
	def.collides = false

	rvwp.register_tile(name, def)
end

function rvwp.register_terrain(name, def)
	def.type = def.type or "terrain"
	def.collides = false

	rvwp.register_tile(name, def)
end

function core.on_expired_timer(pos, layer, event)
	print("A timer expired at " .. pos .. " layer " .. layer .. " event " .. event)

	local tile = rvwp.get_layer(pos, layer)
	assert(tile.name)
	local def = rvwp.registered_tiles[tile.name]
	if def then
		local cb = def["on_" .. event]
		if cb then
			cb(pos, layer, tile)
		else
			print("Error: tile doesn't have callback!")
		end
	else
		print("Error: tile fired on unknown tile type")
	end
end

function core.tile_interact(entity, pos)
	local layer = rvwp.layers.tile
	local tile = rvwp.get_tile(pos)
	local def = rvwp.registered_tiles[tile and tile.name]
	local func = def and def.on_interact
	if not func then
		return false
	end

	return def.on_interact(pos, layer, tile, entity) ~= false
end
