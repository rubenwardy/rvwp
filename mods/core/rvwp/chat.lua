rvwp.registered_on_chat_message = {}
function rvwp.register_on_chat_message(func)
	table.insert(rvwp.registered_on_chat_message, func)
end

rvwp.registered_on_pre_chat_message = {}
function rvwp.register_on_pre_chat_message(func)
	table.insert(rvwp.registered_on_pre_chat_message, func)
end

local COLORS = {
	"#cc0000",
	"#006cad",
	"#4d9900",
	"#6600cc",
	"#a67d00",
	"#009927",
	"#0030c0",
	"#cc009a",
	"#b94600",
	"#869900",
	"#149900",
	"#009960",
	"#006cad",
	"#0099cc",
	"#b300cc",
	"#cc004d",
}

function rvwp.get_nick_color(name)
	local sum = 0
	for i=1, #name do
		sum = sum + string.byte(string.sub(name, i, i)) * math.pow(10, i)
	end
	return COLORS[(sum % #COLORS) + 1]
end

function rvwp.format_chat_message(name, message)
	local color = rvwp.get_color_escape_sequence(rvwp.get_nick_color(name))
	local clear = rvwp.get_color_escape_sequence("#ffffff")
	return ("%s<%s>%s %s"):format(color, name, clear, message)
end

function core.on_chat_message(name, message)
	for _, func in pairs(rvwp.registered_on_pre_chat_message) do
		if func(name, message) then
			return
		end
	end

	for _, func in pairs(rvwp.registered_on_chat_message) do
		func(name, message)
	end

	rvwp.chat_send_all(rvwp.format_chat_message(name, message))
end

local ESCAPE_CHAR = string.char(0x1b)
function rvwp.get_color_escape_sequence(color)
	return ESCAPE_CHAR .. "(c@" .. color .. ")"
end
function rvwp.get_background_escape_sequence(color)
	return ESCAPE_CHAR .. "(b@" .. color .. ")"
end

function string:color(color)
	local color_code = rvwp.get_color_escape_sequence(color)

	local lines = tostring(self):split("\n", true)
	for i, line in ipairs(lines) do
		lines[i] = color_code .. line
	end

	return table.concat(lines, "\n") .. rvwp.get_color_escape_sequence("#ffffff")
end
