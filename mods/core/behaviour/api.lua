local node_factory = {}

function behaviour.register_factory(type, func)
	node_factory[type:lower()] = func
end

function behaviour.get_node_factory(type)
	return node_factory[type:lower()]
end

function behaviour.parse_variable(input)
	if input:sub(1, 1) == "$" then
		return input:sub(2)
	end
end

function behaviour.get_value(blackboard, input)
	assert(blackboard)

	local variable = behaviour.parse_variable(input)
	if variable then
		return blackboard[variable]
	else
		return input
	end
end

function behaviour.get_position(blackboard, input)
	assert(blackboard)

	local variable = behaviour.parse_variable(input)
	if variable then
		local value = blackboard[variable]
		return value and V(value)
	else
		return V(input) + V(0.5, 0.5, 0)
	end
end

