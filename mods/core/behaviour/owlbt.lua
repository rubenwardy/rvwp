local function convert(properties)
	local args = {}

	for i=1, #properties do
		local prop = properties[i]
		assert(prop.name and prop.value)
		args[prop.name:lower()] = prop.value
	end

	return args
end

local function construct(source, children, blackboard)
	local constructor = behaviour.get_node_factory(source.type)
	assert(constructor, "No constructor found for " .. source.type)
	local node = constructor(convert(source.properties or {}), children, blackboard)
	node.typename = node.typename or source.type
	return node
end

local function owl_to_tree(source, blackboard)
	local children = {}
	for i, child in pairs(source.childNodes or {}) do
		children[i] = owl_to_tree(child, blackboard)
	end

	-- print("##### SOURCE " .. source.type .. " #####")
	-- print(dump(source.properties))
	-- print(#children)

	local node = construct(source, children, blackboard)

	local decorators = source.decorators or {}
	for i = #decorators, 1, -1 do
		node = construct(source.decorators[i], { node }, blackboard)
	end

	return node
end

function behaviour.load_tree(ctr, path, blackboard, parent_tree)
	local obj = rvwp.read_json(path)

	blackboard = blackboard or {}

	setmetatable(blackboard, {
		__index = function(self, key)
			return ctr.entity:get_meta():get_string(key)
		end
	})

	return behaviour.BehaviourTree:new(parent_tree, owl_to_tree(obj, blackboard), ctr)
end
