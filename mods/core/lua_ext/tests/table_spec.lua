dofile("mods/core/lua_ext/table.lua")

describe("table", function()
	it("index_of", function()
		assert.equal(3, table.index_of({ 1, 2, 3, 4 }, 3))

		local x = { 0 }
		local y = { 0 }
		local z = { 0 }
		assert.equal(4, table.index_of({ 1, 2, x, y, 3, z }, y))
	end)

	it("copy", function()
		local x = {
			a = 3,
			b = {
				c = 3,
				d = 5
			}
		}

		local y = table.copy(x)
		x.a = 4
		assert.equal(3, y.a)
		x.b.c = 10
		assert.equal(3, y.b.c)
	end)
end)
