--- Find and allocate a job for the given worker
function jobs.allocate(ctr)
	local time = rvwp.get_time().time_since_beginning
	local entity_meta = ctr.entity:get_meta()
	local pos = ctr.entity:get_pos()

	local plot = rvwp.get_plot_at_pos(pos)
	if plot then
		local works = plot:get_work()
		for i=1, #works do
			local work = works[i]
			work.work_id = work.id
			work.id = nil
			work.target = work.bounds.from
			work.layer = work.layer or rvwp.registered_items[work.item_name].type
			work.work_needed = 100
			work.accept_below = work.layer == rvwp.layers.floor

			local last_fail = entity_meta:get_float("work_fail_" .. work.work_id)
			if (not last_fail or time >= last_fail + 40) and
					jobs.can_access(work.target, ctr.entity) then
				return {
					class = work.type,
					arguments = work,
				}
			end
		end
	end

	local poses, tiles = rvwp.find_with_metadata(pos, 50, rvwp.layers.tile, "job_giver")
	if #poses > 0 and jobs.can_access(poses[1], ctr.entity) then
		assert(tiles[1].name == "stove")

		local node_meta = rvwp.get_meta(poses[1], rvwp.layers.tile)
		assert(node_meta)
		node_meta:set_int("work_progress", 0)
		node_meta:set_int("work_needed", 0)

		return {
			class = "cook",
			arguments = {
				target = poses[1],
				work_needed = 100,
			}
		}
	end
end

function jobs.failed(ctr, job)
	assert(job)
	if job.arguments.work_id then
		local time = rvwp.get_time().time_since_beginning
		local meta = ctr.entity:get_meta()
		meta:set_float("work_fail_" .. job.arguments.work_id, time)
	end
end
