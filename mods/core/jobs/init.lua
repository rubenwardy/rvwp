jobs = {}

dofile("mods/core/jobs/lock.lua")
dofile("mods/core/jobs/api.lua")
dofile("mods/core/jobs/behaviours.lua")


rvwp.register_tile("blueprint", {
	title = "Blueprint",
	material = {
		x = 5, y = 2,
	},
	collides = false,
	light_propagates = true,
})


rvwp.register_tile("stove", {
	title = "Stove",
	material = {
		x = 0, y = 11,
	},
	collides = true,
	light_propagates = true,
	buildable = true,

	on_cook = function(pos, layer, _)
		rvwp.chat_send_all("Stove cooked something!")

		local timer = rvwp.get_timer(pos, layer, "cook")
		assert(timer:start(5))
	end,
})

rvwp.register_tile("sink", {
	title = "Sink",
	material = {
		x = 1, y = 11,
	},
	collides = true,
	light_propagates = true,
	buildable = true,
})

rvwp.register_tile("counter", {
	title = "Counter",
	material = {
		x = 2, y = 11,
	},
	collides = true,
	light_propagates = true,
	buildable = true,
})
