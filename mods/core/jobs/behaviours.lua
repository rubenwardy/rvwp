local get_value = behaviour.get_value
local get_position = behaviour.get_position
local states = behaviour.states


local LockPosition = class(behaviour.Node)
behaviour.LockPosition = LockPosition

function LockPosition:constructor(children, target, blackboard)
	self.children = children
	self.target = target
	self.blackboard = blackboard
end

function LockPosition:on_start()
	local target = get_position(self.blackboard, self.target)
	if not target then
		return false
	end

	jobs.reserve(target, self.object.entity)
end

function LockPosition:on_step(...)
	return self.children[1]:run(...)
end

function LockPosition:on_finish()
	local target = assert(get_position(self.blackboard, self.target))
	jobs.unreserve(target, self.object.entity)
end

behaviour.register_factory("LockPosition", function(args, children, blackboard)
	return LockPosition:new(children, args.target, blackboard)
end)



behaviour.make_node("FindWork", function(self, dtime)
	assert(#self.children == 0)

	--- Find job to run.
	--- Will set self.bt if successful
	---
	--- @returns true for success, false for failure
	local function find_job()
		assert(not self.bt)

		self.job = jobs.allocate(self.object)
		if not self.job then
			return false
		end

		local path = "mods/core/behaviour/trees/" .. self.job.class .. ".json"

		assert(self.job.class)
		print("Loading subtree from " .. path)

		local new_blackboard = table.copy(self.job.arguments or {})
		new_blackboard.job_class = self.job.class

		self.bt = behaviour.load_tree(self.object, path, new_blackboard, self.tree)
		return self.bt ~= nil
	end

	if not self.bt and not find_job() then
		print("No jobs available")
		return states.FAILED
	end

	self.bt:run()

	if self.bt.state == behaviour.states.SUCCESS then
		self.bt = nil
		if find_job() then
			return self:on_step(dtime)
		else
			return states.SUCCESS
		end
	elseif self.bt.state == behaviour.states.FAILED then
		jobs.failed(self.object, self.job)
		self.bt = nil
		self.job = nil
		return states.FAILED
	else
		return states.RUNNING
	end
end)



local Work = class(behaviour.Node)
behaviour.Work = Work

function Work:constructor(blackboard, target, layer, work_needed)
	self.blackboard = blackboard

	self.target = target
	self.layer = layer
	self.work_needed = work_needed
end

function Work:on_start()
	print("Starting work")
	local target = get_position(self.blackboard, self.target)
	local layer = get_value(self.blackboard, self.layer)
	local work_needed = get_value(self.blackboard, self.work_needed)
	local meta = rvwp.get_meta(target, layer)
	local current_time = rvwp.get_time().time_since_beginning

	assert(target and work_needed)

	meta:set_float("work_progress", 0)
	meta:set_float("work_last_time", current_time)
	meta:set_float("work_needed", work_needed)
end

function Work:on_step()
	local target = get_position(self.blackboard, self.target)
	local layer = get_value(self.blackboard, self.layer)
	local pos = self.object.entity:get_pos()
	if pos:floor() == target:floor() or not target or pos:sqdist(target) >= 4 then
		print("Failed work")
		return states.FAILED
	end

	self.object.entity:set_yaw((target - pos + V(0.5, 0.5, 0)):yaw())

	local meta = rvwp.get_meta(target, layer)

	local current_time = rvwp.get_time().time_since_beginning
	local delta = current_time - meta:get_float("work_last_time")

	local progress = meta:get_float("work_progress")
	progress = progress + delta
	meta:set_float("work_progress", progress)

	if progress > meta:get_float("work_needed") then
		return states.SUCCESS
	else
		print("Working on " .. target .. ", delta=" .. delta .. ", progress=" .. progress)
		return states.RUNNING
	end
end

behaviour.register_factory("Work", function(args, children, blackboard)
	return Work:new(blackboard, args.target, args.layer, args.workneeded)
end)


behaviour.make_node("PlaceBlueprint", function(self)
	local tile_name = assert(self.blackboard.item_name)
	local target = assert(get_position(self.blackboard, self.args.target))
	local layer = assert(get_value(self.blackboard, self.args.layer))

	local pos = self.object.entity:get_pos()
	if not target or pos:sqdist(target) >= 4 then
		print("Failed PlaceBlueprint")
		return states.FAILED
	end

	self.object.entity:set_yaw((target - pos + V(0.5, 0.5, 0)):yaw())

	rvwp.set_layer(target, layer, {
		name = "blueprint"
	})

	local meta = rvwp.get_meta(target, layer)
	meta:set_string("material", tile_name)

	return states.SUCCESS
end)


behaviour.make_node("PlaceBuilding", function(self)
	local tile_name = assert(self.blackboard.item_name)
	local target = assert(get_position(self.blackboard, self.args.target))
	local layer = assert(get_value(self.blackboard, self.args.layer))

	local pos = self.object.entity:get_pos()
	if not target or pos:sqdist(target) >= 4 then
		print("Failed PlaceBuilding")
		return states.FAILED
	end

	self.object.entity:set_yaw((target - pos + V(0.5, 0.5, 0)):yaw())

	rvwp.set_layer(target, layer, {
		name = tile_name
	})

	return states.SUCCESS
end)

behaviour.make_node("DeconstructTile", function(self)
	local target = assert(get_position(self.blackboard, self.args.target))
	local layer = assert(get_value(self.blackboard, self.args.layer))

	local pos = self.object.entity:get_pos()
	if not target or pos:sqdist(target) >= 4 then
		print("Failed DeconstructTile")
		return states.FAILED
	end

	self.object.entity:set_yaw((target - pos + V(0.5, 0.5, 0)):yaw())

	rvwp.set_layer(target, layer, {
		name = nil
	})

	return states.SUCCESS
end)


behaviour.make_node("RemoveWork", function(self)
	local work_id = assert(get_value(self.blackboard, self.args.workid))
	rvwp.remove_work(work_id)
	return states.SUCCESS
end)
