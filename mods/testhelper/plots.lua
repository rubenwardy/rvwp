local ChatCommand = commands.ChatCommand

rvwp.register_chat_command("plot", ChatCommand:new({
	func = function(self, name, param)
		local ent = rvwp.get_player_entity(name)
		local pos = ent:get_pos()
		local plot = rvwp.get_plot_at_pos(pos)
		if not plot then
			return false, "No plot at current location"
		end

		local box = plot:get_boxes()[1]
		local works = plot:get_work()

		local function f(work, i)
			local value = ("[%s/%d/%d,%d,%d/%s]"):format(work.type, work.id,
					work.bounds.from.x, work.bounds.from.y, work.bounds.from.z,
					work.item_name)

			if (i - 1) % 7 == 0 then
				value = "\n     " .. value
			end

			return value
		end

		local lines = {
			"Plot " .. plot:get_name(),
			box.from .. " size " .. box.size,
			"Work: " .. table.concat(table.map(works, f), ", ")
		}

		return true, table.concat(lines, "\n")
	end
}))

rvwp.register_chat_command("remove_work", ChatCommand:new({
	func = function(self, name, param)
		local ent = rvwp.get_player_entity(name)
		local pos = ent:get_pos()
		local plot = rvwp.get_plot_at_pos(pos)
		if not plot then
			return false, "No plot at current location"
		end

		local works = plot:get_work()
		for _, work in pairs(works) do
			assert(plot:remove_work(work.id))
		end

		return true, "Removed work"
	end
}))

rvwp.register_chat_command("new_plot", ChatCommand:new({
	func = function(self, pname, param)
		local name, pos1, pos2 = string.match(param, "^([%a%d_-]+) ([%-0-9,]+) ([%-0-9,]+)$")
		if not name then
			return false, "Usage: /new_plos name 0,0,0 1,1,1"
		end

		local plot = rvwp.create_plot({
			name = name,
			from = pos1,
			size = pos2 - pos1 + V(1, 1, 1),
		})

		if plot then
			return true, "Created plot " .. plot:get_name()
		else
			return false, "Unable to create plot"
		end
	end
}))

rvwp.register_chat_command("extend_plot", ChatCommand:new({
	func = function(self, name, param)
		local size = string.match(param, "^([%-0-9,]+)$")
		if not size then
			return false, "Usage: /extend_plot 1,1,1"
		end

		local ent  = rvwp.get_player_entity(name)
		local pos  = ent:get_pos()
		local plot = rvwp.get_plot_at_pos(pos)
		if not plot then
			return false, "Not standing in a plot!"
		end

		plot:add_box({
			from = pos,
			size = size,
		})

		return true, "Extended plot by " .. size
	end
}))
