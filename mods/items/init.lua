rvwp.register_tool({
	type = "cancel",
	description = "Cancel",
	material = "cancel.png",
	select_type = "filled",
})

rvwp.register_tool({
	type = "deconstruct",
	description = "Deconstruct",
	material = "deconstruct.png",
	select_type = "filled",
	layer = rvwp.layers.tile,
})

rvwp.register_tool({
	type = "deconstruct",
	description = "Remove Floor",
	material = "remove_floor.png",
	select_type = "filled",
	layer = rvwp.layers.floor,
})

rvwp.register_tile("wall", {
	title = "Wall",
	material = {
		x = 0, y = 3,
		type = "8bit",
	},
	collides = true,
	buildable = true,
})

rvwp.register_floor("floor_wood", {
	title = "Wooden Floor",
	material = {
		x = 1, y = 1,
	},
	footstep_sound = "wood_footstep",
	friction = 0.15,
	buildable = true,
})

rvwp.register_floor("floor_tiled", {
	title = "Tiled Floor",
	material = {
		x = 5, y = 0,
	},
	footstep_sound = "hard_footstep",
	friction = 0.12,
	buildable = true,
})

rvwp.register_tile("stone", {
	title = "Stone",
	material = {
		x = 4, y = 0,
	},
	collides = true,
})

rvwp.register_tile("dirt", {
	title = "Dirt",
	material = {
		x = 2, y = 0,
	},
	collides = false,
	footstep_sound = "dirt_footstep",
})

rvwp.register_tile("dirtwall", {
	title = "Dirt Wall",
	material = {
		x = 3, y = 0,
	},
	collides = true,
	footstep_sound = "dirt_footstep",
})

rvwp.register_terrain("grass", {
	title = "Grass",
	material = {
		x = 1, y = 0,
	},
	footstep_sound = "grass_footstep",
})

rvwp.register_tile("door", {
	title = "Door",
	material = {
		x = 2, y = 1,
		type = "doorlike",
	},
	collides = true,
	buildable = true,

	is_door = true,

	on_interact = function(pos, layer, tile, entity)
		tile.name = "door_open"
		rvwp.set_layer(pos, layer, tile)
	end,
})

rvwp.register_tile("door_open", {
	title = "Open Door",
	material = {
		x = 2, y = 2,
		type = "doorlike",
	},
	collides = false,
	light_propagates = true,

	is_door = true,

	on_interact = function(pos, layer, tile, entity)
		tile.name = "door"
		rvwp.set_layer(pos, layer, tile)
		return true
	end,
})

rvwp.register_tile("window", {
	title = "Window",
	material = {
		x = 4, y = 1,
		type = "doorlike",
	},
	collides = true,
	light_propagates = true,
	buildable = true,
})

rvwp.register_tile("torch", {
	title = "Torch",
	material = {
		x = 1, y = 2,
	},
	light_source = 0xC,
	collides = false,
	buildable = true,
	collision_boxes = {
		{ 0.4, 0.2, 0.65, 0.9 },
	}
})

rvwp.register_tile("sunlamp", {
	title = "Sunlamp",
	material = {
		x = 4, y = 2,
	},
	light_source = 0xF,
	collides = false,
	buildable = true,
})

rvwp.register_item("sword", {
	title = "Sword",
	material = "sword.png",

	-- Weapon specification
	weapon = {
		-- melee or gun
		type = "melee",

		-- Maximum range
		range = 3,

		-- Base damage
		damage = 16,

		-- Time between uses
		use_interval = 1,
	},
})

rvwp.register_item("pistol", {
	title = "Pistol",
	material = "pistol.png",

	-- Weapon specification
	weapon = {
		-- melee or gun
		type = "gun",

		-- Maximum range
		range = 10,

		-- Base damage
		damage = 10,

		-- Time between uses
		use_interval = 1,
	},
})
