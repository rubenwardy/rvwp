unused_args = false
allow_defined_top = true

exclude_files = {
	"winbuild",
	"**/libs/**/*"
}

globals = {
	math   = {fields={"hypot", "sign", "round"}},
	string = {fields={"trim", "split", "color", "is_yes"}},
	table  = {fields={"filter", "copy", "index_of", "map", "keys", "values", "enum"}},
	"rvwp", "core",
	"Vector",
}

read_globals = {
	"ChatCommand",
	"isDerivedFrom",
	"LuaEntity",
	"assert",
}
