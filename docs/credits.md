# Credits

## Resources

* GUI skin - nanogui - BSD 3-clause
* GUI icons - Entypo - CC BY-SA 4.0
* Tilesheet - Kenney - CC0
* Roboto
* Footstep sounds - Minetest - CC BY SA 4.0
* icon_build / icon_order - GreenXenith

## Source Code

* [SFML](https://github.com/SFML/SFML) - zlib
  * OpenAL-Soft - LGPL
  * stb_image and stb_image_write - public domain
  * freetype - FreeType or GPL
  * libogg - BSD license
  * libvorbis - BSD license
  * libflac - BSD license
* [TGUI](https://github.com/texus/tgui) - zlib
* [Lua](https://www.lua.org/) - MIT
* [Sol](https://github.com/ThePhD/sol2) - MIT
* [Thor](https://bromeon.ch/libraries/thor/) - zlib
* [Enet](http://enet.bespin.org/) - MIT
* [JSONCPP](https://github.com/open-source-parsers/jsoncpp) - MIT
* [Loguru](https://github.com/emilk/loguru) - public domain
* [SimplexNoise](https://github.com/SRombauts/SimplexNoise/blob/master/src/SimplexNoise.cpp) - MIT
* [RichText](https://github.com/skyrpex/RichText) - MIT
