# Actor AI

## What is an Actor?

An actor is an NPC or animal that has a behaviour tree.


## Behaviour trees

### Fickle or Stable?

A fickle node will reassess its children every time to determine which child should be running.
A stable node will remember the last running child node, and keep running it.

A fickle node is useful when you want to interrupt lower priority tasks.

### Node Types

* Sequential - run children in order

### Work Manager

The work manager allows pawns to query for available jobs by type and position.


## Jobs

Work is something that needs to be done.
A Work Order is work that was explicitly requested by a player.
Work can also be obtained by work-giving tiles, or from mods.

The Job Manager looks for work, work orders, and work-giving tiles.


## Decisions

### Planning

Q: How to prevent non-builder actors from collecting resources and then giving up?

   If hauling is low priority, then builder actors could end up picking up the build task even when it has prerequisites.
   Maybe a tree structure is better than having dependencies?

   Perhaps jobs should be able to create other jobs on_assign? This could help with this, but would mean that hauler
   actors wouldn't be able to bring materials when no builder is assigned.

Jobs should consist of multiple actions, ie: clear area, move to position, work on thing, etc, rather than always
using dependencies. Dependencies should be done by spawning new jobs to be assigned again, the actor that finished the
last job getting the first choice simply by being available at that time.

* Command for building a wall in empty space. Two jobs are created - gather resources, build wall.
* Command for building a wall in a space which has an item. Three jobs are created - clear space, gather resources, build wall.
* Command for building a wall with not enough resources. Should be stuck on gather resources I guess?

Superseded to use behaviour tree
