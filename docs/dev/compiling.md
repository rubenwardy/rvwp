# Compiling


## Linux

1. Open a terminal in the RVWP source location.
2. First, install some system dependencies:

   ```bash
   # Ubuntu / Debian
   sudo apt-get install build-essential tar curl zip unzip python \
       libluajit-5.1-dev libx11-dev libxrandr-dev \
       libxi-dev libudev-dev libgl1-mesa-dev
   
   # Install PyTexturePacker package
   python3 -mvenv venv
   source venv/bin/activate
   pip install PyTexturePacker
   ```
   
3. Next, install C++ dependencies: 

   ```bash 
   ./utils/vcpkg-linux.sh   
   ```

4. Finally, compile RVWP:

   ```bash    
   mkdir build && pushd build
   cmake .. -DCMAKE_TOOLCHAIN_FILE=./vcpkg/scripts/buildsystems/vcpkg.cmake
   make -j$(nproc)
   popd
   ```

5. Run: `./bin/rvwp`


## Windows

1. Install:
   1. [Git](https://git-scm.com/) (with Git Bash).
   2. An up-to-date copy of [Visual Studio 2019](https://visualstudio.microsoft.com/vs/).
   3. [Python 3](https://www.python.org/), with PIP.
      * Make sure to select "Add to environment".
2. Install PyTexturePacker from PIP.
3. Clone RVWP
4. Open Git Bash/Powershell/CMD in the RVWP source location and run:

   ```bash
   ./utils/vcpkg-windows.bat
   ```
5. Open RVWP in Visual Studio. Select x64-Debug or x64-Release, and build.
6. Debug from Visual Studio:
   1. Select rvwp.exe as the startup target.
   2. Debug > Debug and Launch settings. In the configurations,
      add `"currentDir": "${workspaceRoot}".
   3. Debug > Start debugging.

## MacOS

GLHF
