# Gameplay

This document is about the higher level gameplay specification

## In Words

* Form a gang
    * NPCs protect the base and live. What happens when offline?
* Top down city base builder
    * Make bases out of walls, floors, different levels
    * Doors, workshops
* Hunted by police
* Form smalls bands of NPCs

## Notes

How do players interact with each other?
Does this require players to be serious?
Should it cater to singleplayer?

Would be nice to have NPC base dwellers.
Players getting together could be problematic - how to stop griefers

No one should own NPCs, they should loyalties instead.
If another player tries to enter the base when no one they know is online,
they should act aggressively.
If another player enters with the owner, they shouldn't act aggressively.
They should probably be overly protective of supplies though.
Should NPCs be able to collect supplies? Maybe not.
Maybe if you run out of food they'll leave.

How to model street people?
Is every person an actual person?
Everything server side would make this slow.

How to efficiently program this?

This is getting very complicated.
