# Architecture

RVWP uses a fun blend of various design patterns.

## Components and Controllers

Behaviour is defined in a number of Controllers. All controllers must implement an `update(dtime)` function,
and can be enabled and disabled. Controllers can have children, which will be disabled if any parent
controllers are disabled.  

A controller may take a model object which it controls - for example, the HeroEntityController takes the player's entity.

A View is a subtype of Controller which supports drawing.

## Communication and Decoupling

RVWP implements the observer pattern in a decoupled way by using dedicated event buses.
An event bus should only have a single publisher, but may have a large number of subscribers.
Event buses are buffered, making events non-blocking.

## Input

The IInput interface provides a unified way of obtaining input information, acting as a level of abstraction.
Users can bind to certain input Actions, or poll for continuous or vector information.

The IInput interface is implemented by the InputController.

The InputController does not actually know about any input methods, but instead queries a list of InputResolvers
to obtain the information. IInputResolver is an interface which is implemented by *KeyboardMouseResolver* and *GamepadResolver*.
These resolvers are dependency-injected into the InputController using the `addResolver` method. 

# Decisions

## 001 - Should controllers talk directly to Views?

- How should the HeroController know which tile is to be selected?
- How should the ArchitectView and ArchitectController communicate?  

Controllers which are not directly related should use events and interfaces when
communicating with each-other, with the former being preferred. Controllers
with are related should use dependency injection to gain access to the other controller.

## 002 - How should navigation be done?

*Use events for now, defer design for later*

