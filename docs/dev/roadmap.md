# TODO List and Roadmap

## At Some Point

* Raycasting player collision
* Cheat prevention
* Network reconnection
* Ping timeout leaves stray entity
* Fix world->onEmerge

## Milestone 8 - Gameplay 2

* More advanced script API
* Jobs, plots, and item storage
* Input bindings
* Finished lightings

## Milestone 9 - Better server

* Ring buffer for player actions
* Server side shot checking
* Set tile checking
* Player move checking

## Milestone 10 - Persistence

* Database storage
    * World
* Player profile
    * Passwords
    * Store in database
    * Last login time.

## Milestone V - NPCs and Vehicles

* Pedestrians
* Cars
    * Player attachments
    * PlayerController controlling different things

## Milestone W - Extended tile types

* 8 splice
* Wall hanging

## Milestone X - Power, electronics and water

* Electronics and power
* Water flow
* Item pipes

## Milestone Y - Awards and Statistics

* Player stats:
    * Total time online.
    * Scores, etc.

## Milestone Z - Modding and serverside content

* Server sending of content
