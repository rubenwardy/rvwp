---
title: Materials
layout: default
nav_order: 10
---

# Materials

A material is the specification of how something should be drawn.

## Fields

```lua
{
    -- static, 8bit, or doorlike
    type = "static",
    
    -- source texture file
    texture = "image.png",

    -- Sprite sheet start positions
    x = 1,  y = 2,

    -- Width and height
    w = 64, h = 64,

    -- Length / frames
    l = 1,
}
```

## Examples

### Simple texture

```lua
{
    texture = "hello.png"
}
```

You can also use just the string `"hello.png"` as a shorthand for a single texture.

### Spritesheet

```lua
{
    texture = "my_spritesheet.png",
    x = 2,  y = 3,
    
    -- Spritesheet cell sizes
    w = 64, h = 64,
}
```

The default texture is `tileset.diffuse.png`, and you can use it like so:

```lua
{
    x = 1, y = 3,
}
```
