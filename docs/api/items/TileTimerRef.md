---
title: TileTimerRef
layout: default
nav_order: 10
---

# TileTimerRef

TileTimers allow you to trigger events on tiles after a duration.
Each tile may have more than one active timer present, providing that the timers
are for different events.

When a tile timer triggers, `on_EVENT` will be called on the tile's definition,
where EVENT is the event of the timer. For example, a `grow` event will cause
`on_grow(pos, layer, tile)` to be called.

Timers can only be created on loaded chunks, and will only be triggered on
loaded but active chunks. If the chunk is inactive when the timer is due to be
triggered, then the timer will be triggered the next time the chunk loads.

Obtained by `rvwp.get_timer(pos, layer, event)`.

* `start(delay)`
  * Start event to run after `delay` seconds.
  * Returns true on success, false if there is already a timer of the same type
    that will trigger sooner, or if the chunk isn't loaded.

Example:

```lua
rvwp.register_tile("stove", {
    on_cook = function(pos, layer, tile)
        rvwp.chat_send_all("Stove cooked something!")

        local timer = rvwp.get_timer(pos, layer, "cook")
        assert(timer:start(5))
    end,
})


--- somewhere else
local function start(pos)
    local timer = rvwp.get_timer(pos, rvwp.layers.tile, "cook")
    assert(timer:start(5))
end
```
