---
title: Lua Improvements
layout: default
nav_order: 10
---

# Lua Improvements

Custom extensions and improvements to Lua's standard library.

## Math

* `math.hypot`
* `math.sign(number, tolerance)`
    * 1 if > `tolerance`
    * -1 if < `-tolerance`
    * 0 otherwise
* `math.round`

## Object

* `class(constructor)`
    * Create a class with an optional constructor.
    * `constructor(self, ...)` where `...` is the arguments given to `Class:new()`
    * Example:
        
      ```lua
      MyClass = class(function(self, a, b)
              self.a = a
              self.b = b
          end)
      
      local obj = MyClass:new(1, 2)
      ``` 
      
* `isDerivedFrom(a, b)` - returns true if `b` is somewhere in `a` metatable chain.
                          For example, `isDerivedFrom(ChatCommand:new(), ChatCommand) == true`.
* `basic_dump(o)` - returns a string representing o. Tables are not recursed into.
* `dump(o)` - can return a debug string of any object, except userdata or functions. Singleline
* `dump_multiline(o)` - similar to above, but each value in a table is on a new line.

## String

* `string[i]` - access individual character.
* `string:trim()` - removes whitespace from beginning and end of string.
* `string:split(separator, include_empty, max_splits, sep_is_pattern)`
    * splits string on 'separator', returns table.
    * Separator defaults to `,` if nil.
    * `include_empty` - true to allow empty strings in returned table.
    * `max_splits` - max number of elements to be returned before stopping.
    * `sep_is_pattern` - true if `separator` is a Lua pattern
* `string:color(color)` - returns a colored string, where color is a valid Color
* `string:is_yes()` - converts string to boolean.

## Table

* `table.copy(t)` - deep copy table.
* `table.filter(t, func)` - For numeric tables. Filter table by func.
    * `func` - Takes `value`, returns boolean.
* `table.index_of(t, val)` - find index of `val` in `t`.
* `table.map(t, func)` - Map table using map function.
    * `func` - Takes `value`, returns new value.
* `table.keys(t)` - returns numeric table of keys in `t`.
* `table.values(t)` - returns numeric table of values in `t`.
* `table.enum(t)` - sets enum metatable on `t`, which raises an error on invalid access.
