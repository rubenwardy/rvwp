---
title: Introduction
layout: default
nav_order: 0
---

# Introduction

This documents the current API for RVWP. Note that the API may,
and probably will, be broken with no backwards compatibility.

_Copyright © 2015-20 to Andrew Ward. All Rights Reserved._

## Design

RVWP mods should be *stateless*. The Lua environment that mods run in may
be deleted at anytime, losing anything stored in any locals or globals. You
should only use locals and globals to cache information, and store any state
using RVWP's APIs.

## Types and Classes

Types ending in `Ref` are defined in C++.
Types ending in `Table` are pure Lua tables with no metatables.
Otherwise types are Lua defined classes using metatables.
