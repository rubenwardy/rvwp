---
title: PlotRef
layout: default
nav_order: 10
---

# PlotRef

A plot an area which NPCs can be assigned to and do work within. For example,
players will buy plots and setup buildings to perform tasks such as research
or production. Plots are user visible, and so must have a human-friendly name.

Obtained by `rvwp.create_plot(def)`, `rvwp.get_plot(id)`, or `rvwp.get_plot_at_pos(pos)`

* `get_id()` - returns plot ID, will be globally unique
* `get_name()` - returns human-friendly name.
* `get_bounds()` - returns bounding rect.
* `contains(pos)` - whether `pos` is inside the plot.
* `get_boxes()` - returns an array of rect tables representing the bounds of the plot.
* `add_box(rect)` - extends the plot by `box`.
* `add_boxes(array of rects)`
* `set_boxes(array of rects)`
* `get_work()` - Returns an array of work tables.
* `remove_work(work_or_id)` - remove work by id or work table.

Example:

```lua
local plot = rvwp.create_plot({
    name = "My Base",
    from = { x = 0, y = 0, z = 0 },
    size = { x = 1, y = 1, z = 1 },
})
```

Work table:

```lua 
{
    id = 12,
    type = "build",
    bounds = {
        from = { x = 0, y = 0, z = 0 },
        size = { x = 1, y = 1, z = 1 },
    },
    item_name = "wall",
    material = {
        texture = "abc.png",
    },
}
```
