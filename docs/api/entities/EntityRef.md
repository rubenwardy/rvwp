---
title: EntityRef
layout: default
nav_order: 10
---

# EntityRef

An entity is a moving thing in the world. An entity is driven using [Entity Controllers](EntityController.html)

* `get_id()` - Gets the entity ID. Unique per session, may be reused in the future.
* `has_authority()` - Returns true if the server can control this entity.
* `get_type()` - Entity type name, "Player", or "Item".
* `get_pos()` - returns a [Vector](../utilities/Vector.html).
* `get_yaw()`
* `get_hp()`
* `set_hp(points)`
* `damage(points)` - returns points damaged.
* `get_properties()`
* `set_properties(table)`
    * Properties that are not included will not be modified.
* `get_meta()` - See [Storage > Key-Value](../storage.html#Key-Value) 
    
Only when `has_authority()` is true:

* `set_pos(pos)` - pos is any valid position (vector, table, etc).
* `set_yaw(yaw)` - `yaw` is a float.

## Entity Properties

```lua
{
    -- The material, used to specify rendering
    material = "material.png",

    -- The collision radius
    radius = 0.5,
   
    -- Whether the entity will play footsteps
    footsteps = true,
}
```
