#include <scripting/LuaWorld.hpp>
#include "scripting/LuaContent.hpp"
#include "LuaScripting.hpp"

#include "scripting/ModException.hpp"
#include "scripting/JSON.hpp"
#include "scripting/KeyValueStoreRef.hpp"

using namespace scripting;
using namespace world;
using namespace content;

#define add_func(name) core.set_function(#name, &LuaScripting::l_##name, this)

void LuaScripting::init(ScriptingInterface::Domain domain) {
	Log("LuaScripting", INFO) << "Initialising Lua API";
	lua.open_libraries(sol::lib::base, sol::lib::coroutine, sol::lib::math,
			sol::lib::os, sol::lib::package, sol::lib::string, sol::lib::table);

	DebugUIRef::registerToNamespace(lua);
	KeyValueStoreRef::registerToNamespace(lua);

	debugger = std::make_unique<LuaDebugger>(lua);

	security = std::make_unique<LuaSecurity>(lua);

	core = security->getEnvironment().create_named("core");
	add_func(get_mods);
	add_func(register_item);
	add_func(register_tool);

	auto handler = [&](auto error) { handleError(error); };

	serverModule = std::make_unique<LuaServerModule>(
			scriptingController, lua, core, handler);
	worldModule = std::make_unique<LuaWorldModule>(
			scriptingController, lua, core, handler);

	if (DebugUIRef::isAvailable()) {
		core["debug"] = DebugUIRef();
	}

	core.set_function("read_json", &l_read_json);
	core.set_function("parse_json", &l_parse_json);

	if (debugger->isInteractive()) {
		core["debug_pause"] = [&]() { debugger->pause(); };
		core["debug_breakpoint"] = sol::overload(
				[&](const std::string &file, int line) {
					debugger->addBreakpoint(file, line);
				},
				[&](const std::string &str) { debugger->addBreakpoint(str); });
	}

	isDead = false;
}

void LuaScripting::handleError(const sol::error &error) {
	isDead = true;

	std::string what = error.what();
	Log("LuaScripting", ERROR) << what;
	scriptingController->sendChatMessage(what);
}

bool LuaScripting::initMod(const ModConfig &mod) {
	try {
		mods_by_name.insert({mod.name, mod});

		security->setActiveModPath(mod.path);
		lua.script_file(mod.path + "/init.lua");
		security->setActiveModPath("");

		return true;
	} catch (sol::error &error) {
		security->setActiveModPath("");
		handleError(error);
		return false;
	}
}

sol::table LuaScripting::l_get_mods(sol::this_state state) {
	sol::table table(state.L, sol::create);

	for (const auto &it : mods_by_name) {
		table.add(it.first);
	}

	return table;
}

void LuaScripting::l_register_item(const sol::table &table) {
	using namespace std::string_literals;

	auto definitionManager = scriptingController->getDefinitionManager();

	std::string name = table["name"];
	if (definitionManager->isLocked()) {
		LogF(WARNING,
				"Ignoring definition of %s whilst definition manager is locked",
				name.c_str());
		return;
	}

	std::string sType = table["type"];

	auto optionalType = strToItemType(sType);
	if (!optionalType) {
		throw sol::error("Unknown item type " + sType);
	}

	std::string title = table["title"];
	Material material = LuaToMaterial(table["material"]);

	WeaponSpec spec{};
	auto optSpecTable = table.get<sol::optional<sol::table>>("weapon");
	if (optSpecTable) {
		auto specTable = optSpecTable.value();
		std::string typeName = specTable["type"];
		spec.type = stringToWeaponType(typeName);
		spec.range = specTable["range"];
		spec.damage = specTable["damage"];
		spec.useInterval = specTable["use_interval"];
		if (spec.type == WeaponSpec::Type::None) {
			throw sol::error("Unknown weapon type: " + typeName);
		}
	}

	ItemType type = optionalType.value();
	switch (type) {
	case ItemType::Item: {
		auto def = std::make_unique<ItemDef>(type);
		def->name = name;
		def->title = title;
		def->material = material;
		def->setWeaponSpec(spec);
		definitionManager->registerItem(def.release());
		break;
	}
	case ItemType::Terrain:
		// fallthrough
	case ItemType::Floor:
		// fallthrough
	case ItemType::Tile: {
		auto def = std::make_unique<TileDef>(type);
		def->name = name;
		def->title = title;
		def->material = material;
		def->setWeaponSpec(spec);
		def->collides = table["collides"];
		def->lightSource = table["light_source"];
		def->lightPropagates = table["light_propagates"];
		def->footstepSound = table.get_or("footstep_sound", ""s);
		def->friction = table["friction"];

		def->collisionBoxes.clear();
		sol::table boxes = table["collision_boxes"];
		for (const auto &element : boxes) {
			auto box = element.second.as<sol::table>();
			assert(box.size() == 4);
			float fromX = box[1];
			float fromY = box[2];
			float toX = box[3];
			float toY = box[4];
			auto rect =
					Rect3f({fromX, fromY, 0}, {toX - fromX, toY - fromY, 1});
			def->collisionBoxes.push_back(rect);
		}

		definitionManager->registerTile(def.release());

		break;
	}
	}
}

void LuaScripting::l_register_tool(const sol::table &table) {
	using namespace std::string_literals;

	auto definitionManager = scriptingController->getDefinitionManager();
	if (definitionManager->isLocked()) {
		LogF(WARNING,
				"Ignoring definition of tool whilst definition manager is "
				"locked");
		return;
	}

	std::string sToolType = table["type"];
	auto optionalToolType = strToToolType(sToolType);
	if (!optionalToolType) {
		throw sol::error("Unknown tool type " + sToolType);
	}
	ToolType toolType = optionalToolType.value();

	std::string sSelectType = table.get_or("select_type", "filled"s);
	auto optionalSelectType = strToToolSelectType(sSelectType);
	if (!optionalSelectType) {
		throw sol::error("Unknown select type " + sSelectType);
	}
	ToolSelectType selectType = optionalSelectType.value();

	ChunkLayer layer = stringToLayer(table.get_or("layer", "tile"s));

	definitionManager->registerToolSpec(
			{toolType, table["description"], LuaToMaterial(table["material"]),
					selectType, table.get_or("fixed_selection_size", V3s()),
					table.get_or("item_name", ""s), layer});
}
