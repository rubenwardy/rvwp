#pragma once
#include <string>
#include "world/World.hpp"
#include "scripting/ModConfig.hpp"

namespace scripting {
class ScriptingInterface {
public:
	virtual ~ScriptingInterface() = default;

	/// Where the code is being executed.
	enum Domain { MASTER, CHUNK };

	/// Initialize scripting environment
	///
	/// @param domain The domain.
	virtual void init(Domain domain) = 0;

	/// @return true if the scripting environment hasn't been created yet, or
	/// has crashed
	virtual bool getIsDead() = 0;

	/// Load mod
	///
	/// @param mod Mod config to load
	/// @return True on success
	virtual bool initMod(const ModConfig &mod) = 0;

	/// Runs chat message callbacks
	///
	/// @param name Sender player name
	/// @param message the chat message
	/// @return true to consume the message
	virtual void onChatMessage(
			const std::string &name, const std::string &message) = 0;

	/// Run expired tile timers callback
	///
	/// @param timers Expired times
	virtual void runExpiredTimers(
			const std::vector<world::TileTimer> &timers) = 0;

	/// Set up scripting-controlled Entity
	///
	/// @param entity An newly emerged entity. The entity
	/// @return True on success
	virtual bool createEntity(world::Entity *entity) = 0;

	/// Update entity step
	/// @return True on success
	virtual bool updateEntity(world::Entity *entity, float dtime) = 0;

	/// On tile interact
	///
	/// @param entity Entity
	/// @param pos Tile position
	/// @return True if handled
	virtual bool tileInteract(world::Entity *entity, const V3s &pos) = 0;
};
} // namespace scripting
