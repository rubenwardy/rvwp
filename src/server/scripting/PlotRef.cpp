#include <utility>

#include "PlotRef.hpp"
#include "server/controllers/ScriptingController.hpp"
#include "scripting/LuaWorld.hpp"

using namespace scripting;
using namespace world;

void PlotRef::registerToNamespace(sol::state &lua) {
	// clang-format off
	lua.new_usertype<PlotRef>("PlotRef",
			"get_id",      &PlotRef::get_id,
			"get_name",    &PlotRef::get_name,
			"get_bounds",  &PlotRef::get_bounds,
			"contains",    &PlotRef::contains,
			"get_boxes",   &PlotRef::get_boxes,
			"add_box",     &PlotRef::add_box,
			"add_boxes",   &PlotRef::add_boxes,
			"set_boxes",   &PlotRef::add_box,
			"add_boxes",   &PlotRef::add_boxes,
			"set_boxes",   &PlotRef::set_boxes,
			"get_work",    &PlotRef::get_work,
			"remove_work", &PlotRef::remove_work);
	// clang-format on
}

int PlotRef::get_id() const {
	return plot->id;
}

std::string PlotRef::get_name() const {
	return plot->name;
}

sol::table PlotRef::get_bounds(sol::this_state state) const {
	return RectToTable<s32>(plot->bounds, state);
}

bool PlotRef::contains(V3s pos) const {
	return plot->contains(pos);
}

sol::table PlotRef::get_boxes(sol::this_state state) const {
	sol::table table(state.L, sol::create);

	int i = 1;
	for (const auto &box : plot->peekBoxes()) {
		table[i] = RectToTable<s32>(box, state);
		i++;
	}
	return table;
}

void PlotRef::add_box(sol::table box) {
	auto rect = TableToRect<s32>(std::move(box));
	plot->addBox(rect);
}

void PlotRef::add_boxes(sol::table boxes) {
	size_t size = boxes.size();
	std::vector<Rect3s> rects(size);

	for (size_t i = 1; i <= size; i++) {
		rects[i - 1] = TableToRect<s32>(boxes[i]);
	}

	plot->addBoxes(rects);
}

void PlotRef::set_boxes(sol::table boxes) {
	plot->clearBoxes();
	add_boxes(std::move(boxes));
}

sol::table PlotRef::get_work(sol::this_state state) const {
	sol::table ret(state.L, sol::create);

	std::vector<Work *> works;
	plot->getAllWork(works);

	int i = 1;
	for (const auto work : works) {
		sol::table material(state.L, sol::create);
		material["texture"] = work->material.name;

		sol::table element(state.L, sol::create);
		element["id"] = work->id;
		element["type"] = work->type;
		element["bounds"] = RectToTable(work->bounds, state);
		element["material"] = material;
		if (!work->itemName.empty()) {
			element["item_name"] = work->itemName;
		}
		element["layer"] = layerToString(work->layer);

		ret[i] = element;
		i++;
	}
	return ret;
}

bool PlotRef::remove_work(const sol::object &obj) {
	u32 workId;
	if (obj.is<u32>()) {
		workId = obj.as<u32>();
	} else if (obj.get_type() == sol::type::table) {
		auto table = obj.as<sol::table>();
		workId = table["id"];
	} else {
		throw sol::error("Invalid work passed to PlotRef:remove_work()");
	}

	return controller->removeWork(workId);
}
