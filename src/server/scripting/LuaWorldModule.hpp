#pragma once

#include "server/controllers/ScriptingController.hpp"
#include "scripting/Lua.hpp"
#include "scripting/KeyValueStoreRef.hpp"

namespace scripting {

class EntityRef;
class PlotRef;
class TileTimerRef;
class LuaWorldModule {
	server::ScriptingController *scriptingController;
	sol::state &lua;
	sol::table &core;
	std::function<void(const sol::error &)> handleError;

public:
	LuaWorldModule(server::ScriptingController *scriptingController,
			sol::state &lua, sol::table &core,
			std::function<void(const sol::error &)> handler);

	void runExpiredTimers(const std::vector<world::TileTimer> &timers);
	bool createEntity(world::Entity *entity);
	bool updateEntity(world::Entity *entity, float dtime);
	bool tileInteract(world::Entity *entity, const V3s &pos);

private:
	sol::optional<sol::table> l_get_layer(
			V3s pos, const std::string &typeName, sol::this_state state);
	bool l_set_layer(V3s pos, const std::string &typeName, sol::table tile);
	sol::optional<int> l_get_surface_height(V3s pos);

	sol::optional<KeyValueStoreRef> l_get_meta(
			V3s pos, const std::string &layerName);

	sol::optional<EntityRef> l_get_player_entity(const std::string &name);
	sol::table l_get_entities_in_range(
			V3f pos, float range, sol::this_state state);
	EntityRef l_spawn_entity(V3f pos, const std::string &name);
	sol::optional<PlotRef> l_get_plot(int id);
	sol::optional<PlotRef> l_get_plot_at_pos(V3s pos);
	sol::optional<PlotRef> l_create_plot(sol::table obj);

	sol::object l_find_path(V3s from, V3s to,
			sol::optional<sol::table> settings, sol::this_state state);

	std::tuple<sol::table, sol::table> l_find_with_metadata(V3f pos,
			float range, const std::string &layerName,
			const std::string &metaKey, sol::this_state state);

	sol::optional<TileTimerRef> l_get_timer(
			V3s pos, const std::string &layerName, const std::string &event);

	bool l_remove_work(const sol::object &obj);
};

} // namespace scripting
