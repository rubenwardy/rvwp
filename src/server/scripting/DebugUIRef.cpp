#include "DebugUIRef.hpp"
#include "world/DebugUI.hpp"

using namespace scripting;
using namespace debug;

bool DebugUIRef::is_enabled() const {
	return DebugUI().isEnabled();
}

void DebugUIRef::registerToNamespace(sol::state &lua) {
	// clang-format off
	lua.new_usertype<DebugUIRef>("DebugUIRef",
			 "is_enabled", &DebugUIRef::is_enabled,
			"draw_label", &DebugUIRef::draw_label,
			"draw_line",  &DebugUIRef::draw_line);
	// clang-format on
}

void DebugUIRef::draw_label(V3f pos, const std::string &text) {
	DebugUI().addLabel(pos, text);
}

void DebugUIRef::draw_line(V3f from, V3f to, const std::string &color) {
	DebugUI().addLine(from, to, color);
}

bool DebugUIRef::isAvailable() {
#ifdef DEBUG_UI_ENABLED
	return true;
#else
	return false;
#endif
}
