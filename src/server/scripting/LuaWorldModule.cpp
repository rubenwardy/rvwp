#include "LuaWorldModule.hpp"

#include "EntityRef.hpp"
#include "PlotRef.hpp"
#include "TileTimerRef.hpp"

#include "scripting/LuaWorld.hpp"

using namespace scripting;
using namespace world;

#define add_func(name) core.set_function(#name, &LuaWorldModule::l_##name, this)

LuaWorldModule::LuaWorldModule(server::ScriptingController *scriptingController,
		sol::state &lua, sol::table &core,
		std::function<void(const sol::error &)> handler)
		: scriptingController(scriptingController), lua(lua), core(core),
		  handleError(handler) {
	EntityRef::registerToNamespace(lua);
	PlotRef::registerToNamespace(lua);
	TileTimerRef::registerToNamespace(lua);

	add_func(get_layer);
	add_func(set_layer);
	add_func(get_surface_height);
	add_func(get_meta);
	add_func(get_player_entity);
	add_func(get_entities_in_range);
	add_func(spawn_entity);
	add_func(get_plot);
	add_func(get_plot_at_pos);
	add_func(create_plot);
	add_func(find_path);
	add_func(find_with_metadata);
	add_func(get_timer);
	add_func(remove_work);

	// Layer constants
	auto layers = core.create_named("layers");
	layers["tile"] = "tile";
	layers["floor"] = "floor";
	layers["terrain"] = "terrain";

	// Aliases
	core.set_function("set_tile", [this](V3s pos, sol::table tile) -> bool {
		return l_set_layer(pos, "tile", tile);
	});

	core.set_function("get_tile",
			[this](V3s pos,
					sol::this_state state) -> sol::optional<sol::table> {
				return l_get_layer(pos, "tile", state);
			});

	core.set_function("set_floor", [this](V3s pos, sol::table tile) -> bool {
		return l_set_layer(pos, "floor", tile);
	});

	core.set_function("get_floor",
			[this](V3s pos,
					sol::this_state state) -> sol::optional<sol::table> {
				return l_get_layer(pos, "floor", state);
			});

	core.set_function("set_terrain", [this](V3s pos, sol::table tile) -> bool {
		return l_set_layer(pos, "terrain", tile);
	});

	core.set_function("get_terrain",
			[this](V3s pos,
					sol::this_state state) -> sol::optional<sol::table> {
				return l_get_layer(pos, "terrain", state);
			});
}

void LuaWorldModule::runExpiredTimers(const std::vector<TileTimer> &timers) {
	sol::function f = core["on_expired_timer"];

	for (const auto &timer : timers) {
		auto ret = f(timer.position, layerToString(timer.layer), timer.event);
		if (!ret.valid()) {
			handleError(ret);
			break;
		}
	}
}

bool LuaWorldModule::createEntity(Entity *entity) {
	sol::function f = core["init_entity"];
	auto ret = f(EntityRef(entity));
	if (!ret.valid()) {
		handleError(ret);
		return false;
	}

	return true;
}

bool LuaWorldModule::updateEntity(Entity *entity, float dtime) {
	sol::function f = core["update_entity"];
	auto ret = f(EntityRef(entity), dtime);
	if (!ret.valid()) {
		handleError(ret);
		return false;
	}

	return true;
}

bool LuaWorldModule::tileInteract(world::Entity *entity, const V3s &pos) {
	auto ret = core["tile_interact"](EntityRef(entity), pos);
	if (!ret.valid()) {
		handleError(ret);
		return false;
	}
	return ret.get<bool>();
}

sol::optional<sol::table> LuaWorldModule::l_get_layer(
		V3s pos, const std::string &typeName, sol::this_state state) {
	ChunkLayer layer = stringToLayer(typeName);

	auto tile = scriptingController->getTile(pos, layer);
	if (tile == nullptr || tile->empty()) {
		return {};
	}

	auto def = scriptingController->getTileDefinition(tile->cid);
	if (def == nullptr) {
		return {};
	}

	TileSpecFromLua spec = {def->name};

	return {TileToTable(spec, state)};
}

bool LuaWorldModule::l_set_layer(
		V3s pos, const std::string &typeName, sol::table tile) {
	ChunkLayer layer = stringToLayer(typeName);
	TileSpecFromLua tileSpec = TableToTileSpec(tile);

	auto definitionManager = scriptingController->getDefinitionManager();
	content_id cid = tileSpec.name.has_value()
			? definitionManager->getCidFromName(tileSpec.name.value())
			: 0;

	return scriptingController->setTile(pos, layer, WorldTile(cid));
}

sol::optional<int> LuaWorldModule::l_get_surface_height(V3s pos) {
	auto optional = scriptingController->getSurfaceHeight(pos);
	return std2sol(optional);
}

sol::optional<KeyValueStoreRef> LuaWorldModule::l_get_meta(
		V3s pos, const std::string &layerName) {
	world::ChunkLayer layer = stringToLayer(layerName);

	auto meta = scriptingController->getMetaData(pos, layer);
	if (meta.has_value()) {
		return KeyValueStoreRef(meta.value());
	} else {
		return {};
	}
}

sol::optional<EntityRef> LuaWorldModule::l_get_player_entity(
		const std::string &name) {
	auto entity = scriptingController->getPlayerEntity(name);
	if (entity) {
		return {EntityRef(entity)};
	} else {
		return {};
	}
}

sol::table LuaWorldModule::l_get_entities_in_range(
		V3f pos, float range, sol::this_state state) {
	std::vector<Entity *> entities;
	scriptingController->getEntitiesInRange(pos, entities, range);

	sol::table table(state.L, sol::create);
	for (auto entity : entities) {
		table.add(EntityRef(entity));
	}
	return table;
}

EntityRef LuaWorldModule::l_spawn_entity(V3f pos, const std::string &name) {
	return EntityRef(scriptingController->spawnEntity(pos, name));
}

sol::optional<PlotRef> LuaWorldModule::l_get_plot(int id) {
	auto plot = scriptingController->getPlot(id);
	if (plot) {
		return {PlotRef(scriptingController, plot)};
	} else {
		return {};
	}
}

sol::optional<PlotRef> LuaWorldModule::l_get_plot_at_pos(V3s pos) {
	auto plot = scriptingController->getPlot(pos);
	if (plot) {
		return {PlotRef(scriptingController, plot)};
	} else {
		return {};
	}
}

sol::optional<PlotRef> LuaWorldModule::l_create_plot(sol::table obj) {
	V3s pos1 = obj["from"];
	V3s size = obj["size"];
	std::string name = obj["name"];

	auto plot = scriptingController->createPlot(name, {pos1, size});
	if (plot) {
		return {PlotRef(scriptingController, plot)};
	} else {
		return {};
	}
}

sol::object LuaWorldModule::l_find_path(V3s from, V3s to,
		sol::optional<sol::table> settings, sol::this_state state) {
	world::Pathfinder::Settings readSettings;
	if (settings.has_value()) {
		readSettings = TableToSettings(
				settings.value(), scriptingController->getDefinitionManager());
	} else {
		readSettings = {};
	}

	std::vector<V3s> path;
	if (!scriptingController->findPath(path, from, to, readSettings)) {
		return sol::nil;
	}

	sol::table table(state.L, sol::create);
	for (const auto &node : path) {
		table.add(node.floating() + V3f(0.5f, 0.5f, 0));
	}

	return table;
}

std::tuple<sol::table, sol::table> LuaWorldModule::l_find_with_metadata(V3f pos,
		float range, const std::string &layerName, const std::string &metaKey,
		sol::this_state state) {
	ChunkLayer layer = stringToLayer(layerName);

	std::vector<std::pair<V3s, WorldTile *>> tiles;

	scriptingController->findNodesWithMetaData(
			tiles, pos, range, layer, metaKey);

	sol::table posTable(state.L, sol::create);
	sol::table valueTable(state.L, sol::create);

	for (const auto &tile : tiles) {
		posTable.add(tile.first);

		auto def = scriptingController->getTileDefinition(tile.second->cid);
		assert(def);

		TileSpecFromLua spec;
		spec.name = def->name;
		valueTable.add(TileToTable(spec, state));
	}

	return std::make_tuple(posTable, valueTable);
}

sol::optional<TileTimerRef> LuaWorldModule::l_get_timer(
		V3s pos, const std::string &layerName, const std::string &event) {
	world::ChunkLayer layer = stringToLayer(layerName);

	// Is the chunk loaded?
	if (!scriptingController->getChunk(PosToCPos(pos))) {
		return {};
	}

	return TileTimerRef(scriptingController, pos, layer, event);
}

bool LuaWorldModule::l_remove_work(const sol::object &obj) {
	u32 workId;
	if (obj.is<u32>()) {
		workId = obj.as<u32>();
	} else if (obj.get_type() == sol::type::table) {
		auto table = obj.as<sol::table>();
		workId = table["id"];
	} else {
		throw sol::error("Invalid work passed to PlotRef:remove_work()");
	}

	return scriptingController->removeWork(workId);
}
