#include "EntityRef.hpp"
#include "../../world/Chunk.hpp"
#include "scripting/LuaContent.hpp"

using namespace scripting;
using namespace world;

void EntityRef::registerToNamespace(sol::state &lua) {
	auto equal_to = sol::meta_function::equal_to;

	// clang-format off
	lua.new_usertype<EntityRef>("EntityRef",
			"get_id",         &EntityRef::get_id,
			"has_authority",  &EntityRef::has_authority,
			"get_type",       &EntityRef::get_type,
			equal_to,         &EntityRef::equals,
			"get_pos",        &EntityRef::get_pos,
			"set_pos",        &EntityRef::set_pos,
			"move_to",        &EntityRef::move_to,
			"move",           &EntityRef::move,
			"jump",           &EntityRef::jump,
			"get_yaw",        &EntityRef::get_yaw,
			"set_yaw",        &EntityRef::set_yaw,
			"get_hp",         &EntityRef::get_hp,
			"set_hp",         &EntityRef::set_hp,
			"damage",         &EntityRef::damage,
			"get_properties", &EntityRef::get_properties,
			"set_properties", &EntityRef::set_properties,
			"get_meta",       &EntityRef::get_meta);
	// clang-format on
}

//
// Methods
//

int EntityRef::get_id() const {
	if (entity == nullptr) {
		return 0;
	}

	return entity->id;
	;
}

bool EntityRef::has_authority() const {
	if (entity == nullptr) {
		return false;
	}

	return entity->hasAuthority;
}

std::string EntityRef::get_type() const {
	if (entity == nullptr) {
		return "";
	}

	return entity->typeName;
}

bool EntityRef::equals(EntityRef *L) const {
	return L->entity == entity;
}

V3f EntityRef::get_pos() const {
	return entity->getPosition();
}

void EntityRef::set_pos(V3f pos) {
	entity->setPosition(pos);
}

bool EntityRef::move_to(V3f pos) {
	entity->moveTo(pos);
	entity->checkForFalling();

	return true;
}

bool EntityRef::move(V3f delta) {
	entity->move(delta.x, 0);
	entity->move(0, delta.y);
	entity->checkForFalling();

	return true;
}

bool EntityRef::jump(V3f delta) {
	return entity->jump(delta);
}

f32 EntityRef::get_yaw() const {
	return entity->getYaw();
}

void EntityRef::set_yaw(f32 yaw) {
	entity->setYaw(yaw);
}

int EntityRef::get_hp() const {
	return entity->hp;
}

void EntityRef::set_hp(int hp) {
	entity->hp = hp;
}

int EntityRef::damage(int points) {
	return entity->damage(points);
}

sol::table EntityRef::get_properties(sol::this_state state) {
	sol::table table(state.L, sol::create);
	table["radius"] = entity->properties.radius;
	table["footsteps"] = entity->properties.footsteps;
	table["material"] = MaterialToLua(entity->properties.material, state);
	return table;
}

void EntityRef::set_properties(const sol::table &table) {
	entity->properties.radius =
			table.get_or("radius", entity->properties.footsteps);
	entity->properties.footsteps =
			table.get_or("footsteps", entity->properties.footsteps);

	auto material = table.get<sol::optional<sol::object>>("material");
	if (material.has_value()) {
		entity->properties.material = LuaToMaterial(material.value());
	}

	entity->dirty_properties = true;
}

KeyValueStoreRef EntityRef::get_meta() {
	return KeyValueStoreRef(entity->metadata);
}
