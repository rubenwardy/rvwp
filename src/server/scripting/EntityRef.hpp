#pragma once

#include "scripting/Lua.hpp"
#include "scripting/KeyValueStoreRef.hpp"
#include "world/Entity.hpp"

namespace scripting {

class EntityRef {
	world::Entity *entity;

	int get_id() const;
	bool has_authority() const;

	std::string get_type() const;
	bool equals(EntityRef *L) const;

	V3f get_pos() const;
	void set_pos(V3f pos);
	bool move_to(V3f pos);
	bool move(V3f delta);
	bool jump(V3f delta);

	f32 get_yaw() const;
	void set_yaw(f32 yaw);

	int get_hp() const;
	void set_hp(int hp);

	int damage(int points);

	sol::table get_properties(sol::this_state state);
	void set_properties(const sol::table &table);

	KeyValueStoreRef get_meta();

public:
	explicit EntityRef(world::Entity *entity) : entity(entity) {}
	static void registerToNamespace(sol::state &lua);
};

} // namespace scripting
