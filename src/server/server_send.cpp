#include "Server.hpp"
#include <log.hpp>

using namespace server;
using namespace world;
using namespace network;

void Server::sendPlayerProfile(Peer *peer, ServerPlayer *player) {
	if (!peer) {
		Log("Server", ERROR) << "Cannot send profile, peer doesn't exist!";
		return;
	} else if (!player) {
		if (peer->player_info) {
			player = peer->player_info;
		} else {
			Log("Server", ERROR)
					<< "Cannot send profile, player info doesn't exist!";
			return;
		}
	}

	assert(player);
	const size_t size = 2 + player->username.size() + 3 * 4;
	Packet pkt(peer->address, TOCLIENT_PROFILE, size);
	pkt << player->username;
	pkt << player->last_position;
	socket.send(pkt);
}

void Server::sendPlayerInventory(Peer *peer) {
	if (!peer) {
		Log("Server", ERROR) << "Cannot send inventory, peer doesn't exist!";
		return;
	} else if (!peer->player_info) {
		Log("Server", ERROR)
				<< "Cannot send inventory, player info doesn't exist!";
		return;
	}

	ServerPlayer *player_info = peer->player_info;

	Packet pkt(peer->address, TOCLIENT_PLAYER_INVENTORY,
			player_info->inventory.serialised_size());
	player_info->inventory.serialise(pkt);
	socket.send(pkt);
}

void Server::sendDefinitions(Peer *peer) {
	for (const auto def : definitionManager) {
		size_t size = 1 + 2 + def->name.size() + 2 + def->title.size() +
				def->material.serialised_size() + 1;
		auto tileDef = dynamic_cast<content::TileDef *>(def);
		if (def->weapon_spec) {
			size += 4 + 2 + 4;
		}
		if (tileDef) {
			size += 2 + 1 + 1 + 1 + 2 + tileDef->footstepSound.size() + 4 + 1 +
					def->collisionBoxes.size() * 2 * 3 * 4;
		}

		Packet pkt(peer->address, TOCLIENT_ITEM_DEFINITION, size);
		pkt << static_cast<u8>(def->getType()) << def->name << def->title;
		def->material.serialise(pkt);

		if (def->weapon_spec) {
			const auto &weapon_spec = def->weapon_spec.value();
			pkt << static_cast<u8>(weapon_spec.type) << weapon_spec.range
				<< weapon_spec.damage << weapon_spec.useInterval;
		} else {
			pkt << static_cast<u8>(content::WeaponSpec::Type::None);
		}

		if (tileDef) {
			pkt << tileDef->id << tileDef->collides << tileDef->lightSource
				<< tileDef->lightPropagates << tileDef->footstepSound
				<< tileDef->friction;

			pkt << (u8)def->collisionBoxes.size();
			for (const auto &box : def->collisionBoxes) {
				pkt << box;
			}
		}

		socket.send(pkt);
	}

	for (const auto &tool : definitionManager.getToolSpecs()) {
		size_t size = 1 + 2 + tool.description.size() + 1 + 3 * 4 +
				tool.material.serialised_size() + 2 + tool.itemName.size() + 1;

		Packet pkt(peer->address, TOCLIENT_TOOL_DEFINITION, size);
		pkt << static_cast<u8>(tool.type) << tool.description
			<< static_cast<u8>(tool.selectType) << tool.fixedSelectionSize
			<< tool.itemName << static_cast<u8>(tool.layer);
		tool.material.serialise(pkt);
		socket.send(pkt);
	}
}

void Server::sendChatMessageToPlayer(Peer *peer, const std::string &message) {
	Packet pkt(peer->address, TOANY_CHAT_MESSAGE, 2 + message.size());
	pkt << message;

	socket.send(pkt);
}

void Server::sendChatMessageToAllPlayers(
		const std::string &message, Peer *ignore_peer) {
	for (auto &it : peers) {
		Peer *peer = it.second.get();
		if (peer->loaded && peer != ignore_peer) {
			sendChatMessageToPlayer(peer, message);
		}
	}
}

void Server::sendGameTimeToPlayer(Peer *peer, GameTime gameTime) {
	if (!peer) {
		Log("Server", ERROR) << "Cannot send game time, peer doesn't exist!";
		return;
	}

	const int size = 4 * 3;
	Packet pkt(peer->address, TOCLIENT_SET_TIME, size);
	pkt << gameTime.realTimeSinceBeginning;
	pkt << gameTime.timeOfDay;
	pkt << gameTime.speed;

	socket.send(pkt);
}

void Server::sendChunkToPlayer(Peer *peer, WorldChunk *chunk) {
	if (!peer) {
		Log("Server", ERROR) << "Cannot send chunk, peer doesn't exist!";
		return;
	} else if (!chunk) {
		Log("Server", ERROR) << "Cannot send chunk, it doesn't exist!";
		return;
	} else if (chunk->isDummy()) {
		Log("Server", ERROR) << "Cannot send chunk, it's a dummy chunk!";
		return;
	} else if (!chunk->isActive()) {
		Log("Server", ERROR) << "Cannot send chunk, it's not active!";
		return;
	}

	const int size = 3 * 4 + 2 * CHUNK_SIZE * CHUNK_SIZE * (2 + 1) +
			2 * CHUNK_SIZE * CHUNK_SIZE + 2 * CHUNK_SIZE * CHUNK_SIZE;
	Packet pkt(peer->address, TOCLIENT_WORLD_BLOCK, size);
	pkt << chunk->pos;

	for (const auto &tile : chunk->tiles) {
		if (!tile.empty()) {
			pkt << (u16)tile.cid;
			pkt << (u8)0;
		} else {
			pkt << (u16)0;
			pkt << (u8)0;
		}
	}

	for (const auto &tile : chunk->floort) {
		if (!tile.empty()) {
			pkt << (u16)tile.cid;
			pkt << (u8)0;
		} else {
			pkt << (u16)0;
			pkt << (u8)0;
		}
	}

	for (u16 i : chunk->terrain) {
		pkt << i;
	}

	for (u16 lightLevel : chunk->lightLevels) {
		pkt << lightLevel;
	}

	socket.send(pkt);

	peer->setChunkSent(chunk->pos);
}

void Server::sendSetTileToAllPlayers(
		const V3s &pos, ChunkLayer layer, Peer *ignore_peer) {
	WorldTile *tile = world_ctr->get(pos, layer);

	const int size = 3 * 4 + 4;
	network::Packet pkt(network::Address(), TOANY_WORLD_SET_TILE, size);
	pkt << pos;
	pkt << (u32)(tile ? tile->cid : 0);

	for (auto &it : peers) {
		Peer *peer = it.second.get();
		if (peer != ignore_peer) {
			pkt.setOther(peer->address);
			socket.send(pkt);
		}
	}
}

void Server::sendPlayerPosition(Peer *peer) {
	if (!peer) {
		Log("Server", ERROR) << "Cannot send player pos, peer doesn't exist!";
		return;
	} else if (!peer->entity) {
		Log("Server", ERROR)
				<< "Cannot send player pos, peer entity doesn't exist!";
		return;
	}

	const int size = 3 * 4;
	Packet pkt(peer->address, TOCLIENT_WORLD_GOTO, size);
	pkt << peer->entity->getPosition();
	socket.send(pkt);
}

void Server::sendRemoveEntityToPlayer(Peer *peer, int id) {
	if (!peer) {
		Log("Server", ERROR)
				<< "Cannot send remove entity, peer doesn't exist!";
		return;
	} else if (id < 0) {
		Log("Server", ERROR) << "Cannot send remove entity, it doesn't exist!";
		return;
	}
	Log("Server", INFO) << "Sending remove entity to player!";

	const int size = 4;
	Packet pkt(peer->address, TOCLIENT_WORLD_DELETE_ENTITY, size);
	pkt << (u32)id;
	socket.send(pkt);

	peer->unsetEntitySent(id);
}

void Server::sendRemoveEntityToAllPlayers(int id, Peer *exclude) {
	for (auto &it : peers) {
		Peer *peer = it.second.get();
		if (peer->loaded && peer != exclude) {
			sendRemoveEntityToPlayer(peer, id);
		}
	}
}

void Server::detectAndSendEntityChanges() {
	// According to stack overflow, 1500 is a typical MTU size.
	// This value is comfortably lower than that.
	const float maxPacketSize = 1200;

	for (auto &it : peers) {
		Peer *peer = it.second.get();
		if (!peer->loaded) {
			continue;
		}

		Packet pktNew(peer->address, TOCLIENT_WORLD_NEW_ENTITIES);
		Packet pktUpdates(peer->address, TOCLIENT_WORLD_UPDATE_ENTITIES);

		for (auto entity : world_ctr->getEntityList()) {
			if (!peer->hasEntity(entity)) {
				pktNew << (u32)entity->id;
				pktNew << entity->getPosition();
				pktNew << entity->getYaw();
				pktNew << entity->hp;
				entity->properties.material.serialise(pktNew);
				pktNew << entity->properties.radius;
				pktNew << entity->properties.footsteps;
				peer->setEntitySent(entity);

				if (pktNew.size() > maxPacketSize) {
					socket.send(pktNew);
					pktNew = Packet(peer->address, TOCLIENT_WORLD_NEW_ENTITIES);
				}

				continue;
			}

			float dist = entity->last_sent_position.sqDistance(
					entity->getPosition());
			f32 diff = entity->last_sent_yaw - entity->getYaw();

			// Check properties have changed, check that position changes aren't
			// sent to the originating player
			if ((peer->entity == entity ||
						(dist <= 0.01f && std::abs(diff) <= 2.f)) &&
					!entity->dirty_properties) {
				continue;
			}

			pktUpdates << (u32)entity->id;

			// TODO: don't sent position/yaw separately like this
			if (peer->entity != entity) {
				pktUpdates << (u16)1 << entity->getPosition();
				pktUpdates << (u16)2 << entity->getYaw();
			}

			if (entity->dirty_properties) {
				for (int i = (int)Entity::Property::HP;
						i < Entity::PropertyCount; i++) {
					auto property = static_cast<Entity::Property>(i);
					if (peer->entity == entity &&
							(property == Entity::Property::Position ||
									property == Entity::Property::Yaw)) {
						continue;
					}

					pktUpdates << (u16)(i + 1);

					switch (property) {
					case Entity::Property::Position:
						pktUpdates << entity->getPosition();
						break;
					case Entity::Property::Yaw:
						pktUpdates << entity->getYaw();
						break;
					case Entity::Property::HP:
						pktUpdates << entity->hp;
						break;
					case Entity::Property::Material:
						entity->properties.material.serialise(pktUpdates);
						break;
					case Entity::Property::Radius:
						pktUpdates << entity->properties.radius;
						break;
					case Entity::Property::Footsteps:
						pktUpdates << entity->properties.footsteps;
						break;
					}
				}
			}

			pktUpdates << (u16)0;

			if (pktUpdates.size() > maxPacketSize) {
				socket.send(pktUpdates);
				pktUpdates =
						Packet(peer->address, TOCLIENT_WORLD_UPDATE_ENTITIES);
			}
		}

		if (pktUpdates.size() > 0) {
			socket.send(pktUpdates);
		}

		if (pktNew.size() > 0) {
			socket.send(pktNew);
		}
	}

	for (auto entity : world_ctr->getEntityList()) {
		float dist =
				entity->last_sent_position.sqDistance(entity->getPosition());
		f32 diff = entity->last_sent_yaw - entity->getYaw();
		if (dist > 0.01f || std::abs(diff) > 2.f || entity->dirty_properties) {
			entity->dirty_properties = false;
			entity->last_sent_position = entity->getPosition();
			entity->last_sent_yaw = entity->getYaw();
		}
	}
}

void Server::sendDisconnectAndRemovePeer(
		Address address, const std::string &errm, int reconnect) {
	const size_t size = 1 + 2 + errm.size() + 1;
	Packet pkt(address, TOANY_DISC, size);
	pkt << (u8)0;
	pkt << errm;
	pkt << (u8)reconnect;
	socket.send(pkt);
	socket.disconnect(address);

	auto peer = peers.find(address);
	if (peer != peers.end() && peer->second) {
		leavePlayer(peer->second.get());
		peers.erase(peer);
	}
}

void Server::sendDirtyChunkToWatchers(WorldChunk *chunk) {
	assert(chunk->dirty);

	for (const auto &pair : peers) {
		if (pair.second->hasChunk(chunk->pos)) {
			sendChunkToPlayer(pair.second.get(), chunk);
		}
	}

	chunk->dirty = false;
}

void Server::sendPlotToPlayer(Peer *peer, const world::Plot *plot) {
	if (!peer) {
		LogF(ERROR, "[Server] Cannot send player pos, peer doesn't exist!");
		return;
	}

	const size_t size = 4 + 2 + plot->name.size() + 2 +
			plot->peekBoxes().size() * (3 * 4 * 2);
	Packet pkt(peer->address, TOCLIENT_WORLD_PLOT, size);
	pkt << (u32)plot->id;
	pkt << plot->name;

	pkt << (u16)plot->peekBoxes().size();
	for (const auto &box : plot->peekBoxes()) {
		pkt << box;
	}

	socket.send(pkt);

	std::vector<world::Work *> works;
	plot->getAllWork(works);
	for (auto work : works) {
		sendWorkCreatedToPlayer(peer, plot->id, work);
	}

	peer->setPlotSent(plot);
}

void Server::sendWorkCreatedToPlayer(
		Peer *peer, int plotId, const world::Work *work) {
	if (peer->workServerToClient.find(work->id) !=
			peer->workServerToClient.end()) {
		return;
	}

	const size_t size = 4 + 4 + 2 + work->type.size() + 2 * 3 * 4 + 2 +
			work->itemName.size() + work->material.serialised_size() + 1;
	Packet pkt(peer->address, TOANY_WORLD_NEW_WORK, size);
	pkt << (u32)work->id;
	pkt << (u32)plotId;
	pkt << work->type;
	pkt << work->bounds;
	work->material.serialise(pkt);
	pkt << work->itemName;
	pkt << (u8)work->layer;
	socket.send(pkt);
}

void Server::sendWorkCreatedToWatchers(int plotId, const world::Work *work) {
	for (const auto &pair : peers) {
		if (pair.second->hasPlot(plotId)) {
			sendWorkCreatedToPlayer(pair.second.get(), plotId, work);
		}
	}
}

void Server::sendWorkRemovedToPlayer(Peer *peer, u32 serverId) {
	u32 clientId = peer->workServerToClient[serverId];

	const int size = 4 + 4;
	Packet pkt(peer->address, TOANY_WORLD_REMOVE_WORK, size);
	pkt << serverId;
	pkt << clientId;
	socket.send(pkt);
}

void Server::sendWorkRemovedToWatchers(u32 plotId, u32 workId) {
	for (const auto &pair : peers) {
		if (pair.second->hasPlot(plotId)) {
			sendWorkRemovedToPlayer(pair.second.get(), workId);
		}
	}
}

void Server::sendSpawnParticle(Peer *peer, const V3f &position,
		const V3f &velocity, const content::Material &material) {
	const size_t size = 2 * 3 * 4 + material.serialised_size();
	Packet pkt(peer->address, TOCLIENT_SPAWN_PARTICLE, size);
	pkt << position;
	pkt << velocity;
	material.serialise(pkt);
	socket.send(pkt);
}

void Server::sendSpawnParticleToAllPlayers(const V3f &position,
		const V3f &velocity, const content::Material &material, Peer *exclude) {
	for (auto &it : peers) {
		Peer *peer = it.second.get();
		if (peer->loaded && peer != exclude) {
			sendSpawnParticle(peer, position, velocity, material);
		}
	}
}
