#pragma once
#include "../../network/protocol.hpp"
#include "../../network/packets.hpp"
#include "../../network/net.hpp"
#include "../../content/content.hpp"
#include "content/Inventory.hpp"
#include "world/World.hpp"
#include "ServerPlayer.hpp"
#include <string>
#include <unordered_set>
#include <map>

namespace server {

class Peer {
	std::unordered_set<V3s> sent_chunks;
	std::unordered_set<int> sent_entities;
	std::unordered_set<int> sent_plots;

public:
	Peer(const std::string &name, network::Address address)
			: name(name), address(address), loaded(false),
			  player_info(nullptr) {}

	std::string name;
	const network::Address address;

	std::unordered_map<int, int> workClientToServer;
	std::unordered_map<int, int> workServerToClient;

	bool loaded;

	world::Entity *entity = nullptr;

	ServerPlayer *player_info;

	bool hasChunk(const V3s &cpos) const {
		return sent_chunks.find(cpos) != sent_chunks.end();
	}

	void setChunkSent(const V3s &cpos) { sent_chunks.insert(cpos); }

	bool hasEntity(const world::Entity *e) const {
		return e == entity || sent_entities.find(e->id) != sent_entities.end();
	}

	void setEntitySent(const world::Entity *e) { sent_entities.insert(e->id); }

	void unsetEntitySent(int id) { sent_entities.erase(id); }

	bool hasPlot(int plotId) const {
		return sent_plots.find(plotId) != sent_plots.end();
	}

	bool hasPlot(const world::Plot *plot) const { return hasPlot(plot->id); }

	void setPlotSent(const world::Plot *plot) { sent_plots.insert(plot->id); }
};

} // namespace server
