#pragma once

namespace server {

struct ServerSpec {
	bool is_singleplayer = false;
	bool shutdown_on_last_leave = false;
};

} // namespace server
