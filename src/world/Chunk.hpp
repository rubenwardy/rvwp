#pragma once

#include "types/types.hpp"
#include "content/ItemDef.hpp"
#include "Tile.hpp"
#include "Entity.hpp"

#include <list>
#include <vector>
#include <unordered_map>
#include <cassert>
#include <sanity.hpp>

namespace world {

#define CHUNK_SIZE 16

inline V3s PosToCPos(const V3f &pos) {
	return {(s32)floorf(pos.x / (f32)CHUNK_SIZE),
			(s32)floorf(pos.y / (f32)CHUNK_SIZE), pos.z};
}

inline V3s PosToCPos(const V3s &pos) {
	return PosToCPos(pos.floating());
}

inline V3s CPosToPos(const V3s &cpos) {
	V3s pos = cpos * CHUNK_SIZE;
	pos.z = cpos.z;
	return pos;
}

enum class LightChannel : u8 {
	ARTIFICIAL_BLUE = 0,
	ARTIFICIAL_GREEN,
	ARTIFICIAL_RED,
	SUNLIGHT
};

inline u8 GetLightChannel(u16 light, LightChannel channel) {
	u8 offset = static_cast<u8>(channel) * 4;
	return (u8)((light >> offset) & 0xF);
}

inline u8 GetSunlight(u16 light) {
	return GetLightChannel(light, LightChannel::SUNLIGHT);
}

inline u8 GetArtificialRed(u16 light) {
	return GetLightChannel(light, LightChannel::ARTIFICIAL_RED);
}

inline void SetLightChannel(u16 &light, LightChannel channel, u8 value) {
	u8 offset = static_cast<u8>(channel) * 4;
	u16 mask = ~(0xF << offset);
	light = (u16)(light & mask) | ((u16)value << offset);
}

inline void SetSunlight(u16 &light, u8 value) {
	SetLightChannel(light, LightChannel::SUNLIGHT, value);
}

inline void SetArtificialRed(u16 &light, u8 value) {
	SetLightChannel(light, LightChannel::ARTIFICIAL_RED, value);
}

static const V3s AROUND_2D[] = {
		V3s(-1, -1, 0),
		V3s(0, -1, 0),
		V3s(1, -1, 0),

		V3s(-1, 0, 0),
		V3s(1, 0, 0),

		V3s(-1, 1, 0),
		V3s(0, 1, 0),
		V3s(1, 1, 0),
};

enum ChunkState {
	CHUNK_NULL,	  // CHUNK isn't loaded
	CHUNK_DUMMY,  // CHUNK is loading, but has no data
	CHUNK_LOADED, // CHUNK is loaded, but can't receive updates
	CHUNK_ACTIVE  // CHUNK is active, and can update and be sent to clients
};

enum class ChunkLayer { Floor, Tile };

static inline ChunkLayer typeToChunkLayer(content::ItemType type) {
	switch (type) {
	case content::ItemType::Item:
		// fallthrough
	case content::ItemType::Terrain:
		FatalError("Item type cannot be placed as terrain");
	case content::ItemType::Floor:
		return ChunkLayer::Floor;
	case content::ItemType::Tile:
		return ChunkLayer::Tile;
	}

	FatalError("Unsupported type");
}

struct TileTimer {
	V3s position;
	ChunkLayer layer;
	std::string event;
	float expiresAt;
};

class Entity;

class WorldChunk {
public:
	explicit WorldChunk(const V3s &pos);
	WorldChunk(const WorldChunk &other) = delete;

	struct ChunkTileTimer {
		u8 index;
		ChunkLayer layer;
		std::string event;
		float expiresAt;
	};

	V3s pos;
	ChunkState _state = CHUNK_DUMMY;
	bool dirty = false;

	WorldTile tiles[CHUNK_SIZE * CHUNK_SIZE] = {};
	WorldTile floort[CHUNK_SIZE * CHUNK_SIZE] = {};
	content_id terrain[CHUNK_SIZE * CHUNK_SIZE] = {};

	u16 lightLevels[CHUNK_SIZE * CHUNK_SIZE] = {};

	std::list<Entity *> entities;

	std::unordered_map<u16, std::shared_ptr<content::KeyValueStore>> meta;

	std::vector<ChunkTileTimer> timers;

	void clear();

	bool isFloorSolid() const;
	bool isSolid() const;

	inline u8 getIndex(const V3s &tpos) const {
		int ix = tpos.x - CHUNK_SIZE * pos.x;
		int iy = tpos.y - CHUNK_SIZE * pos.y;
		int idx = ix + iy * CHUNK_SIZE;

		assert(idx >= 0 && idx < CHUNK_SIZE * CHUNK_SIZE);
		return idx;
	}

	inline WorldTile *getInternal(u8 idx, ChunkLayer layer) {
		switch (layer) {
		case ChunkLayer::Tile:
			return &tiles[idx];
		case ChunkLayer::Floor:
			return &floort[idx];
		}

		FatalError("Unknown chunk layer")
	}

	inline int getLightIndex(const V3s &tpos) const { return getIndex(tpos); }

	/// Get artificial relative to chunk
	///
	/// @param rx Relative to chunk x index
	/// @param ry Relative to chunk y index
	/// @return
	inline int getArtificial(int rx, int ry) const {
		assert(rx >= 0 && ry >= 0 && rx < CHUNK_SIZE && ry < CHUNK_SIZE);
		return GetArtificialRed(lightLevels[rx + ry * CHUNK_SIZE]);
	}

	/// Get sunlight relative to chunk
	///
	/// @param rx Relative to chunk x index
	/// @param ry Relative to chunk y index
	/// @return
	inline u8 getSunlight(int rx, int ry) const {
		assert(rx >= 0 && ry >= 0 && rx < CHUNK_SIZE && ry < CHUNK_SIZE);
		return GetSunlight(lightLevels[rx + ry * CHUNK_SIZE]);
	}

	/// Get artificial relative to world
	///
	/// @param pos Absolute position
	/// @return
	inline u8 getArtificialAbs(const V3s &pos) const {
		return GetArtificialRed(lightLevels[getLightIndex(pos)]);
	}

	/// Get sunlight relative to world
	///
	/// @param pos Absolute position
	/// @return
	inline u8 getSunlightAbs(const V3s &tpos) const {
		return GetSunlight(lightLevels[getLightIndex(tpos)]);
	}

	/// Get metadata
	///
	/// @param idx Internal index
	/// @param layer Layer
	/// @return the meta data
	std::optional<std::shared_ptr<content::KeyValueStore>>
	getMetaDataInternalNoCreate(u8 idx, ChunkLayer layer) {
		u16 key = (idx << 1u) + (u16)layer;

		auto it = meta.find(key);
		if (it != meta.end()) {
			return it->second;
		}

		return {};
	}

	/// Get metadata
	///
	/// @param idx Internal index
	/// @param layer Layer
	/// @return the meta data
	std::optional<std::shared_ptr<content::KeyValueStore>> getMetaDataInternal(
			u8 idx, ChunkLayer layer) {
		u16 key = (idx << 1u) + (u16)layer;

		auto it = meta.find(key);
		if (it != meta.end()) {
			return it->second;
		}

		if (getInternal(idx, layer)->cid == 0) {
			return {};
		}

		meta[key] = std::make_shared<content::KeyValueStore>();
		return meta[key];
	}

	/// Get metadata
	///
	/// @param pos Absolute position
	/// @param layer Layer
	/// @return the meta data
	inline std::optional<std::shared_ptr<content::KeyValueStore>> getMetaData(
			const V3s &tpos, ChunkLayer layer) {
		return getMetaDataInternal(getIndex(tpos), layer);
	}

	const ChunkState &getState() const { return _state; }
	bool isDummy() const { return _state == CHUNK_DUMMY; }
	bool isLoaded() const { return _state >= CHUNK_LOADED; }
	bool isActive() const { return _state >= CHUNK_ACTIVE; }

	void bumpState(ChunkState minState) {
		if (_state < minState) {
			_state = minState;
		}
	}

	void printLightmap() const;

	/// Adds a tile timer to the chunk
	///
	/// Returns true if the timer was created/updated successfully.
	/// Returns false if there was an existing timer with an earlier expiresAt.
	///
	/// @param timer The timer
	/// @return success boolean
	[[nodiscard]] bool pushTimer(const TileTimer &timer);

	/// Return expired timers, removing them from the chunk
	void popExpiredTimers(
			std::vector<TileTimer> &expiredTimers, float gameTime);
};

} // end namespace world
