#include "LightCalculator.hpp"

#include "World.hpp"

using namespace world;

void LightCalculator::initialSpread(WorldChunk *chunk) {
	std::queue<LightNode> queue;

	for (int idx = 0; idx < CHUNK_SIZE * CHUNK_SIZE; idx++) {
		if (GetArtificialRed(chunk->lightLevels[idx]) > 1) {
			queue.emplace(idx, chunk, LightChannel::ARTIFICIAL_RED, 0);
		}

		if (GetSunlight(chunk->lightLevels[idx]) > 1) {
			queue.emplace(idx, chunk, LightChannel::SUNLIGHT, 0);
		}
	}

	spreadChannel(queue);
}

void LightCalculator::spreadArtificial(WorldChunk *chunk, const V3s &pos) {
	assert(chunk->isActive());

	std::queue<LightNode> queue;
	queue.emplace(chunk->getIndex(pos), chunk, LightChannel::ARTIFICIAL_RED, 0);
	spreadChannel(queue);
}

void LightCalculator::removeArtificial(WorldChunk *chunk, const V3s &pos) {
	assert(chunk->isActive());

	std::queue<LightNode> queue;
	int idx = chunk->getIndex(pos);
	queue.emplace(idx, chunk, LightChannel::ARTIFICIAL_RED,
			GetArtificialRed(chunk->lightLevels[idx]));
	SetLightChannel(chunk->lightLevels[idx], LightChannel::ARTIFICIAL_RED, 0);
	removeChannel(queue);
}

void LightCalculator::spreadSunlight(WorldChunk *chunk, const V3s &pos) {
	assert(chunk->isActive());

	std::queue<LightNode> queue;
	queue.emplace(chunk->getIndex(pos), chunk, LightChannel::SUNLIGHT, 0);
	spreadChannel(queue);
}

void LightCalculator::removeSunlight(WorldChunk *chunk, const V3s &pos) {
	assert(chunk->isActive());

	std::queue<LightNode> queue;
	int idx = chunk->getIndex(pos);
	queue.emplace(idx, chunk, LightChannel::SUNLIGHT,
			GetSunlight(chunk->lightLevels[idx]));
	SetLightChannel(chunk->lightLevels[idx], LightChannel::SUNLIGHT, 0);
	removeChannel(queue);
}

void LightCalculator::spreadEdges(WorldChunk *chunk) {
	assert(world);
	assert(chunk->isActive());

	std::queue<LightNode> queue;

	V3s pos = chunk->pos * CHUNK_SIZE;
	V3s delta{1, 0, 0};

	for (int side = 0; side < 4; side++) {
		for (int i = 1; i < CHUNK_SIZE; i++) {
			u8 lightLevel = GetArtificialRed(
					chunk->lightLevels[chunk->getLightIndex(pos)]);
			if (lightLevel > 0) {
				queue.emplace(chunk->getIndex(pos), chunk,
						LightChannel::ARTIFICIAL_RED, lightLevel);
			}
			pos += delta;
		}

		// Turn right
		int tmp = delta.x;
		delta.x = -delta.y;
		delta.y = tmp;
	}

	// Propagate sunlight
	WorldChunk *below = world->getChunk(chunk->pos + V3s(0, 0, -1));
	if (below) {
		for (int idx = 0; idx < CHUNK_SIZE * CHUNK_SIZE; idx++) {
			if (chunk->terrain[idx] != 1 || !chunk->floort[idx].empty() ||
					!canPropagateLight(below->tiles[idx].cid)) {
				continue;
			}

			u8 aboveSunlight = GetSunlight(chunk->lightLevels[idx]);
			if (aboveSunlight > 0 && aboveSunlight != 0xF) {
				aboveSunlight--;
			}

			u8 sunlight = GetSunlight(below->lightLevels[idx]);
			if (aboveSunlight > 1 && aboveSunlight > sunlight) {
				SetSunlight(below->lightLevels[idx], aboveSunlight);
				queue.emplace(idx, below, LightChannel::SUNLIGHT, 0);
			}
		}
	}

	spreadChannel(queue);
}

void LightCalculator::spreadChannel(std::queue<LightNode> &queue) {
	while (!queue.empty()) {
		const LightNode &node = queue.front();
		const int sourceIdx = node.index;
		WorldChunk *sourceChunk = node.chunk;
		const LightChannel channel = node.channel;
		queue.pop();

		const int sourceX = sourceIdx % CHUNK_SIZE;
		const int sourceY = sourceIdx / CHUNK_SIZE;
		assert(sourceIdx >= 0 && sourceIdx < 256);
		assert(sourceX >= 0 && sourceX < CHUNK_SIZE && sourceY >= 0 &&
				sourceY < CHUNK_SIZE);

		u16 sourceLight = sourceChunk->lightLevels[sourceIdx];
		u8 sourceValue = GetLightChannel(sourceLight, channel);
		if (sourceValue <= 0) {
			continue;
		}

		for (const auto &delta : NEIGHBOURS_4) {
			WorldChunk *targetChunk = sourceChunk;
			int targetX = sourceX + delta.x;
			int targetY = sourceY + delta.y;

			// Get new target sourceChunk if out of bounds
			if (targetX < 0 || targetY < 0 || targetX >= CHUNK_SIZE ||
					targetY >= CHUNK_SIZE) {
				// Skip target if no world provided
				if (!world)
					continue;

				targetChunk = world->getChunk(
						sourceChunk->pos + V3s(delta.x, delta.y, 0));
				assert(targetChunk && targetChunk->isLoaded());

				if (targetX < 0)
					targetX += CHUNK_SIZE;
				if (targetY < 0)
					targetY += CHUNK_SIZE;
				if (targetX >= CHUNK_SIZE)
					targetX -= CHUNK_SIZE;
				if (targetY >= CHUNK_SIZE)
					targetY -= CHUNK_SIZE;
			}

			const int targetIdx = targetX + targetY * CHUNK_SIZE;
			assert(targetX >= 0 && targetX < CHUNK_SIZE && targetY >= 0 &&
					targetY < CHUNK_SIZE);
			assert(targetIdx >= 0 && targetIdx < 256);

			// Check target tile, attempt spreading
			u8 targetValue = GetLightChannel(
					targetChunk->lightLevels[targetIdx], channel);
			if (canPropagateLight(targetChunk->tiles[targetIdx].cid) &&
					targetValue + (u8)1 < sourceValue) {
				SetLightChannel(targetChunk->lightLevels[targetIdx], channel,
						sourceValue - (u8)1);
				targetChunk->dirty = true;
				queue.emplace(targetIdx, targetChunk, channel, 0);
			}
		}
	}
}

void LightCalculator::removeChannel(std::queue<LightNode> &queue) {
	std::queue<LightNode> insert_queue;

	while (!queue.empty()) {
		const LightNode &node = queue.front();
		const int sourceIdx = node.index;
		WorldChunk *sourceChunk = node.chunk;
		const LightChannel channel = node.channel;
		const u8 sourceValue = node.val;
		queue.pop();

		const int sourceX = sourceIdx % CHUNK_SIZE;
		const int sourceY = sourceIdx / CHUNK_SIZE;
		assert(sourceIdx >= 0 && sourceIdx < 256);
		assert(sourceX >= 0 && sourceX < CHUNK_SIZE && sourceY >= 0 &&
				sourceY < CHUNK_SIZE);

		if (world && channel == LightChannel::SUNLIGHT) {
			WorldChunk *aboveChunk = world
					? world->getChunk(sourceChunk->pos + V3s(0, 0, 1))
					: nullptr;
			if (aboveChunk && aboveChunk->floort[sourceIdx].empty() &&
					aboveChunk->terrain[sourceIdx] <= CID_VOID) {
				u8 aboveValue = GetLightChannel(
						aboveChunk->lightLevels[sourceIdx], channel);
				if (aboveValue > 0 && aboveValue != 0xF) {
					aboveValue--;
				}

				SetLightChannel(sourceChunk->lightLevels[sourceIdx], channel,
						aboveValue);
				insert_queue.emplace(
						sourceIdx, sourceChunk, channel, aboveValue);
			}
		}

		for (const auto &delta : NEIGHBOURS_4) {
			WorldChunk *targetChunk = sourceChunk;
			int targetX = sourceX + delta.x;
			int targetY = sourceY + delta.y;

			// Get new target sourceChunk if out of bounds
			if (targetX < 0 || targetY < 0 || targetX >= CHUNK_SIZE ||
					targetY >= CHUNK_SIZE) {
				// Skip target if no world provided
				if (!world)
					continue;

				targetChunk = world->getChunk(
						sourceChunk->pos + V3s(delta.x, delta.y, 0));
				assert(targetChunk && targetChunk->isLoaded());

				if (targetX < 0)
					targetX += CHUNK_SIZE;
				if (targetY < 0)
					targetY += CHUNK_SIZE;
				if (targetX >= CHUNK_SIZE)
					targetX -= CHUNK_SIZE;
				if (targetY >= CHUNK_SIZE)
					targetY -= CHUNK_SIZE;
			}

			const int targetIdx = targetX + targetY * CHUNK_SIZE;
			assert(targetX >= 0 && targetX < CHUNK_SIZE && targetY >= 0 &&
					targetY < CHUNK_SIZE);
			assert(targetIdx >= 0 && targetIdx < 256);

			const u8 targetValue = GetLightChannel(
					targetChunk->lightLevels[targetIdx], channel);
			if (targetValue > 0 && targetValue < sourceValue) {
				SetLightChannel(
						targetChunk->lightLevels[targetIdx], channel, 0);
				targetChunk->dirty = true;
				queue.emplace(targetIdx, targetChunk, channel, targetValue);
			} else if (targetValue >= sourceValue) {
				insert_queue.emplace(targetIdx, targetChunk, channel, 0);
			}
		}
	}

	spreadChannel(insert_queue);
}

bool LightCalculator::canPropagateLight(content_id id) {
	if (id == 0) {
		return true;
	}

	auto def = definitionManager->getTileDef(id);
	return def && def->lightPropagates;
}
