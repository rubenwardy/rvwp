#pragma once

#include "Pathfinder.hpp"
#include "World.hpp"
#include "Raycast.hpp"

#include <unordered_set>
#include <memory>

namespace world {

class AStarPathfinder : public Pathfinder {
	struct Node {
		Node *parent = nullptr;
		V3s pos;
		float costToHere;
		float estimatedCostToGoal;
		float cost() const { return costToHere + estimatedCostToGoal; }

		Node(const V3s &pos, float costToHere, float estimatedCostToGoal,
				Node *parent = nullptr)
				: parent(parent), pos(pos), costToHere(costToHere),
				  estimatedCostToGoal(estimatedCostToGoal) {}

		Node(const Node &node) = delete;
	};

	World *world;
	Raycast raycast;

	std::vector<std::unique_ptr<Node>> nodes;
	std::vector<Node *> open;
	V3s goal;
	Settings settings{};

	// Node has been processed and neighbours checked
	std::unordered_set<V3s> checked;

public:
	explicit AStarPathfinder(World *world) : world(world), raycast(world) {
		raycast.tile_collision = true;
		raycast.entity_collision = false;
	}
	~AStarPathfinder() override = default;

	[[nodiscard]] bool findPath(std::vector<V3s> &path, const V3s &from,
			const V3s &to, const Settings &set) override;

private:
	float estimateCostToGoal(const V3s &pos) { return pos.sqDistance(goal); }

	bool findPathImpl(std::vector<V3s> &path, const V3s &from);

	/// fromPos will be different to from->pos when a jump is being attempted.
	/// from is the last node in the path, whereas fromPos is the base position
	/// when looking for neighbours.
	///
	/// @param node
	/// @param fromPos
	/// @param baseWeight The base weight, which will be >1 when jumping.
	void checkNeighbours(
			Node *node, const V3s &fromPos, const float baseWeight);

	void add(Node *parent, const V3s &pos, float cost) {
		nodes.push_back(std::make_unique<Node>(
				pos, cost, estimateCostToGoal(pos), parent));
		open.emplace_back(nodes[nodes.size() - 1].get());
	}

	bool isGoal(const V3s &pos) const;
};

} // namespace world
