#include "DebugUI.hpp"
#include <vector>
#include <cassert>
#include <atomic>

#ifdef DEBUG_UI_ENABLED

using namespace debug;

namespace {
/// Stores buffers
class DebugContext {
	bool flip = false;
	std::vector<DebugElement> a;
	std::vector<DebugElement> b;

public:
	std::shared_mutex mutex;
	std::atomic_bool isEnabled = true;

	std::vector<DebugElement> &getRead() { return flip ? a : b; }

	std::vector<DebugElement> &getWrite() { return flip ? b : a; }

	/// Thread-safe
	void swap() {
		std::lock_guard<std::shared_mutex> lock(mutex);

		flip = !flip;
		getWrite().clear();
	}
};

static DebugContext *context = nullptr;
} // namespace

bool DebugUI::isEnabled() {
	return context->isEnabled.load();
}

void DebugUI::setEnabled(bool enabled) {
	context->isEnabled.store(enabled);
}

void DebugUI::Init() {
	assert(context == nullptr);
	context = new DebugContext();
}

void DebugUI::CleanUp() {
	delete context;
	context = nullptr;
}

void DebugUI::addLabel(const V3f &pos, const std::string &text) {
	if (!isEnabled()) {
		return;
	}

	context->getWrite().push_back({DebugElementType::LABEL_3D, pos, {}, text});
}

void DebugUI::addLine(
		const V3f &from, const V3f &to, const std::string &color) {
	if (!isEnabled()) {
		return;
	}

	context->getWrite().push_back({DebugElementType::LINE, from, to, color});
}

void DebugUI::display() {
	if (!isEnabled()) {
		return;
	}

	context->swap();
}

DebugReader DebugUI::getReader() {
	std::shared_lock<std::shared_mutex> lock(context->mutex);
	return DebugReader(std::move(lock), context->getRead());
}
#endif
