#include "AStarPathfinder.hpp"
#include <queue>
#include <list>
#include <algorithm>

using namespace world;

#define Debug(...)
// LogF(__VA_ARGS__)

[[nodiscard]] bool AStarPathfinder::findPath(std::vector<V3s> &path,
		const V3s &from, const V3s &to, const Settings &set) {
	LogF(INFO, "Pathfinding from %d,%d,%d to %d,%d,%d", from.x, from.y, from.z,
			to.x, to.y, to.z);

	goal = to;
	settings = set;

	// Negative giveUpCosts means that it's a multiplier
	if (settings.giveUpCost <= 0) {
		float aerialDistance = V3s(from - to).length();
		settings.giveUpCost =
				std::max(settings.giveUpCost * -aerialDistance, 30.f);
	}

	assert(open.empty());
	assert(nodes.empty());
	assert(checked.empty());

	bool ret = findPathImpl(path, from);
	open.clear();
	nodes.clear();
	checked.clear();
	return ret;
}

bool AStarPathfinder::findPathImpl(std::vector<V3s> &path, const V3s &from) {
	Debug(ERROR, "Starting pathfinder");

	add(nullptr, from, 0);

	while (!open.empty()) {
		// Find and remove smallest item
		auto it = std::min_element(open.begin(), open.end(),
				[](auto a, auto b) { return a->cost() < b->cost(); });
		Node *node = *it;
		open.erase(it);

		Debug(ERROR, " - Checking node: %d, %d, %d", node->pos.x, node->pos.y,
				node->pos.z);

		// Assert not already checked
		assert(checked.find(node->pos) == checked.end());
		checked.insert(node->pos);

		// Have we reached the end?
		if (isGoal(node->pos)) {
			while (node) {
				path.emplace_back(node->pos);
				node = node->parent;
			}

			std::reverse(path.begin(), path.end());
			return true;
		}

		// Look at neighbours
		checkNeighbours(node, node->pos, 1);

		// Attempt to jump
		if (!world->hasAnything(node->pos + V3s(0, 0, 1))) {
			checkNeighbours(node, node->pos + V3s(0, 0, 1), 10);
		}
	}

	return false;
}

void AStarPathfinder::checkNeighbours(
		Node *from, const V3s &fromPos, const float baseWeight) {
	for (const auto &delta : NEIGHBOURS_4) {
		V3s pos = fromPos + V3s(delta.x, delta.y, 0);
		Debug(ERROR, "   - neighbour %d, %d, %d", pos.x, pos.y, pos.z);

		float weight = baseWeight;

		// Look for tile weight overrides
		bool tileOverridden = false;
		auto tile = world->get(pos, ChunkLayer::Tile);
		if (tile && !tile->empty()) {
			auto weightIt = settings.weights.find(tile->cid);
			if (weightIt != settings.weights.end()) {
				weight += weightIt->second;
				tileOverridden = true;
				if (weight > 100000.f) {
					continue;
				}
			}
		}

		// Check collision
		if (!tileOverridden &&
				world->collidesAt(fromPos.floating(), pos.floating())) {
			Debug(ERROR, "     - collides");
			continue;
		}

		float costToHere = from->costToHere + weight;

		// Check for falling
		if (!world->hasGround(pos)) {
			// Don't allow jumping if they can't stand
			if (fromPos.z != from->pos.z) {
				continue;
			}

			// Fall
			pos.z--;
			costToHere += 0.2f;

			if (!world->canStandAt(pos)) {
				continue;
			}
		}

		if (checked.find(pos) != checked.end()) {
			Debug(ERROR, "     - already checked");
			continue;
		}

		if (costToHere > settings.giveUpCost) {
			Debug(ERROR, "     - giving up!");
			continue;
		}

		// Check open nodes
		auto found = std::find_if(open.begin(), open.end(),
				[&pos](auto a) { return a->pos == pos; });

		if (found != open.end() && (*found)->costToHere <= costToHere) {
			Debug(ERROR, "     - not updating existing node (%.0f < %.0f)",
					(*found)->costToHere, costToHere);
			continue;
		}

		Node *parent = from;
		if (parent->parent && pos.z == parent->parent->pos.z &&
				!raycast.trace_to(
						parent->parent->pos.floating(), pos.floating())) {
			parent = parent->parent;
		}

		if (found == open.end()) {
			Debug(ERROR, "     - adding node to open");
			add(parent, pos, costToHere);
		} else {
			(*found)->costToHere = costToHere;
			(*found)->parent = parent;
			Debug(ERROR, "     - updating existing node (%.0f > %.0f)",
					(*found)->costToHere, costToHere);
		}
	}
}

bool AStarPathfinder::isGoal(const V3s &pos) const {
	if (pos.z != goal.z && (!settings.acceptBelow || pos.z != goal.z - 1)) {
		return false;
	}

	if (settings.acceptExactTarget && pos.x == goal.x && pos.y == goal.y) {
		return true;
	}

	return settings.acceptNeighbours &&
			((pos.y == goal.y && abs(pos.x - goal.x) == 1) ||
					(pos.x == goal.x && abs(pos.y - goal.y) == 1));
}
