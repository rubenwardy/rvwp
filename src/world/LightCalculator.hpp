#pragma once

#include "types/Vector.hpp"
#include "Chunk.hpp"
#include "content/DefinitionManager.hpp"

#include <queue>

namespace world {

class World;

/// Contains light propagation algorithms
///
/// See:
/// https://www.seedofandromeda.com/blogs/29-fast-flood-fill-lighting-in-a-blocky-voxel-game-pt-1
/// https://www.seedofandromeda.com/blogs/30-fast-flood-fill-lighting-in-a-blocky-voxel-game-pt-2
class LightCalculator {
	struct LightNode {
		LightNode(int indx, WorldChunk *ch, LightChannel channel, u8 val)
				: index(indx), chunk(ch), channel(channel), val(val) {}

		int index;
		WorldChunk *chunk;
		LightChannel channel;
		u8 val;
	};

	content::DefinitionManager *definitionManager;
	World *world;

public:
	/// Construct instance
	///
	/// @param world Nullable, if present light will spread between chunks
	explicit LightCalculator(content::DefinitionManager *definitionManager,
			World *world = nullptr)
			: definitionManager(definitionManager), world(world) {}

	/// Called after mapgen is completed, to set initial light values.
	///
	/// See spreadEdges, this will finish the light spreading when the chunk
	/// is activated.
	///
	/// @param chunk The chunk which is generated.
	void initialSpread(WorldChunk *chunk);

	/// Spread artificial light from this position. Light value must be
	/// set before calling this function. Light values will never decrease.
	///
	/// Will propagate into surrounding chunks if `world` was given to
	/// the constructor.
	///
	/// Must only be called in active chunks
	///
	/// @param chunk The chunk that contains the updated position.
	/// @param pos The position, absolute.
	void spreadArtificial(WorldChunk *chunk, const V3s &pos);

	/// Sets the artificial light at the position. Reduces light values,
	/// and then calls spreadArtificial to re-spread light when needed.
	///
	/// Will propagate into surrounding chunks if `world` was given to
	/// the constructor.
	///
	/// Must only be called in active chunks
	///
	/// @param chunk The chunk that contains the updated position.
	/// @param pos The position, absolute.
	void removeArtificial(WorldChunk *chunk, const V3s &pos);

	/// Spread sunlight from this position. Light value must be
	/// set before calling this function. Light values will never decrease.
	///
	/// Will propagate into surrounding chunks if `world` was given to
	/// the constructor.
	///
	/// Must only be called in active chunks
	///
	/// @param chunk The chunk that contains the updated position.
	/// @param pos The position, absolute.
	void spreadSunlight(WorldChunk *chunk, const V3s &pos);

	/// Sets the sunlight at the position. Reduces light values,
	/// and then calls spreadSunlight to re-spread light when needed.
	///
	/// Will propagate into surrounding chunks if `world` was given to
	/// the constructor.
	///
	/// Must only be called in active chunks
	///
	/// @param chunk The chunk that contains the updated position.
	/// @param pos The position, absolute.
	void removeSunlight(WorldChunk *chunk, const V3s &pos);

	/// Will spread light into surrounding chunks.
	///
	/// Must only be called in active chunks
	///
	/// @param chunk The chunk
	void spreadEdges(WorldChunk *chunk);

private:
	void spreadChannel(std::queue<LightNode> &queue);
	void removeChannel(std::queue<LightNode> &queue);

	bool canPropagateLight(content_id id);
};

} // namespace world
