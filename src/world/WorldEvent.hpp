#pragma once

#include "../types/types.hpp"

#include "Chunk.hpp"
#include "Plot.hpp"

namespace world {

class WorldChunk;
struct WorldEvent {
	enum Type {
		/// A chunk was added to the world.
		CHUNK_EMERGED,

		/// A chunk was activated.
		CHUNK_ACTIVATED,

		/// Set tile
		SET_TILE,

		/// A job was created
		WORK_CREATED,

		/// A job was created
		WORK_REMOVED
	};

	Type type;

	/// Absolute position
	V3s pos;
	ChunkLayer layer;

	WorldChunk *chunk = nullptr;

	int plotId;
	u32 workId;

	static WorldEvent chunkEmerged(WorldChunk *chunk) {
		return {CHUNK_EMERGED, chunk->pos, ChunkLayer::Tile, chunk, 0, 0};
	}

	static WorldEvent chunkActivated(WorldChunk *chunk) {
		return {CHUNK_ACTIVATED, chunk->pos, ChunkLayer::Tile, chunk, 0, 0};
	}

	static WorldEvent setTile(WorldChunk *chunk, V3s pos, ChunkLayer layer) {
		return {SET_TILE, pos, layer, chunk, 0, 0};
	}

	static WorldEvent workCreated(Plot *plot, Work *work) {
		return {WORK_CREATED, {}, ChunkLayer::Tile, nullptr, plot->id,
				work->id};
	}

	static WorldEvent workRemoved(Plot *plot, u32 workId) {
		return {WORK_REMOVED, {}, ChunkLayer::Tile, nullptr, plot->id, workId};
	}
};

} // namespace world
