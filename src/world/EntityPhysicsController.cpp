#include "EntityPhysicsController.hpp"
#include "World.hpp"

using namespace world;

void EntityPhysicsController::update(float dtime) {
	auto floor = world->getFloorTerrain(entity->getPosition().floor());

	// Accelerate
	velocity += moveVector * 16 * dtime;

	if (floor > CID_VOID) {
		auto def = world->getDefinitionManager()->getTileDef(floor);

		// Drag
		velocity *= 1.f - def->friction;

		// Increase friction on stopping
		if (moveVector.sqLength() < 0.01f) {
			velocity *= 0.9f;
		}
	}

	// Move each axis independently
	if (!entity->move(velocity.x * dtime, 0)) {
		velocity.x *= -0.2f;
	}
	if (!entity->move(0, velocity.y * dtime)) {
		velocity.y *= -0.2f;
	}
	entity->checkForFalling();
}
