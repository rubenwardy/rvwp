#pragma once

#include <shared_mutex>
#include <vector>

#include "types/types.hpp"

#if !(defined(NDEBUG) || defined(SERVER))
#	define DEBUG_UI_ENABLED
#endif

namespace debug {
enum class DebugElementType { LABEL_3D, LINE };

struct DebugElement {
	DebugElementType type;
	V3f pos;
	V3f pos2;
	std::string arg1;
};

#ifdef DEBUG_UI_ENABLED

class DebugReader {
	std::shared_lock<std::shared_mutex> lock;
	const std::vector<DebugElement> &elements;

public:
	DebugReader(std::shared_lock<std::shared_mutex> &&lock,
			const std::vector<DebugElement> &elements)
			: lock(std::move(lock)), elements(elements) {}

	explicit DebugReader(DebugReader &&other)
			: lock(std::move(other.lock)), elements(other.elements) {}

	DebugReader(const DebugReader &other) = delete;

	inline const std::vector<DebugElement> &get() { return elements; }
};

class DebugUI {
public:
	bool isEnabled();
	void setEnabled(bool enabled);

	/// Place label at 3d location
	/// @param pos Location
	/// @param text Label text
	void addLabel(const V3f &pos, const std::string &text);

	/// Place line
	/// @param from Start pos
	/// @param to End pos
	void addLine(const V3f &from, const V3f &to, const std::string &color);

	/// Switches buffers, displaying debug information on the client
	void display();

	DebugReader getReader();

	static void Init();
	static void CleanUp();
};

#else

class DebugReader {
	std::vector<DebugElement> elements;

public:
	inline const std::vector<DebugElement> &get() { return elements; }
};

class DebugUI {
public:
	inline bool isEnabled() { return false; }
	inline void setEnabled(bool enabled) {}
	inline void addLabel(const V3f &pos, const std::string &text) {}
	inline void addLine(
			const V3f &from, const V3f &to, const std::string &color) {}
	inline void display() {}
	inline DebugReader getReader() { return {}; }

	static void Init() {}
	static void CleanUp() {}
};

#endif
} // namespace debug
