#pragma once

#include "types/types.hpp"
#include "content/content.hpp"
#include "events/EventBus.hpp"

#include "Entity.hpp"
#include "Chunk.hpp"
#include "Plot.hpp"
#include "WorldEvent.hpp"

#include <memory>
#include <vector>
#include <map>
#include <unordered_map>
#include <limits>

namespace world {

struct WorldSector {
	std::vector<std::unique_ptr<WorldChunk>> chunks;
};

const std::array<V2s, 4> NEIGHBOURS_4 = {
		V2s(0, -1),
		V2s(1, 0),
		V2s(0, 1),
		V2s(-1, 0),
};

const std::array<V2s, 8> NEIGHBOURS_8 = {
		V2s(0, -1),
		V2s(1, -1),
		V2s(1, 0),
		V2s(1, 1),
		V2s(0, 1),
		V2s(-1, 1),
		V2s(-1, 0),
		V2s(-1, -1),
};

class World {
protected:
	content::DefinitionManager *defman;
	EventBus<WorldEvent> *bus;

	std::unordered_map<V2s, WorldSector> sectors;
	WorldChunk *chunkCache = nullptr;

	std::map<int, Entity *> entity_lookup;
	std::vector<std::unique_ptr<Entity>> entityList;

	/// Store entities that are in unloaded blocks
	std::list<Entity *> entity_insertion_queue;

	float nextTimerCheck = std::numeric_limits<float>::max();

	std::vector<std::unique_ptr<Plot>> plots;

	int entity_id_counter = 1;
	int plot_id_counter = 1;
	int work_id_counter = 1;

public:
	explicit World(content::DefinitionManager *defman,
			EventBus<WorldEvent> *bus = nullptr)
			: defman(defman), bus(bus) {}

	World(const World &that) = delete;

	content::DefinitionManager *getDefinitionManager() const { return defman; }

	/// Get a chunk by its position
	///
	/// @param cpos Chunk position. See PosToCPos
	/// @param optionally return the chunk state to a variable
	/// @return chunk or nullptr
	WorldChunk *getChunk(const V3s &cpos, ChunkState *state = nullptr);

	ChunkState getChunkState(const V3s &pos) {
		ChunkState state;
		getChunk(pos, &state);
		return state;
	}

	WorldChunk *getOrCreateChunk(const V3s &pos);

	void activateChunk(WorldChunk *c) {
		c->_state = CHUNK_ACTIVE;
		if (bus) {
			bus->push(WorldEvent::chunkActivated(c));
		}
	}

	WorldChunk *createChunkAndActivate(const V3s &pos) {
		assert(getChunk(pos) == nullptr);

		auto chunk = getOrCreateChunk(pos);
		activateChunk(chunk);
		return chunk;
	}

	void onChunkEmerge(WorldChunk *c);
	void getChunksInArea(std::vector<WorldChunk *> &res, const V3f &position,
			float radius, ChunkState minState = CHUNK_LOADED);
	int getChunkCount() const;

	void getDirtyChunks(std::vector<WorldChunk *> &res);

	void getNeighbouringChunks2d(std::vector<WorldChunk *> &res,
			const V3s &cpos, ChunkState minState = CHUNK_LOADED);
	void getNeighbouringChunks3d(std::vector<WorldChunk *> &res,
			const V3s &cpos, ChunkState minState = CHUNK_LOADED);

	[[nodiscard]] WorldTile *set(
			const V3s &pos, ChunkLayer layer, const WorldTile &tile);

	[[nodiscard]] inline WorldTile *set(
			const V3s &pos, ChunkLayer layer, content_id cid) {
		return set(pos, layer, WorldTile(cid));
	}

	WorldTile *get(
			const V3s &pos, ChunkLayer layer, ChunkState *state = nullptr);

	content_id getFloorTerrain(const V3s &pos, ChunkState *state = nullptr);

	/// Get metadata
	///
	/// @param pos
	/// @param layer
	/// @return
	std::optional<std::shared_ptr<content::KeyValueStore>> getMetaData(
			const V3s &pos, ChunkLayer layer);

	/// Where the position has ground, ignores tiles.
	bool hasGround(const V3s &pos);

	/// Whether an entity can move between from and to.
	bool collidesAt(const V3f &from, const V3f &to);

	/// Whether the position has ground or a tile.
	bool hasAnything(const V3s &pos);

	/// Whether an entity can stand at a position.
	bool canStandAt(const V3s &pos);

	/// Get the surface height (Z) at the specified position,
	/// returns an empty optional if no chunks are loaded.
	///
	/// @param pos Pos, z value is ignored
	/// @return Optional surface height
	std::optional<int> getSurfaceHeight(V3s pos);

	/// Find nodes with specified metadata key in range
	///
	/// @param pos
	/// @param range
	/// @param metadata
	void findNodesWithMetaData(std::vector<std::pair<V3s, WorldTile *>> &result,
			const V3f &pos, float range, ChunkLayer layer,
			const std::string &metadata);

	/// Adds a tile timer to the world
	///
	/// Returns true if the timer was created/updated successfully.
	/// Returns false if there was an existing timer with an earlier expiresAt.
	///
	/// @param timer The timer
	/// @return success boolean
	[[nodiscard]] bool pushTimer(const TileTimer &timer);

	/// Return expired timers, removing them from the world
	void popExpiredTimers(std::vector<TileTimer> &timers, float gameTime);

	// Entities

	std::vector<Entity *> getEntityList() {
		std::vector<Entity *> retval;
		retval.reserve(entityList.size());
		for (const auto &e : entityList) {
			retval.push_back(e.get());
		}
		return retval;
	}

	/// Create entity
	Entity *createEntity(const V3f &pos, const std::string &typeName,
			const content::Material &material = content::Material(),
			int id = -2);

	/// Removes an entity from the environment
	/// Removes from its chunk and the updating linked list.
	/// Deletes the entity
	void removeEntity(Entity *entity);

	/// Updates chunk/entity association, the entity must already be in the
	/// world.
	void reinsertEntityToChunk(Entity *entity);

	/// Looks up an entity by its id using entity_lookup.
	Entity *getEntityById(int id);

	/// Finds all entities at a position
	void getEntities(
			V3f pos, std::vector<Entity *> &entities, size_t giveup_count = 0);

	/// Finds all entities in a radius
	void getEntitiesInRange(
			const V3f &pos, std::vector<Entity *> &entities, float range);

	size_t getEntityInsertionQueueSize() const {
		return entity_insertion_queue.size();
	}

	/// Create a plot
	///
	/// @return plot with populated ID
	Plot *createPlot(
			const std::string &name, const Rect3s &bounds, int id = -1);

	/// Get plot at position
	Plot *getPlot(const V3s &pos);

	/// Get plot by ID
	Plot *getPlot(int id);

	/// Get plot by ID
	Plot *getPlotByWork(u32 workId);

	/// Get plots by position and range
	void getPlotsInRect(std::vector<Plot *> &res, const Rect3s &rect);

	/// Get work by ID
	Work *getWork(u32 workId);

	/// Create work, will assign an ID
	[[nodiscard]] Work *addWork(Plot *plot, std::unique_ptr<Work> &&work);

	[[nodiscard]] inline Work *addWork(
			int plotId, std::unique_ptr<Work> &&work) {
		auto plot = getPlot(plotId);
		if (!plot) {
			LogF(ERROR, "Unable to add work to unknown plot %d", plotId);
			return nullptr;
		}

		return addWork(plot, std::move(work));
	}

	/// Remove work by id
	[[nodiscard]] bool removeWork(u32 workId);
};

} // namespace world
