#include "SelectionSpec.hpp"
#include "Entity.hpp"

using namespace world;

std::ostream &world::operator<<(std::ostream &out, const SelectionSpec &spec) {
	switch (spec.type) {
	case SelectionSpec::SST_None:
		out << "None";
		break;
	case SelectionSpec::SST_Tile:
		out << "Tile(" << spec.pos.floor() << ")";
		break;
	case SelectionSpec::SST_Entity:
		out << "Entity("
			<< (spec.entity->getPosition() * 10.f).floor().floating() / 10.f
			<< ")";
		break;
	}

	return out;
}
