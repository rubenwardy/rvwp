#include "Raycast.hpp"

using namespace world;

inline bool checkCollision(content::DefinitionManager *definitionManager,
		content_id cid, V3f pos) {
	auto def = definitionManager ? definitionManager->getTileDef(cid) : nullptr;
	if (!def) {
		return true;
	}

	for (const auto &box : def->collisionBoxes) {
		if (box.contains(pos - pos.floor().floating())) {
			return true;
		}
	}

	return false;
}

SelectionSpec Raycast::trace(V3f from, V3f direction, float max_distance,
		float initial_jump_radius) {
	assert(direction.z == 0);
	assert(direction.isNormalised());

	auto definitionManager = world->getDefinitionManager();

	max_distance -= initial_jump_radius;

	V3f cursor(from + direction * initial_jump_radius);
	for (int i = 0; i < max_distance * 10; i++) {
		if (tile_collision) {
			WorldTile *tile = world->get(cursor.floor(), ChunkLayer::Tile);
			if (tile) {
				if (checkCollision(definitionManager, tile->cid, cursor)) {
					return SelectionSpec::makeTile(tile, cursor);
				}
			}
		}

		if (entity_collision) {
			std::vector<Entity *> entities;
			world->getEntities(cursor, entities, 2);
			for (auto &entity : entities) {
				if (entity != ignoredEntity) {
					return SelectionSpec::makeEntity(entity, cursor);
				}
			}
		}

		cursor += direction / 10.f;
	}

	return SelectionSpec::makeEmpty();
}
