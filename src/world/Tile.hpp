#pragma once

#include "../content/ItemDef.hpp"

namespace world {

class World;
class WorldChunk;
class WorldTile {
public:
	content_id cid = 0;

	WorldTile() = default;
	explicit WorldTile(content_id cid) : cid(cid) {}

	inline bool empty() const { return cid == 0; }

	explicit operator bool() const { return !empty(); }

	inline void clear() { cid = 0; }
};

} // namespace world
