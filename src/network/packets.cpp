#include "packets.hpp"
#include <sanity.hpp>
#include "readwrite.hpp"

#define WRITE_SPACE(sp)                                                        \
	if (data.size() < (sp) + cursor) {                                         \
		data.resize((sp) + cursor);                                            \
	}

#define CHECK_SPACE(sp) SanityCheck(data.size() - cursor >= (sp));

using namespace network;

Packet::Packet() : Packet(Address(), TONONE_nullptr, 0) {}

Packet::Packet(Address to, EPacketType type) : to(to), type(type), cursor(0) {
	data.resize(4 + 1);
	*this << (u32)PROTOCOL_IDENTIFIER;
	*this << (u8)type;
}

Packet::Packet(Address to, EPacketType type, size_t size)
		: to(to), type(type), cursor(0) {
	data.resize(size + 4 + 1);
	*this << (u32)PROTOCOL_IDENTIFIER;
	*this << (u8)type;
}

Packet::Packet(Address from, u8 *packet, size_t size)
		: to(from), type(TONONE_nullptr), cursor(0) {
	setData(packet, size);
}

void Packet::setData(u8 *packet, size_t size) {
	if (size < 5) {
		type = TONONE_nullptr;
		cursor = size;
		return;
	}

	// Copy Packet
	data.resize(size);
	cursor = 0;
	memcpy(&data[0], &packet[0], size);

	// Read Protocol
	u32 prot = 0;
	*this >> prot;
	if (prot != PROTOCOL_IDENTIFIER) {
		type = TONONE_nullptr;
		cursor = size;
		return;
	}

	// Read type
	u8 type8 = 0;
	*this >> type8;
	type = (EPacketType)type8;
}

//
// Integers
//

Packet &Packet::operator<<(bool i) {
	WRITE_SPACE(1);
	writeU8(&data[cursor], i ? 1 : 0);
	cursor++;

	return *this;
}

Packet &Packet::operator<<(u8 i) {
	WRITE_SPACE(1);
	writeU8(&data[cursor], i);
	cursor++;

	return *this;
}

Packet &Packet::operator<<(u16 i) {
	WRITE_SPACE(2);
	writeU16(&data[cursor], i);
	cursor += 2;

	return *this;
}

Packet &Packet::operator<<(u32 i) {
	WRITE_SPACE(4);
	writeU32(&data[cursor], i);
	cursor += 4;

	return *this;
}

Packet &Packet::operator<<(u64 i) {
	WRITE_SPACE(8);
	writeU64(&data[cursor], i);
	cursor += 8;

	return *this;
}

Packet &Packet::operator>>(bool &dst) {
	CHECK_SPACE(1);
	dst = readU8(&data[cursor]) != 0;
	cursor += 1;

	return *this;
}

Packet &Packet::operator>>(u8 &dst) {
	CHECK_SPACE(1);
	dst = readU8(&data[cursor]);
	cursor += 1;

	return *this;
}

Packet &Packet::operator>>(u16 &dst) {
	CHECK_SPACE(2);
	dst = readU16(&data[cursor]);
	cursor += 2;

	return *this;
}

Packet &Packet::operator>>(u32 &dst) {
	CHECK_SPACE(4);
	dst = readU32(&data[cursor]);
	cursor += 4;

	return *this;
}

Packet &Packet::operator>>(u64 &dst) {
	CHECK_SPACE(8);
	dst = readU64(&data[cursor]);
	cursor += 8;

	return *this;
}

//
// Floats
//

Packet &Packet::operator<<(const f32 &i) {
	WRITE_SPACE(4);
	writeF32(&data[cursor], i);
	cursor += 4;

	return *this;
}

Packet &Packet::operator>>(f32 &dst) {
	CHECK_SPACE(4);
	dst = readF32(&data[cursor]);
	cursor += 4;

	return *this;
}

//
// Vectors
//

Packet &Packet::operator<<(const V3f &i) {
	WRITE_SPACE(3 * 4);
	writeV3f(&data[cursor], i);
	cursor += 3 * 4;

	return *this;
}

Packet &Packet::operator<<(const V3s &i) {
	WRITE_SPACE(3 * 4);
	writeV3s(&data[cursor], i);
	cursor += 3 * 4;

	return *this;
}

Packet &Packet::operator<<(const Rect3s &i) {
	*this << i.minPos;
	*this << i.size;

	return *this;
}

Packet &Packet::operator<<(const Rect3f &i) {
	*this << i.minPos;
	*this << i.size;

	return *this;
}

Packet &Packet::operator>>(V3f &dst) {
	CHECK_SPACE(3 * 4);
	dst = readV3f(&data[cursor]);
	cursor += 3 * 4;

	return *this;
}

Packet &Packet::operator>>(V3s &dst) {
	CHECK_SPACE(3 * 4);
	dst = readV3s(&data[cursor]);
	cursor += 3 * 4;

	return *this;
}

Packet &Packet::operator>>(Rect3s &dst) {
	*this >> dst.minPos;
	*this >> dst.size;

	return *this;
}

Packet &Packet::operator>>(Rect3f &dst) {
	*this >> dst.minPos;
	*this >> dst.size;

	return *this;
}

//
// Strings
//

Packet &Packet::operator<<(const std::string &i) {
	WRITE_SPACE(i.size() + 2);
	writeString(&data[cursor], i);
	cursor += i.size() + 2;

	return *this;
}

Packet &Packet::operator>>(std::string &dst) {
	CHECK_SPACE(2);
	u16 len = readU16(&data[cursor]);
	cursor += 2;

	CHECK_SPACE(len);
	dst = std::string((char *)&data[cursor], len);
	cursor += len;

	return *this;
}
