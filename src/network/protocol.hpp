#pragma once

#include "../types/types.hpp"

static const u32 PROTOCOL_IDENTIFIER = 0x72767770;
static const u16 PROTOCOL_VERSION = 1;

enum EPacketType {
	TONONE_nullptr = 0,

	// strings are pascal strings:
	//    prefixed by u16 to represent length
	//    no trailing \0

	///////////////////////////////
	// HANDSHAKES AND CONNECTION //
	///////////////////////////////

	TOSERVER_HELLO,
	//    u16     protocol_version
	//    string  username
	//    string  hash

	TOCLIENT_HELLO,
	//    u16     protocol_version
	//    string  motd
	//    ...in future, anything to prepare client to accept data.

	TOCLIENT_READY,
	// the client can stop showing the loading screen now.
	//    empty

	TOANY_DISC,
	//    u8      type (0 - custom, 1 - auth)
	//    string  msg
	//    u8      reconnect  (0 - back to menu only.  1 - show button.  2 - auto
	//    reconnect)

	TOCLIENT_ITEM_DEFINITION,
	//    u8      type
	//    string  name
	//    string  title
	//    Mat     material
	// if weapon:
	//    u8      weaponType
	//    float   range
	//    u16     damage
	//    f32     useInterval
	//    f32[10] accuracy
	// if tile:
	//    cid     id
	//    bool    collides
	//    u8      lightSource
	//    bool    lightPropagates
	//    string  footstepSound
	//    float   friction
	//    u8      boxes
	//  for each collision box:
	//    Rect3f  box

	TOCLIENT_TOOL_DEFINITION,
	//    u8      type
	//    string  description
	//    string  itemName
	//    u8      selectType
	//    V3s     fixedSelectionSize
	//    Mat     material

	//////////////////////////////////
	// PLAYER PROFILE AND INVENTORY //
	//////////////////////////////////

	TOCLIENT_PROFILE,
	//    string  username
	//    V3f    position

	TOCLIENT_PLAYER_INVENTORY,
	//    Inv     inventory

	TOSERVER_PLAYER_INVENTORY_COMMAND,
	//    u8      command type
	//    string  from inv
	//    string  from list
	//    u16      from idx
	//    string  to inv
	//    string  to list
	//    u16      to idx
	//    u16     count

	TOANY_CHAT_MESSAGE,
	//    string  message

	///////////////////////////
	// WORLD AND ENVIRONMENT //
	///////////////////////////

	TOCLIENT_SET_TIME,
	//    f32     timeSinceBeginning
	//    f32     timeOfDay
	//    f32     speed

	TOCLIENT_WORLD_BLOCK,
	//    V3f    position
	//  for each tile:
	//    u16     content id
	//    u8      hitpoints
	//  for each floor:
	//    u16     content id
	//    u8      hitpoints
	//  for each terrain:
	//    u16     terrain id
	//  for each tile:
	//	  u8      light level

	TOCLIENT_WORLD_NEW_ENTITIES,
	// for each entity:
	//    u32     id
	//    u8      type
	//    V3f     position
	//    f32     yaw
	//    u16     hitpoints
	//    Mat     material
	//    f32     radius
	//    bool    footsteps

	TOCLIENT_WORLD_UPDATE_ENTITIES,
	// for each entity:
	//    u32     entityId
	//    for each property:
	//      u16 propertyId + 1   (0 to terminate)
	//      var data

	TOCLIENT_WORLD_DELETE_ENTITY,
	//    u32     id

	TOCLIENT_WORLD_GOTO,
	//    V3f     position

	TOSERVER_PLAYER_MOVE,
	//    V3f     new_position
	//    f32     yaw
	//    u32     update_idx
	//    u8      keys
	//       - up, down, left, right, space, use, sprint, <not-used>.
	//    V3f    camera position

	TOSERVER_TILE_INTERACT,
	//    V3f    position of tile

	TOSERVER_PUNCH_ENTITY,
	//    u32     id
	//    u32     update_idx
	//    u8      hotbar_idx
	//    u16     damage done
	//    u16     expected after hp

	TOANY_WORLD_SET_TILE,
	//    V3f     position
	//    u32     content id
	//    u8      hotbar id

	TOCLIENT_WORLD_PLOT,
	//    u32     id
	//    string  name
	//    u16     number of boxes
	// for each box
	//    Rect3s  box

	TOANY_WORLD_NEW_WORK,
	//    u32     senderId
	//    u32     plotId
	//    string  type
	//    Rect3s  bounds
	//    Mat     material
	//    string  itemName
	//    u8      layer

	TOANY_WORLD_REMOVE_WORK,
	//    u32     serverId
	//    u32     clientId

	TOSERVER_FIRE_WEAPON,
	//    V3f     position
	//    f32     yaw
	//    u8      hotbar_idx

	TOCLIENT_SPAWN_PARTICLE,
	//    V3f     position
	//    V3f     velocity
	//    Mat     material

	NUM_PACKET_TYPES,
};
