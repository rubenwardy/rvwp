#pragma once

#include "types/types.hpp"
#include <string>
#include <vector>
#include <memory>
#include <unordered_map>

#include "ItemDef.hpp"
#include "ToolSpec.hpp"

namespace content {

/// @brief Content Definition Manager, used to store defined items.
///
/// All defined things are items. Every item has a unique name.
/// All items have a unique name which is used to make looking up tiles more
/// readable.
///
/// Tiles are items which can be placed in the world. Tiles have a unique id
/// called a content_id.
class DefinitionManager {
	bool locked = true;
	content_id contentIdCounter = 0;
	std::vector<std::unique_ptr<ItemDef>> items;
	std::unordered_map<std::string, ItemDef *> items_by_name;
	std::unordered_map<content_id, TileDef *> tiles_by_cid;
	std::vector<ToolSpec> tool_specs;

public:
	DefinitionManager() = default;
	DefinitionManager(const DefinitionManager &that) = delete;

	void defineVoid();
	inline void lock() { locked = true; }
	inline void unlock() { locked = false; }
	inline bool isLocked() const { return locked; }
	inline int size() const { return (int)contentIdCounter; }

	ItemDef *registerItem(ItemDef *def);
	TileDef *registerTile(TileDef *def);

	inline bool exists(const std::string &name) {
		return getItemDef(name) != nullptr;
	}
	inline bool exists(content_id id) { return getTileDef(id) != nullptr; }

	ItemDef *getItemDef(const std::string &name);
	TileDef *getTileDef(content_id id);
	TileDef *getTileDef(const std::string &name);

	/// Gets the content_id from a tile's item name
	/// @param name Item name
	/// @return content_id, 0 if not found
	content_id getCidFromName(const std::string &name);

	void registerToolSpec(const ToolSpec &spec) { tool_specs.push_back(spec); }
	const std::vector<ToolSpec> &getToolSpecs() const { return tool_specs; }

	class iterator {
		std::vector<std::unique_ptr<ItemDef>>::iterator source;

	public:
		explicit iterator(
				std::vector<std::unique_ptr<ItemDef>>::iterator source)
				: source(source) {}

		iterator &operator++() {
			source++;
			return *this;
		}

		iterator operator++(int) {
			iterator retval = *this;
			++(*this);
			return retval;
		}

		bool operator==(iterator other) const { return source == other.source; }
		bool operator!=(iterator other) const { return !(*this == other); }

		ItemDef *operator*() { return source->get(); }

		// iterator traits
		using difference_type = ItemDef;
		using value_type = ItemDef *;
		using pointer = const ItemDef **;
		using reference = const ItemDef *&;
		using iterator_category = std::forward_iterator_tag;
	};

	iterator begin() { return iterator{items.begin()}; }
	iterator end() { return iterator{items.end()}; }
};

} // namespace content
