#include "Inventory.hpp"
#include <cassert>
#include <log.hpp>

using namespace content;

bool InventoryList::equals(const InventoryList &other) const {
	if (w != other.w || h != other.h || name != other.name) {
		return false;
	}

	for (size_t i = 0; i < items.size(); i++) {
		if (items[i] != other.items[i]) {
			return false;
		}
	}

	return true;
}

ItemStack InventoryList::addItem(ItemStack item) {
	int stack_max = 100;

	for (int i = 0; i < w * h && item.count > 0; ++i) {
		if (items[i].empty() || items[i].name == item.name) {
			items[i].name = item.name;
			int to_add = stack_max - items[i].count;
			if (to_add > item.count)
				to_add = item.count;
			item.count -= to_add;
			items[i].count += to_add;
		}
	}
	return item;
}

ItemStack InventoryList::takeItem(ItemStack item) {
	ItemStack ret;
	ret.name = item.name;

	for (int i = 0; i < w * h && item.count > 0; ++i) {
		if (items[i].name == item.name) {
			int to_take = items[i].count;
			if (to_take > item.count)
				to_take = item.count;
			item.count -= to_take;
			ret.count += to_take;
			items[i].count -= to_take;

			assert(items[i].count >= 0);
			if (items[i].count == 0)
				items[i].name = "";
		}
	}

	return ret;
}

InventoryList *Inventory::newList(const std::string &name, int w, int h) {
	lists[name] = std::make_unique<InventoryList>(location, name, w, h);
	return lists[name].get();
}

bool Inventory::equals(const Inventory &other) const {
	if (lists.size() != other.lists.size()) {
		return false;
	}

	for (const auto &item : lists) {
		auto otheritem = other.lists.find(item.first);
		if (otheritem == other.lists.end() ||
				!item.second->equals(*otheritem->second)) {
			return false;
		}
	}

	return true;
}

int Inventory::serialised_size() {
	size_t size = 0;

	size += 2; // number of lists

	for (const auto &pair : lists) {
		InventoryList *list = pair.second.get();
		assert(list);

		size += 2 + pair.first.size(); // inventory name
		size += 1 + 1;				   // inventory size

		size += list->w * list->h * sizeof(u16);

		for (const auto &item : list->items) {
			size += item.name.size() + 2;
		}
	}

	return (int)size;
}

void Inventory::serialise(network::Packet &pkt) {
	pkt << (u16)lists.size();
	for (const auto &pair : lists) {
		InventoryList *list = pair.second.get();
		pkt << pair.first;
		pkt << (u8)list->w;
		pkt << (u8)list->h;

		assert(list->items.size() == (size_t)list->w * list->h);
		for (const auto &item : list->items) {
			static_assert(
					sizeof(content_id) == sizeof(u16), "assumption on CID");
			pkt << item.name;
			pkt << (u16)item.count;
		}
	}
}

void Inventory::deserialise(network::Packet &pkt) {
	u16 number_of_lists;
	pkt >> number_of_lists;

	for (int i = 0; i < number_of_lists; i++) {
		std::string listName;
		u8 w, h;
		pkt >> listName;
		pkt >> w;
		pkt >> h;

		InventoryList *list = newList(listName, w, h);

		for (int j = 0; j < w * h; j++) {
			std::string itemName;
			u16 count;
			pkt >> itemName;
			pkt >> count;

			list->set(j, ItemStack(itemName, count));
		}
	}
}
