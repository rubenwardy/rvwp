#pragma once

#include "Material.hpp"
#include "WeaponSpec.hpp"

#include <optional>

using content_id = u16;

/// Void is a colliding invisible tile
constexpr const content_id CID_VOID = 1;

namespace content {

enum class ItemType { Item = 1, Terrain, Floor, Tile };

static inline std::optional<ItemType> strToItemType(const std::string &str) {
	if (str == "item") {
		return ItemType::Item;
	} else if (str == "terrain") {
		return ItemType::Terrain;
	} else if (str == "floor") {
		return ItemType::Floor;
	} else if (str == "tile") {
		return ItemType::Tile;
	} else {
		return {};
	}
}

struct ItemDef {
	ItemDef(ItemType type = ItemType::Item) : type(type) {}
	virtual ~ItemDef() = default;

	ItemType type = ItemType::Item;
	std::string name = "";
	std::string title = "";
	Material material = {};
	std::optional<WeaponSpec> weapon_spec = {};
	std::vector<Rect3f> collisionBoxes = {Rect3f({0, 0, 0}, {1, 1, 1})};

	/// Sets the weapon spec only if the type is not None
	/// @param spec
	void setWeaponSpec(const WeaponSpec &spec) {
		if (spec.type != WeaponSpec::Type::None) {
			weapon_spec = spec;
		} else {
			weapon_spec = {};
		}
	}

	virtual ItemType getType() const { return ItemType::Item; }
};

struct TileDef : public ItemDef {
	TileDef(ItemType type = ItemType::Tile) : ItemDef(type) {}
	~TileDef() override = default;

	content_id id = 0;
	bool collides = true;
	u8 lightSource = 0;
	bool lightPropagates = false;
	std::string footstepSound = "";
	f32 friction = 0.8f;

	ItemType getType() const override { return type; }
};

} // namespace content
