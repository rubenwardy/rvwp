#pragma once

#include "Inventory.hpp"

namespace content {

class InventoryCommand {
public:
	enum Type { SWAP, MOVE };

private:
	Type type;
	StackLocation source;
	StackLocation dest;
	int count;

public:
	InventoryCommand() : type(SWAP), source(), dest(), count(0) {}

	InventoryCommand(Type type, const StackLocation &source,
			const StackLocation &dest, int count)
			: type(type), source(source), dest(dest), count(count) {}

	inline Type getType() const { return type; }
	inline const StackLocation &getSource() const { return source; }
	inline const StackLocation &getDestination() const { return dest; }
	inline int getCount() const { return count; }

	[[nodiscard]] bool perform(const InventoryProvider &provider);
};

} // namespace content
