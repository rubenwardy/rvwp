#pragma once

#include <unordered_map>
#include <string>
#include <optional>

namespace content {

class KeyValueStore {
	std::unordered_map<std::string, std::string> values;

public:
	template <typename T>
	std::optional<T> get(const std::string &key) const;

	template <typename T>
	void set(const std::string &key, const T &value) {
		values[key] = std::to_string(value);
	}

	bool has(const std::string &key) const {
		return values.find(key) != values.end();
	}

	using const_iterator =
			std::unordered_map<std::string, std::string>::const_iterator;

	const_iterator begin() { return values.begin(); }

	const_iterator end() { return values.end(); }
};

template <>
inline void KeyValueStore::set(const std::string &key, const bool &value) {
	values[key] = value ? "true" : "false";
}

template <>
inline void KeyValueStore::set(
		const std::string &key, const std::string &value) {
	values[key] = value;
}

} // namespace content
