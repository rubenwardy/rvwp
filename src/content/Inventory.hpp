#pragma once

#include "content.hpp"
#include "ItemStack.hpp"

#include <string>
#include <vector>
#include <map>
#include <functional>

namespace content {

struct InventoryLocation {
	std::string name;

	bool operator==(const InventoryLocation &other) const {
		return name == other.name;
	}

	static InventoryLocation FromPlayer(const std::string &username) {
		return {"player:" + username};
	}
};

struct StackLocation {
	InventoryLocation inv;
	std::string list;
	int idx;

	bool operator==(const StackLocation &other) const {
		return inv == other.inv && list == other.list && idx == other.idx;
	}
};

class InventoryList {
	InventoryLocation parent;

public:
	std::string name;
	int w;
	int h;
	std::vector<ItemStack> items;

	InventoryList(const InventoryLocation &parent, const std::string &name,
			int w, int h)
			: parent(parent), name(name), w(w), h(h) {
		items.resize(w * h);
	}
	InventoryList(const InventoryList &that) = delete;

	inline ItemStack &get(int i) { return items[i]; }

	inline int calcIdx(int x, int y) const {
		assert(x >= 0 && x < w);
		assert(y >= 0 && y < h);
		return x + y * w;
	}

	inline ItemStack &get(int x, int y) { return get(calcIdx(x, y)); }

	inline const InventoryLocation &getParent() const { return parent; }

	inline StackLocation getLocation(int i) const { return {parent, name, i}; }

	inline StackLocation getLocation(int x, int y) const {
		return {parent, name, calcIdx(x, y)};
	}

	inline void set(int i, ItemStack stack) { items[i] = stack; }

	inline void set(int x, int y, ItemStack stack) {
		set(calcIdx(x, y), stack);
	}

	inline void set(int i, int count) {
		assert(!items[i].name.empty());
		items[i].count = count;
	}

	inline void set(int x, int y, int count) { set(calcIdx(x, y), count); }

	bool equals(const InventoryList &other) const;

	ItemStack addItem(ItemStack item);
	ItemStack takeItem(ItemStack item);
};

class Inventory {
	InventoryLocation location = {"player"};
	std::map<std::string, std::unique_ptr<InventoryList>> lists;

public:
	explicit Inventory(const InventoryLocation &location)
			: location(location) {}

	Inventory(const Inventory &other) = delete;

	InventoryList *newList(const std::string &name, int w, int h);

	inline std::optional<InventoryList *> get(const std::string &name) {
		auto it = lists.find(name);
		if (it != lists.end()) {
			return {it->second.get()};
		}

		return {};
	}

	inline ItemStack get(const std::string &name, int x) const {
		auto it = lists.find(name);
		if (it == lists.end()) {
			Log("Inventory", ERROR)
					<< "Unable to find list " << name << " in inventory!";
			return {};
		}

		return it->second->get(x);
	}

	inline ItemStack get(const StackLocation &loc) const {
		assert(loc.inv == location);
		return get(loc.list, loc.idx);
	}

	inline void set(const std::string &name, int x, ItemStack stack) {
		auto it = lists.find(name);
		if (it == lists.end()) {
			Log("Inventory", ERROR)
					<< "Unable to find list " << name << " in inventory!";
			return;
		}

		it->second->set(x, stack);
	}

	inline void set(const StackLocation &loc, ItemStack stack) {
		set(loc.list, loc.idx, stack);
	}

	inline void set(const std::string &name, int x, int count) {
		auto it = lists.find(name);
		if (it == lists.end()) {
			Log("Inventory", ERROR)
					<< "Unable to find list " << name << " in inventory!";
			return;
		}

		it->second->set(x, count);
	}

	inline void set(const StackLocation &loc, int count) {
		set(loc.list, loc.idx, count);
	}

	inline const InventoryLocation &getLocation() { return location; }

	inline size_t size() const { return lists.size(); }

	bool equals(const Inventory &other) const;

	int serialised_size();
	void serialise(network::Packet &pkt);
	void deserialise(network::Packet &pkt);
};

class InventoryProvider {
	std::function<Inventory *(std::string)> provider;

public:
	InventoryProvider(const std::function<Inventory *(std::string)> &provider)
			: provider(provider) {}

	std::optional<Inventory *> get(
			const InventoryLocation &inventoryLocation) const {
		auto inv = provider(inventoryLocation.name);
		if (!inv) {
			return {};
		}

		return inv;
	}

	std::optional<InventoryList *> getList(
			const InventoryLocation &inventoryLocation,
			const std::string &listname) const {
		auto inv = get(inventoryLocation).value_or(nullptr);
		if (!inv) {
			return std::nullopt;
		}

		auto list = inv->get(listname);
		if (!list) {
			return std::nullopt;
		}

		return list;
	}

	ItemStack getStack(const StackLocation &stackLocation) const {
		auto list = getList(stackLocation.inv, stackLocation.list)
							.value_or(nullptr);

		if (!list) {
			return {};
		}

		return list->get(stackLocation.idx);
	}
};

} // namespace content
