#pragma once
#include <string>
#include "../types/types.hpp"
#include "Inventory.hpp"

namespace content {

class Player {
public:
	std::string username;
	Inventory inventory;

	explicit Player(const std::string &username)
			: username(username),
			  inventory(InventoryLocation::FromPlayer(username)) {}

	Player(const Player &other) = delete;
};

} // namespace content
