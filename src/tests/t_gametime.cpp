#include "tests.hpp"
#include "../world/GameTime.hpp"

using namespace world;

GameTime makeTime(float timeOfDay) {
	return GameTime(timeOfDay);
}

inline bool roughly(float a, float b) {
	return std::abs(a - b) < 0.01f;
}

Test(GameTimeUpdateTest) {
	GameTime time{};

	time.setSpeedFromDayLength(123.f);
	Assert(time.asDecimal() == 0);
	Assert(time.getString() == "00:00");
	time.update(123.f / 2.f);
	Assert(time.asDecimal() == 0.5);
	Assert(time.getString() == "12:00");
	time.update(123.f / 2.f);
	Assert(time.asDecimal() == 0);
	Assert(time.getString() == "00:00");
}

Test(GameTimeDaylightTest) {
	Assert(roughly(makeTime(12.f).getDaylight(), 1));
	Assert(roughly(makeTime(0.f).getDaylight(), 0));
	Assert(roughly(makeTime(24.f).getDaylight(), 0));
	Assert(roughly(makeTime(6.f).getDaylight(), 0));
	Assert(roughly(makeTime(18.f).getDaylight(), 0));

	float v = makeTime(6.5f).getDaylight();
	Assert(v > 0 && v < 0.3f);
}
