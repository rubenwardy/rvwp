#include "tests.hpp"

#include <client/controllers/architect/ArchitectController.hpp>
#include <world/World.hpp>
#include "mocks.hpp"

#include "DefinitionTestData.hpp"

using namespace client;
using namespace content;

Test(Architect) {
	EventBus<ArchitectEvent> bus;
	DefinitionManager def;
	defineTestData(&def);
	world::World world{&def};
	MockInput input;
	MockCamera camera;
	ArchitectController ctr{&bus, &input, &camera, &def, &world};
}
