#include "DefinitionTestData.hpp"
#include "content/RegistrationHelpers.hpp"

using namespace content;

void defineTestData(content::DefinitionManager *defMan) {
	defMan->unlock();

	defMan->defineVoid();
	defMan->registerTile(
			simpleTile("wall", "Wall", Material(0, 3, EMT_CON_8BIT), true));
	defMan->registerTile(
			floorTile("floor_wood", "Wooden Floor", Material(1, 1), "wood"));

	defMan->registerTile(
			simpleTile("stone", "Stone", Material(4, 0), true, "hard"));
	defMan->registerTile(
			simpleTile("dirt", "Dirt", Material(2, 0), false, "dirt"));
	defMan->registerTile(
			simpleTile("dirtwall", "Dirt Wall", Material(3, 0), true, "dirt"));
	defMan->registerTile(
			simpleTile("grass", "Grass", Material(1, 0), false, "grass"));

	defMan->registerTile(
			simpleTile("door", "Door", Material(2, 1, EMT_DOORLIKE), true));
	defMan->registerTile(simpleTile(
			"door_open", "Open Door", Material(2, 2, EMT_DOORLIKE), false));

	defMan->registerTile(lightTile("torch", "Torch", Material(1, 2), 0xC));
	defMan->registerTile(lightTile("sunlamp", "Sunlamp", Material(4, 2), 0xF));

	defMan->registerTile(buildingTile("stove", "Stove", Material(0, 11), true));
	defMan->registerTile(buildingTile("sink", "Sink", Material(1, 11), true));
	defMan->registerTile(
			buildingTile("counter", "Counter", Material(2, 11), true));

	defMan->registerTile(
			buildingTile("blueprint", "Blueprint", Material(5, 2), false));

	defMan->registerItem(weapon("pistol", "Pistol", Material("pistol.png"),
			WeaponSpec::makeStandardGun(10, 10, 0.2)));
	defMan->registerItem(simpleItem("sword", "Sword", Material("sword.png")));

	defMan->registerToolSpec({ToolType::CANCEL, "Cancel", {},
			ToolSelectType::FILLED, {}, "", world::ChunkLayer::Tile});

	std::vector<std::string> buildableNames = {
			"wall",
			"door",
			"torch",
			"sunlamp",
			"stove",
			"sink",
			"counter",
			"floor_wood",
	};
	for (auto name : buildableNames) {
		auto def = defMan->getTileDef(name);
		defMan->registerToolSpec(makeBuildSpec(def));
	}

	defMan->lock();
}
