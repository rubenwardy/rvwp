#include "tests.hpp"
#include "content/KeyValueStore.hpp"
#include "content/content.hpp"
#include "content/RegistrationHelpers.hpp"

using namespace content;

Test(DefManTests) {
	DefinitionManager def;

	Assert(def.isLocked());
	def.unlock();
	Assert(!def.isLocked());

	ItemDef *a =
			def.registerTile(simpleTile("wall", "Wall", Material(0, 0), true));
	ItemDef *b = def.registerTile(
			floorTile("floor_wood", "Wooden Floor", Material(1, 0), "wood"));
	ItemDef *c = def.registerItem(
			simpleItem("sword", "Sword", Material("sword.png")));

	Assert(!def.isLocked());
	def.lock();
	Assert(def.isLocked());

	Assert(a && b && c);
	Assert(def.getItemDef("wall") == a);
	Assert(def.getItemDef("floor_wood") == b);
	Assert(def.getItemDef("sword") == c);
	Assert(def.getItemDef("wall") == a);
	Assert(def.getItemDef("floor_wood") == b);
	Assert(def.getItemDef("sword") == c);
	Assert(def.exists("wall"));
	Assert(def.exists("floor_wood"));
	Assert(def.exists("sword"));
	Assert(def.exists(1));
	Assert(def.exists(2));
	Assert(!def.exists(4));
	Assert(!def.exists(0));
	Assert(def.getItemDef("wall")->material.name == "tileset.diffuse.png");
	Assert(def.getItemDef("floor_wood")->material.name ==
			"tileset.diffuse.png");
	Assert(def.getItemDef("sword")->material.name == "sword.png");
}

Test(KeyValueStore) {
	KeyValueStore store;

	Assert(!store.get<std::string>("hello").has_value());
	store.set<std::string>("hello", "value");

	{
		auto optionalValue = store.get<std::string>("hello");
		Assert(optionalValue.has_value());
		Assert(optionalValue.value() == "value");
	}

	Assert(!store.get<std::string>("numberOfFish").has_value());
	store.set<int>("numberOfFish", 2);
	Assert(store.get<int>("numberOfFish").value_or(0) == 2);
	Assert(store.get<std::string>("numberOfFish").value_or("") == "2");

	Assert(!store.get<std::string>("hasThing").has_value());
	store.set<bool>("hasThing", true);
	Assert(store.get<bool>("hasThing").value_or(false));
	LogF(ERROR, "%s", store.get<std::string>("hasThing").value_or("").c_str());
	Assert(store.get<std::string>("hasThing").value_or("") == "true");

	Assert(!store.get<std::string>("lengthf").has_value());
	store.set<float>("lengthf", 3.1415);
	Assert(fabs(store.get<float>("lengthf").value_or(0.0) - 3.1415) < 0.0001);

	Assert(!store.get<std::string>("lengthd").has_value());
	store.set<double>("lengthd", 3.1415);
	Assert(fabs(store.get<double>("lengthd").value_or(0.0) - 3.1415) < 0.0001);
}
