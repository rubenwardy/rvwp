#include "events/EventListener.hpp"
#include "tests.hpp"
#include "content/InventorySelection.hpp"

using namespace content;

namespace {

const std::string A = "wall";
const std::string B = "sword";

void initInventory(content::Inventory &inventory) {
	inventory.newList("main", 6, 4);
	inventory.newList("hotbar", 2, 0);
	inventory.set("main", 0, ItemStack(A, 6));
	inventory.set("main", 3, ItemStack(A, 10));
	inventory.set("main", 5, ItemStack(B, 10));
}
} // namespace

Test(InventorySelection_MoveTo_Same) {
	auto inventoryLocation = InventoryLocation::FromPlayer("test");
	Inventory inventory(inventoryLocation);
	initInventory(inventory);

	InventoryProvider provider = {[&](auto loc) -> content::Inventory * {
		if (loc == inventoryLocation.name) {
			return &inventory;
		} else {
			return nullptr;
		}
	}};

	EventBus<InventoryCommand> eventBus;
	InventorySelection selection(provider, &eventBus);
	Assert(selection.empty());
	selection.select({inventoryLocation, "main", 0}, 6);
	Assert(!selection.empty());
	Assert(selection.getSelection().list == "main");
	Assert(selection.getSelection().idx == 0);
	Assert(selection.getCount() == 6);

	Assert(inventory.get("main", 0).name == A);
	Assert(inventory.get("main", 0).count == 6);

	bool called = false;
	EventListener<InventoryCommand> listener(
			&eventBus, [&](auto cmd) { called = true; });

	Assert(!selection.moveTo({inventoryLocation, "main", 0}, 0));
	eventBus.flush();

	Assert(!called);
	Assert(inventory.get("main", 0).name == A);
	Assert(inventory.get("main", 0).count == 6);
	Assert(selection.empty());
}

Test(InventorySelection_MoveTo_Empty) {
	auto inventoryLocation = InventoryLocation::FromPlayer("test");
	Inventory inventory(inventoryLocation);
	initInventory(inventory);

	InventoryProvider provider = {[&](auto loc) -> content::Inventory * {
		if (loc == inventoryLocation.name) {
			return &inventory;
		} else {
			return nullptr;
		}
	}};

	EventBus<InventoryCommand> eventBus;
	InventorySelection selection(provider, &eventBus);
	selection.select({inventoryLocation, "main", 0}, 6);

	Assert(inventory.get("main", 0).name == A);
	Assert(inventory.get("main", 0).count == 6);
	Assert(inventory.get("main", 1).empty());

	InventoryCommand capture;
	EventListener<InventoryCommand> listener(
			&eventBus, [&](auto cmd) { capture = cmd; });

	Assert(selection.moveTo({inventoryLocation, "main", 1}, 0));
	eventBus.flush();

	Assert(inventory.get("main", 0).empty());
	Assert(inventory.get("main", 1).name == A);
	Assert(inventory.get("main", 1).count == 6);

	Assert(capture.getType() == InventoryCommand::SWAP);
	Assert(capture.getSource().list == "main");
	Assert(capture.getSource().idx == 0);
	Assert(capture.getDestination().list == "main");
	Assert(capture.getDestination().idx == 1);
	Assert(capture.getCount() == 6);
}

Test(InventorySelection_MoveTo_Empty_Partial) {
	auto inventoryLocation = InventoryLocation::FromPlayer("test");
	Inventory inventory(inventoryLocation);
	initInventory(inventory);

	InventoryProvider provider = {[&](auto loc) -> content::Inventory * {
		if (loc == inventoryLocation.name) {
			return &inventory;
		} else {
			return nullptr;
		}
	}};

	EventBus<InventoryCommand> eventBus;
	InventorySelection selection(provider, &eventBus);
	selection.select({inventoryLocation, "main", 0}, 2);

	Assert(inventory.get("main", 0).name == A);
	Assert(inventory.get("main", 0).count == 6);
	Assert(inventory.get("main", 1).empty());

	InventoryCommand capture;
	EventListener<InventoryCommand> listener(
			&eventBus, [&](auto cmd) { capture = cmd; });

	Assert(selection.moveTo({inventoryLocation, "main", 1}, 0));
	eventBus.flush();

	Assert(inventory.get("main", 0).name == A);
	Assert(inventory.get("main", 0).count == 4);
	Assert(inventory.get("main", 1).name == A);
	Assert(inventory.get("main", 1).count == 2);

	Assert(capture.getType() == InventoryCommand::MOVE);
	Assert(capture.getSource().list == "main");
	Assert(capture.getSource().idx == 0);
	Assert(capture.getDestination().list == "main");
	Assert(capture.getDestination().idx == 1);
	Assert(capture.getCount() == 2);
}

Test(InventorySelection_MoveTo_Different) {
	auto inventoryLocation = InventoryLocation::FromPlayer("test");
	Inventory inventory(inventoryLocation);
	initInventory(inventory);

	InventoryProvider provider = {[&](auto loc) -> content::Inventory * {
		if (loc == inventoryLocation.name) {
			return &inventory;
		} else {
			return nullptr;
		}
	}};

	InventorySelection selection(provider);
	selection.select({inventoryLocation, "main", 0}, 2);

	Assert(!selection.moveTo({inventoryLocation, "main", 5}, 10));
}

Test(InventorySelection_MoveTo_Same_Partial) {
	auto inventoryLocation = InventoryLocation::FromPlayer("test");
	Inventory inventory(inventoryLocation);
	initInventory(inventory);

	InventoryProvider provider = {[&](auto loc) -> content::Inventory * {
		if (loc == inventoryLocation.name) {
			return &inventory;
		} else {
			return nullptr;
		}
	}};

	EventBus<InventoryCommand> eventBus;
	InventorySelection selection(provider, &eventBus);
	selection.select({inventoryLocation, "main", 0}, 2);

	Assert(inventory.get("main", 0).name == A);
	Assert(inventory.get("main", 0).count == 6);
	Assert(inventory.get("main", 3).name == A);
	Assert(inventory.get("main", 3).count == 10);

	InventoryCommand capture;
	EventListener<InventoryCommand> listener(
			&eventBus, [&](auto cmd) { capture = cmd; });

	Assert(selection.moveTo({inventoryLocation, "main", 3}, 10));
	eventBus.flush();

	Assert(inventory.get("main", 0).name == A);
	Assert(inventory.get("main", 0).count == 4);
	Assert(inventory.get("main", 3).name == A);
	Assert(inventory.get("main", 3).count == 12);

	Assert(capture.getType() == InventoryCommand::MOVE);
	Assert(capture.getSource().list == "main");
	Assert(capture.getSource().idx == 0);
	Assert(capture.getDestination().list == "main");
	Assert(capture.getDestination().idx == 3);
	Assert(capture.getCount() == 2);
}

Test(InventorySelection_MoveTo_Same_Full) {
	auto inventoryLocation = InventoryLocation::FromPlayer("test");
	Inventory inventory(inventoryLocation);
	initInventory(inventory);

	InventoryProvider provider = {[&](auto loc) -> content::Inventory * {
		if (loc == inventoryLocation.name) {
			return &inventory;
		} else {
			return nullptr;
		}
	}};

	EventBus<InventoryCommand> eventBus;
	InventorySelection selection(provider, &eventBus);
	selection.select({inventoryLocation, "main", 0}, 6);

	Assert(inventory.get("main", 0).name == A);
	Assert(inventory.get("main", 0).count == 6);
	Assert(inventory.get("main", 3).name == A);
	Assert(inventory.get("main", 3).count == 10);

	InventoryCommand capture;
	EventListener<InventoryCommand> listener(
			&eventBus, [&](auto cmd) { capture = cmd; });

	Assert(selection.moveTo({inventoryLocation, "main", 3}, 10));
	eventBus.flush();

	Assert(inventory.get("main", 0).empty());
	Assert(inventory.get("main", 3).name == A);
	Assert(inventory.get("main", 3).count == 16);

	Assert(capture.getType() == InventoryCommand::MOVE);
	Assert(capture.getSource().list == "main");
	Assert(capture.getSource().idx == 0);
	Assert(capture.getDestination().list == "main");
	Assert(capture.getDestination().idx == 3);
	Assert(capture.getCount() == 6);
}
