#include "tests.hpp"

#include "client/input/InputController.hpp"

#include <memory>

using namespace client;

namespace {
class MockInputResolver : public IInputResolver {
public:
	V3f moveVector;
	std::optional<V3f> cursorPosition;

	V3f passedInOrigin{};

	V3f getMoveVector() override { return moveVector; }

	std::optional<V3f> getCursorPosition(const V3f &origin) override {
		passedInOrigin = origin;
		return cursorPosition;
	}

	bool isVirtualCursor() const override { return false; }

	void update(float dtime) override {}

	int getTimeSinceLastActive() const override { return 0; }
};
} // namespace

Test(InputController_GetMoveVector_CorrectResult_Tests) {
	InputController input;

	V3f expectedMoveVector(1, 0, 0);

	std::unique_ptr<MockInputResolver> resolver1 =
			std::make_unique<MockInputResolver>();
	resolver1->moveVector = V3f(0, 0, 0);
	input.addResolver(std::move(resolver1));

	std::unique_ptr<MockInputResolver> resolver2 =
			std::make_unique<MockInputResolver>();
	resolver2->moveVector = expectedMoveVector;
	input.addResolver(std::move(resolver2));

	Assert(input.getMoveVector() == expectedMoveVector);
}

Test(InputController_GetMoveVector_Normalised_Tests) {
	InputController input;

	V3f expectedMoveVector(1, 0, 0);

	std::unique_ptr<MockInputResolver> resolver =
			std::make_unique<MockInputResolver>();
	resolver->moveVector = V3f(10, 0, 0);
	input.addResolver(std::move(resolver));

	Assert(input.getMoveVector() == expectedMoveVector);
}

Test(InputController_GetCursorPosition_Tests) {
	InputController input;

	V3f expectedCursorPosition(2.5, 3.5, 4);
	V3f origin = {1.5, 2.5, 3};

	MockInputResolver *mockResolver;

	{
		std::unique_ptr<MockInputResolver> resolver =
				std::make_unique<MockInputResolver>();
		resolver->cursorPosition = expectedCursorPosition;
		mockResolver = resolver.get();
		input.addResolver(std::move(resolver));
	}

	auto position = input.getCursorPosition(origin);

	Assert(position);
	Assert(position.value() == expectedCursorPosition);
	Assert(mockResolver->passedInOrigin == origin);
}
