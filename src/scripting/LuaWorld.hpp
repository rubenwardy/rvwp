#pragma once

#include "Lua.hpp"

#include "world/Chunk.hpp"
#include "world/Pathfinder.hpp"
#include "content/DefinitionManager.hpp"

#include <string>

namespace scripting {

/// A definition of a tile from Lua
struct TileSpecFromLua {
	std::optional<std::string> name;
};

static inline sol::table TileToTable(
		const TileSpecFromLua &spec, sol::this_state state) {
	sol::table table(state.L, sol::create);
	table["name"] = std2sol(spec.name);
	return table;
}

static inline TileSpecFromLua TableToTileSpec(sol::table table) {
	sol::optional<std::string> name = table["name"];

	return {sol2std(name)};
}

static inline content::ItemType stringToType(const std::string &layer) {
	if (layer == "tile") {
		return content::ItemType::Tile;
	} else if (layer == "floor") {
		return content::ItemType::Floor;
	} else if (layer == "terrain") {
		return content::ItemType::Terrain;
	} else {
		throw sol::error("Unknown item type: " + layer);
	}
}

static inline world::ChunkLayer stringToLayer(const std::string &layer) {
	if (layer == "tile") {
		return world::ChunkLayer::Tile;
	} else if (layer == "floor") {
		return world::ChunkLayer::Floor;
	} else {
		throw sol::error("Unknown chunk layer: " + layer);
	}
}

static inline std::string layerToString(world::ChunkLayer layer) {
	switch (layer) {
	case world::ChunkLayer::Tile:
		return "tile";
	case world::ChunkLayer::Floor:
		return "floor";
	}

	FatalError("Unknown chunk layer");
}

extern world::Pathfinder::Settings TableToSettings(
		sol::table table, content::DefinitionManager *definitionManager);

} // namespace scripting
