#include "KeyValueStoreRef.hpp"

using namespace scripting;
using namespace content;

void KeyValueStoreRef::registerToNamespace(sol::state &lua) {
	// clang-format off
	lua.new_usertype<KeyValueStoreRef>("KeyValueRef",
			"get_string", &KeyValueStoreRef::get_string,
			"set_string", &KeyValueStoreRef::set_string,
			"get_int",    &KeyValueStoreRef::get_int,
			"set_int",    &KeyValueStoreRef::set_int,
			"get_bool",   &KeyValueStoreRef::get_bool,
			"set_bool",   &KeyValueStoreRef::set_bool,
			"get_float",  &KeyValueStoreRef::get_float,
			"set_float",  &KeyValueStoreRef::set_float,
			"to_table",   &KeyValueStoreRef::to_table);
	// clang-format on
}

sol::optional<std::string> KeyValueStoreRef::get_string(
		const std::string &key) const {
	return std2sol(getReference()->get<std::string>(key));
}

void KeyValueStoreRef::set_string(
		const std::string &key, const std::string &value) {
	getReference()->set(key, value);
}

sol::optional<int> KeyValueStoreRef::get_int(const std::string &key) const {
	return std2sol(getReference()->get<int>(key));
}

void KeyValueStoreRef::set_int(const std::string &key, int value) {
	getReference()->set(key, value);
}

sol::optional<bool> KeyValueStoreRef::get_bool(const std::string &key) const {
	return std2sol(getReference()->get<bool>(key));
}

void KeyValueStoreRef::set_bool(const std::string &key, bool value) {
	getReference()->set(key, value);
}

sol::optional<float> KeyValueStoreRef::get_float(const std::string &key) const {
	return std2sol(getReference()->get<float>(key));
}

void KeyValueStoreRef::set_float(const std::string &key, float value) {
	getReference()->set(key, value);
}

sol::table KeyValueStoreRef::to_table(sol::this_state state) {
	sol::table table(state.L, sol::create);
	for (auto kv : *getReference()) {
		table[kv.first] = kv.second;
	}

	return table;
}
