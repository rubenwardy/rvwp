#pragma once

#include "server/controllers/ScriptingController.hpp"
#include "scripting/Lua.hpp"

namespace scripting {

struct Breakpoint {
	std::string file;
	int line = -1;

	Breakpoint(const std::string &file, int line) : file(file), line(line) {}

	bool valid() const { return line > 0; }
};

class LuaDebugger {
	static void trampoline(lua_State *L, lua_Debug *ar);

	sol::state &lua;
	std::vector<Breakpoint> breakpoints;
	bool stepping = false;

public:
	LuaDebugger(sol::state &lua) : lua(lua) {}

	/// Whether interactive capabilities are available
	bool isInteractive() const;

	/// Add a breakpoint
	///
	/// @param file File path
	/// @param line Line number
	void addBreakpoint(const std::string &file, int line);

	/// Add a breakpoint
	///
	/// @param str "path/to/file.lua:123"
	void addBreakpoint(const std::string &str);

	/// Pause execution for interactive mode
	///
	/// @param ar Information on the currently executing function, or nullptr.
	void pause(lua_Debug *ar = nullptr);

private:
	void attachHook();
	void onLineExecuted(lua_Debug *ar);
	Breakpoint parseBreakpoint(const std::string &str);

	bool runLuaAtScope(lua_Debug *ar, const std::string &line);
};

} // namespace scripting
