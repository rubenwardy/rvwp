#pragma once

#include <vector>
#include <unordered_map>
#include <unordered_set>

#include "ModConfig.hpp"

namespace scripting {

class ModOrderResolver {
	std::unordered_map<std::string, int> index;
	std::unordered_map<std::string, ModConfig> mods_by_name;
	std::vector<ModConfig> ordered;
	std::unordered_set<std::string> added;

public:
	explicit ModOrderResolver(const std::vector<ModConfig> &mods) {
		for (ModConfig mod : mods) {
			mods_by_name.insert({mod.name, mod});
		}
	}

	std::vector<ModConfig> resolve();

private:
	void resolveSpecific(const ModConfig &mod);
};

} // namespace scripting
