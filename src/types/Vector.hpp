#pragma once
#include "../types/types.hpp"
#include <log.hpp>
#include <climits>

#ifndef SERVER
#	include <SFML/System/Vector2.hpp>
#	include <SFML/System/Vector3.hpp>
#endif

namespace types {

template <typename T>
class Vector3 {
public:
	Vector3<T>() : x(0), y(0), z(0) {}

	Vector3<T>(T x, T y, s32 z) : x(x), y(y), z(z) {}

#ifdef SFML_VECTOR2_HPP
	Vector3<T>(const sf::Vector2f &other, const s32 &z)
			: x(other.x), y(other.y), z(z) {}

	operator sf::Vector2f() const { return sf::Vector2f(x, y); }

	operator sf::Vector3f() const { return sf::Vector3f(x, y, z); }
#endif

	Vector3<T> operator+(const Vector3<T> &other) const {
		return Vector3<T>(x + other.x, y + other.y, z + other.z);
	}

	Vector3<T> operator+(const T &other) const {
		return Vector3<T>(x + other, y + other, z + other);
	}

	Vector3<T> operator-(const Vector3<T> &other) const {
		return Vector3<T>(x - other.x, y - other.y, z - other.z);
	}

	Vector3<T> operator-(const T &other) const {
		return Vector3<T>(x - other, y - other, z - other);
	}

	Vector3<T> operator/(const T &other) const {
		return Vector3<T>(x / other, y / other, z / other);
	}

	Vector3<T> operator*(const T &other) const {
		return Vector3<T>(x * other, y * other, z * other);
	}

	bool isZero() const { return x == 0 && y == 0 && z == 0; }

	bool isInvalid() const { return z <= -1000; }

	Vector3<T> makeInvalid() {
		z = -1000;
		return *this;
	}

	void operator*=(const T &other) {
		x *= other;
		y *= other;
		z *= other;
	}

	void operator*=(const Vector3<T> &other) {
		x *= other.x;
		y *= other.y;
		z *= other.z;
	}

	void operator+=(const Vector3<T> &other) {
		x += other.x;
		y += other.y;
		z += other.z;
	}

	bool operator==(const Vector3<T> &other) const {
		return (x == other.x && y == other.y && z == other.z);
	}

	bool operator<(const Vector3<T> &other) const {
		return (x < other.x && y < other.y && z < other.z);
	}

	bool operator<=(const Vector3<T> &other) const {
		return (x <= other.x && y <= other.y && z <= other.z);
	}

	bool operator!=(const Vector3<T> &other) const {
		return !(operator==(other));
	}

	Vector3<T> min(const Vector3<T> &b) const {
		return {std::min(x, b.x), std::min(y, b.y), std::min(z, b.z)};
	}

	Vector3<T> max(const Vector3<T> &b) const {
		return {std::max(x, b.x), std::max(y, b.y), std::max(z, b.z)};
	}

	inline Vector3<f32> floating() const {
		return {static_cast<f32>(x), static_cast<f32>(y), z};
	}

	inline Vector3<s32> floor() const {
		return {static_cast<s32>(std::floor(x)),
				static_cast<s32>(std::floor(y)), z};
	}

	inline T sqLength() const { return x * x + y * y + z * z; }

	inline float length() const { return sqrt(x * x + y * y + z * z); }

	inline T sqDistance(const Vector3<T> &other) const {
		return (*this - other).sqLength();
	}

	inline float distance(const Vector3<T> &other) const {
		return (*this - other).length();
	}

	bool isNormalised() const { return std::fabs(sqLength() - 1) < 0.001f; }

	void normalise2() {
		T dist = (T)sqrt(x * x + y * y);
		x = x / dist;
		y = y / dist;
	}

	void turn90AC() {
		T ox = x;
		x = y;
		y = -ox;
	}

	void turn90CW() {
		T ox = x;
		x = -y;
		y = ox;
	}

	T x, y;
	s32 z;
};

using V3s = Vector3<s32>;
using V3f = Vector3<f32>;

class Vector2 {
public:
	Vector2() : x(0), y(0) {}

	Vector2(int x, int y) : x(x), y(y) {}

	explicit Vector2(const V3s &other) : x(other.x), y(other.y) {}

	explicit Vector2(const V3f &other)
			: x((s32)floor(other.x)), y((s32)floor(other.y)) {}

#ifdef SFML_VECTOR2_HPP
	Vector2(const sf::Vector2i &other) : x(other.x), y(other.y) {}

	operator sf::Vector2i() const { return sf::Vector2i(x, y); }
#endif

	Vector2 operator+(const Vector2 &other) const {
		return {x + other.x, y + other.y};
	}

	Vector2 operator+(const s32 &other) const { return {x + other, y + other}; }

	void operator+=(const Vector2 &other) {
		x += other.x;
		y += other.y;
	}

	Vector2 operator-(const Vector2 &other) const {
		return {x - other.x, y - other.y};
	}

	Vector2 operator-(const s32 &other) const { return {x - other, y - other}; }

	Vector2 operator/(const s32 &other) const { return {x / other, y / other}; }

	Vector2 operator*(const s32 &other) const { return {x * other, y * other}; }

	bool operator==(const Vector2 &other) const {
		return (x == other.x && y == other.y);
	}

	bool operator!=(const Vector2 &other) const { return !(operator==(other)); }

	s32 sqDist(const Vector2 &other) const {
		s32 dx = x - other.x;
		s32 dy = y - other.y;
		return dx * dx + dy * dy;
	}

	s32 x, y;
};

using V2s = Vector2;

} // namespace types

namespace std {

template <>
struct hash<types::V3s> {
	std::size_t operator()(const types::V3s &p) const {
		return (p.z * 1000000 + p.y * 10000 + p.x) % (SIZE_MAX - 1);
	}
};

template <>
struct hash<types::V2s> {
	static_assert(sizeof(size_t) * CHAR_BIT == 64);
	size_t operator()(const types::V2s &v) const {
		using std::hash;
		return (size_t)v.x << 32 | (size_t)v.y;
	}
};

template <typename T>
inline ostream &operator<<(ostream &os, const types::Vector3<T> &t) {
	os << t.x << ", " << t.y << ", " << t.z;
	return os;
}

inline ostream &operator<<(ostream &os, const types::V2s &t) {
	os << t.x << ", " << t.y;
	return os;
}

} // namespace std
