#pragma once

#include <vector>

namespace types {

class Controller {
	bool parentEnabled = true;
	bool selfEnabled = true;
	std::vector<Controller *> children;

public:
	Controller() = default;
	Controller(const Controller &other) = delete;
	virtual ~Controller() = default;

	virtual void update(float dtime) = 0;

	inline void update_if_enabled(float dtime) {
		if (isEnabled()) {
			update(dtime);

			for (auto child : children) {
				child->update_if_enabled(dtime);
			}
		}
	}

	void addChild(Controller *child) {
		child->setParentEnabled(parentEnabled);
		children.push_back(child);
	}

	void removeChild(Controller *child) {
		for (auto iter = children.begin(); iter != children.end(); ++iter) {
			if (child == *iter) {
				child->setParentEnabled(true);
				children.erase(iter);
				return;
			}
		}
	}

	inline bool isEnabled() const { return parentEnabled && selfEnabled; }

	inline void enable() { setEnabled(true); }

	inline void disable() { setEnabled(false); }

private:
	void setEnabled(bool value) {
		selfEnabled = value;

		for (auto child : children) {
			child->setParentEnabled(value && parentEnabled);
		}
	}

	void setParentEnabled(bool value) {
		if (parentEnabled == value) {
			return;
		}

		parentEnabled = value;

		for (auto child : children) {
			child->setParentEnabled(value && parentEnabled);
		}
	}
};

} // namespace types
