#pragma once

#include "Mapgen.hpp"

namespace MG {

class FlatMG : public Mapgen {
public:
	explicit FlatMG(content::DefinitionManager *def) : Mapgen(def) {}

	void generate(world::WorldChunk *c) override;
};

} // namespace MG
