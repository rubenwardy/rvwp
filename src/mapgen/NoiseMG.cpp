#include "NoiseMG.hpp"
#include "content/content.hpp"
#include "world/LightCalculator.hpp"

#include <SimplexNoise.hpp>

using namespace world;
using namespace MG;

void NoiseMG::generate(WorldChunk *c) {
	V3s origin = CPosToPos(c->pos);
	auto c_grass = def->getCidFromName("grass");
	auto c_dirt = def->getCidFromName("dirt");
	auto c_dirtwall = def->getCidFromName("dirtwall");
	auto c_wall = def->getCidFromName("wall");
	auto c_door = def->getCidFromName("door");
	auto c_torch = def->getCidFromName("torch");
	auto c_wood = def->getCidFromName("floor_wood");
	auto c_stove = def->getCidFromName("stove");
	auto c_sink = def->getCidFromName("sink");
	auto c_counter = def->getCidFromName("counter");

	const float SCALE_X = 100;
	const float SCALE_Y = 2;

	for (int ry = 0; ry < CHUNK_SIZE; ry++) {
		for (int rx = 0; rx < CHUNK_SIZE; rx++) {
			float height = SCALE_Y *
							SimplexNoise::noise(
									((float)(rx + origin.x)) / SCALE_X,
									((float)(ry + origin.y)) / SCALE_X) -
					c->pos.z;

			c->lightLevels[rx + ry * CHUNK_SIZE] = 0xF000;

			if (height > 1) {
				c->tiles[rx + ry * CHUNK_SIZE].cid = c_dirtwall;
				c->terrain[rx + ry * CHUNK_SIZE] = c_dirt;
				c->lightLevels[rx + ry * CHUNK_SIZE] = 0x0000;
			} else if (height > 0) {
				c->terrain[rx + ry * CHUNK_SIZE] = c_grass;
			}
		}
	}

	if (c->pos == V3s(0, 0, 0)) {
		for (int y = 2; y < 7; y++) {
			c->tiles[7 + y * CHUNK_SIZE].cid = c_wall;
			c->tiles[11 + y * CHUNK_SIZE].cid = c_wall;
		}

		for (int x = 7; x < 12; x++) {
			c->tiles[x + 2 * CHUNK_SIZE].cid = c_wall;
		}

		c->tiles[8 + 6 * CHUNK_SIZE].cid = c_wall;
		c->tiles[9 + 6 * CHUNK_SIZE].cid = c_door;
		c->tiles[10 + 6 * CHUNK_SIZE].cid = c_wall;

		for (int y = 2; y < 7; y++) {
			for (int x = 7; x < 12; x++) {
				c->floort[x + y * CHUNK_SIZE].cid = c_wood;
				c->lightLevels[x + y * CHUNK_SIZE] = 0x0000;
			}
		}

		c->tiles[8 + 3 * CHUNK_SIZE].cid = c_stove;
		c->tiles[9 + 3 * CHUNK_SIZE].cid = c_sink;
		c->tiles[10 + 3 * CHUNK_SIZE].cid = c_counter;

		c->getMetaData({8, 3, 0}, ChunkLayer::Tile)
				.value()
				->set<bool>("job_giver", true);

		c->tiles[8 + 5 * CHUNK_SIZE].cid = c_torch;
		SetArtificialRed(c->lightLevels[8 + 5 * CHUNK_SIZE], 0xF);

		c->tiles[12 + 8 * CHUNK_SIZE].cid = c_torch;
		SetArtificialRed(c->lightLevels[12 + 8 * CHUNK_SIZE], 0xF);
	}

	LightCalculator light{def};
	light.initialSpread(c);
}
