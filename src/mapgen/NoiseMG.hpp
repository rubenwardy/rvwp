#pragma once

#include "Mapgen.hpp"

namespace MG {

class NoiseMG : public Mapgen {
public:
	explicit NoiseMG(content::DefinitionManager *def) : Mapgen(def) {}

	void generate(world::WorldChunk *c) override;
};

} // namespace MG
