#include "ClientLauncher.hpp"

#include "network/net.hpp"
#include "server/Server.hpp"

#include "windowicon.hpp"

#include <SFML/Graphics.hpp>
#include <log.hpp>
#include <thread>
#include <TGUI/TGUI.hpp>
#include <TGUI/Backend/SFML-Graphics.hpp>

#include "input/InputController.hpp"
#include "MainMenu.hpp"
#include "sfext/DropShadow.hpp"
#include "Game.hpp"

using namespace client;

void startServer(const server::ServerSpec &spec) {
	loguru::set_thread_name("Server");
	server::Server s(spec);
	if (s.start(30100)) {
		s.run();
	}
}

bool ClientLauncher::launch() {
	if (!sf::Shader::isAvailable()) {
		LogF(ERROR, "Shaders are not supported");
		return false;
	}

	sf::RenderWindow window(sf::VideoMode(800, 600), "RVWP");
	window.setVerticalSyncEnabled(true);
	window.setIcon(windowIconData.width, windowIconData.height,
			windowIconData.pixel_data);

	tgui::Gui gui(window);

	sf::Font font;
	tgui::Font tguiFont("assets/fonts/Roboto-Regular.ttf");
	if (!font.loadFromFile("assets/fonts/Roboto-Regular.ttf")) {
		LogF(ERROR, "Error loading font assets/fonts/Roboto-Regular.ttf");
		return false;
	}

	gui.setFont(tguiFont);

	const auto theme = std::make_shared<tgui::Theme>("assets/gui/rvwp.style");
	tgui::Theme::setDefault(theme);

	auto shadowShader = std::make_unique<sf::Shader>();
	if (!shadowShader->loadFromFile(
				"assets/shaders/blur.frag", sf::Shader::Fragment)) {
		LogF(ERROR, "Error loading drop shadow shader!");
		return false;
	}
	sfext::DropShadow::setShader(shadowShader.get());

	auto inputCtr = std::make_unique<InputController>();

	MainMenu menu(&window, gui, font, inputCtr.get());
	if (!menu.load()) {
		LogF(ERROR, "Error loading main menu!");
		return false;
	}

	if (menu.run()) {
		MainMenu::ClientSpec &spec = menu.spec;
		spec.describe();

		std::thread serverThread;
		if (spec.startServer) {
			serverThread = std::thread(&startServer, spec.server);
		}

		gui.removeAllWidgets();

		Log("Main", INFO) << "Starting client!";
		Game g(&window, gui, font, inputCtr.get());
		if (!g.load(network::Address(spec.address, spec.port), spec.username,
					spec.password)) {
			Log("Main", ERROR) << "Error loading game!";
			return false;
		}
		g.run();

		if (serverThread.joinable()) {
			serverThread.join();
		}
	}

	return true;
}
