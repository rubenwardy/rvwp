#pragma once

#include <TGUI/TGUI.hpp>
#include <TGUI/Backend/SFML-Graphics.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <map>
#include <string>

#include "content/content.hpp"
#include "content/Inventory.hpp"
#include "content/InventoryCommand.hpp"

#include "events/EventListener.hpp"

#include "client/ResourceManager.hpp"
#include "client/gui/InvListWidget.hpp"
#include "client/gui/InvSelection.hpp"
#include "View.hpp"

namespace client {

class Game;
class InvView : public View {
	Game *game;
	tgui::Gui &gui;
	tgui::ChildWindow::Ptr window;
	InvListWidget::Ptr ilMain, ilHB;
	content::InventoryProvider provider;
	InvSelection selection;

	std::vector<InvListWidget::Ptr> invListWidgets;

	const EventListener<content::InventoryCommand> listener;

public:
	InvView(Game *game, EventBus<content::InventoryCommand> *inventoryBus,
			tgui::Gui &gui);

	bool load() override;
	void activate() override;
	void deactivate() override;
	void draw(sf::RenderTarget &target, sf::RenderStates states) override;
	void drawPostGUI(
			sf::RenderTarget &target, sf::RenderStates states) override;
	void update(float delta) override;

	void onInventoryAction(const content::InventoryCommand &action);
};

} // namespace client
