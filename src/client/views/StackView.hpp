#pragma once

#include "View.hpp"

namespace client {

class StackView : public View {
	std::vector<View::Ptr> children;

public:
	using Ptr = std::shared_ptr<StackView>;

	StackView() = default;

	template <class... Args>
	explicit StackView(Args &&...args) {
		children = {args...};
	}

	~StackView() override = default;

	void add(View::Ptr &&ptr) { children.emplace_back(std::move(ptr)); }

	template <class... Args>
	void emplace(Args &&...args) {
		children.emplace(std::forward<Args>(args)...);
	}

	template <class T>
	inline std::shared_ptr<T> get(int i) {
		return std::dynamic_pointer_cast<T>(children[i]);
	}

	template <class T>
	inline std::shared_ptr<T> getFront() {
		return get<T>(children.size() - 1);
	}

	//
	// View Methods
	//

	bool load() override {
		for (auto &view : children) {
			if (!view->load()) {
				return false;
			}
		}

		return true;
	}

	void activate() override {
		for (auto &view : children) {
			view->activate();
		}
	}

	void deactivate() override {
		for (auto &view : children) {
			view->deactivate();
		}
	}

	void update(float dtime) override {
		for (auto &view : children) {
			view->update(dtime);
		}
	}

	void draw(sf::RenderTarget &target, sf::RenderStates states) override {
		for (auto &view : children) {
			view->draw(target, states);
		}
	}

	void drawHUD(sf::RenderTarget &target, sf::RenderStates states) override {
		for (auto &view : children) {
			view->drawHUD(target, states);
		}
	}

	void drawPostGUI(
			sf::RenderTarget &target, sf::RenderStates states) override {
		for (auto &view : children) {
			view->drawPostGUI(target, states);
		}
	}

	void resize(unsigned int width, unsigned int height) override {
		for (auto &view : children) {
			view->resize(width, height);
		}
	}

	bool exit() override {
		for (auto &view : children) {
			if (view->exit()) {
				return true;
			}
		}

		return false;
	}
};

} // namespace client
