#pragma once
#include <TGUI/TGUI.hpp>
#include <TGUI/Backend/SFML-Graphics.hpp>
#include <SFML/Graphics.hpp>
#include <vector>
#include <map>
#include <string>
#include "client/ResourceManager.hpp"
#include "View.hpp"

namespace client {

class Game;
class ChatSendView : public View {
	Game *game;
	sf::Font font;

	tgui::Gui &gui;
	tgui::ChildWindow::Ptr window;
	tgui::EditBox::Ptr txtMessage;

public:
	ChatSendView(Game *game, tgui::Gui &gui, const sf::Font &font)
			: game(game), font(font), gui(gui) {}

	bool load() override;
	void activate() override;
	void deactivate() override;
	void draw(sf::RenderTarget &target, sf::RenderStates states) override {}
	void update(float delta) override {}

	void onSendClick();
	void setText(const std::string &text);
};

} // namespace client
