#pragma once

#include <SFML/Graphics.hpp>
#include <Thor/Math.hpp>
#include <Thor/Graphics.hpp>
#include <Thor/Particles.hpp>
#include <string>
#include <TGUI/TGUI.hpp>
#include <TGUI/Backend/SFML-Graphics.hpp>

#include "world/Entity.hpp"
#include "world/World.hpp"
#include "content/content.hpp"
#include "client/gui/BottomBar.hpp"
#include "client/gui/ToolPanel.hpp"
#include "client/views/View.hpp"
#include "content/ToolSpec.hpp"
#include "client/views/architect/tools/ITool.hpp"
#include "client/input/IInput.hpp"

namespace client {

class Game;
class HudView;
class WorldView;
class ArchitectLayer;
class ArchitectView : public View {
	Game *game;
	WorldView *worldView;
	HudView *hudView;
	ArchitectLayer *layer = nullptr;
	tgui::Gui &gui;
	BottomBar::Ptr bottomBar;
	ToolPanel::Ptr buildPanel;
	sf::Shader &worldShader;
	std::vector<InputConnection> inputConnections;

public:
	ArchitectView(Game *game, WorldView *worldView, HudView *hudView,
			tgui::Gui &gui, sf::Shader &worldShader)
			: game(game), worldView(worldView), hudView(hudView), gui(gui),
			  worldShader(worldShader) {}

	bool load() override;
	void activate() override;
	void deactivate() override;
	void draw(sf::RenderTarget &target, sf::RenderStates states) override {}
	void update(float dtime) override;

	/// Close active tool or dialog
	///
	/// @return whether something was closed
	bool exit() override;

private:
	void onToolSelected(const content::ToolSpec &spec);
};

} // namespace client
