#include "ArchitectView.hpp"

#include <log.hpp>

#include "client/controllers/architect/ArchitectController.hpp"
#include "client/Game.hpp"
#include "client/views/world/WorldView.hpp"
#include "ArchitectLayer.hpp"
#include "tools/BuildTool.hpp"
#include "tools/CancelTool.hpp"
#include "tools/DeconstructTool.hpp"
#include "client/input/IInput.hpp"
#include "client/views/HudView.hpp"

using namespace client;
using namespace content;

bool ArchitectView::load() {
	bottomBar = BottomBar::create({"100%", "40"});
	bottomBar->setPosition("0", "100% - height");
	gui.add(bottomBar, "BottomBar");
	bottomBar->setVisible(false);

	buildPanel = ToolPanel::create(game->getResourceManager(), {"400", "196"});
	buildPanel->setOnToolSelected(
			[&](const auto &spec) { onToolSelected(spec); });

	auto add = [&](auto text, auto image, tgui::Widget::Ptr view) {
		gui.add(view);

		auto texture = game->getResourceManager()->getTextureOrLoad(image);
		bottomBar->addButton(text, texture, view);
	};

	add("Build", "icon_build.png", buildPanel);
	add("Order", "icon_order.png", tgui::Label::create("Order"));
	add("People", "icon_people.png", tgui::Label::create("People"));
	add("Menu", "icon_menu.png", tgui::Label::create("Menu"));

	auto resources = game->getResourceManager();
	auto blueprint = resources->getTileset(TextureType::BLUEPRINT);
	if (!blueprint) {
		LogF(ERROR, "Error loading blueprint tileset!");
		return false;
	}

	ArchitectController *controller = game->getModeController()->getArchitect();
	auto uniqueLayer = std::make_unique<ArchitectLayer>(worldShader, blueprint,
			game->getWorld(), game->getResourceManager(), game->getDefMan(),
			controller);
	layer = uniqueLayer.get();
	uniqueLayer->disable();
	worldView->registerLayer(std::move(uniqueLayer));

	return true;
}

void ArchitectView::activate() {
	ArchitectController *controller = game->getModeController()->getArchitect();
	buildPanel->setSpecs(controller->getAvailableToolSpecs());

	bottomBar->setVisible(true);
	layer->enable();

	hudView->setViewBoxing(30);
	worldView->setCursor(game->getModeController()->getArchitect());

	controller->setPlot(1);
	worldView->rebuildLayer(layer);

	auto input = controller->getInput();
	for (int i = 0; i < 8; i++) {
		auto action = static_cast<Action>(static_cast<int>(Action::TAB_1) + i);
		inputConnections.push_back(
				input->connect(action, [=]() { bottomBar->openTab(i); }));
	}
}

void ArchitectView::deactivate() {
	inputConnections.clear();

	bottomBar->setVisible(false);
	layer->setTool(std::unique_ptr<ITool>());
	layer->disable();
	bottomBar->closeTab();

	ArchitectController *controller = game->getModeController()->getArchitect();
	controller->setPlot(0);
	worldView->rebuildLayer(layer);
	worldView->resetCursor();
}

bool ArchitectView::exit() {
	if (layer->hasTool()) {
		layer->setTool(std::unique_ptr<ITool>());
		return true;
	}
	return bottomBar->closeTab();
}

void ArchitectView::onToolSelected(const ToolSpec &spec) {
	LogF(INFO, "Selected tool %s, of type %d", spec.description.c_str(),
			(int)spec.type);

	ArchitectController *controller = game->getModeController()->getArchitect();

	switch (spec.type) {
	case ToolType::CANCEL:
		layer->setTool(std::make_unique<CancelTool>(controller, spec));
		return;
	case ToolType::BUILD: {
		auto def = game->getDefMan()->getTileDef(spec.itemName);
		layer->setTool(std::make_unique<BuildTool>(controller, def, spec));
		return;
	}
	case ToolType::DECONSTRUCT:
		layer->setTool(std::make_unique<DeconstructTool>(controller, spec));
		return;
	}

	FatalError("Unable to create tool");
}

void ArchitectView::update(float dtime) {
	layer->update(dtime);
}
