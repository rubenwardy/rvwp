#pragma once

#include "types/Controller.hpp"
#include "../../types/types.hpp"
#include <SFML/Graphics.hpp>

#include <memory>

namespace client {

class View : public Controller {
public:
	using Ptr = std::shared_ptr<View>;

	virtual ~View() = default;

	virtual bool load() { return true; }
	virtual void activate() {}
	virtual void deactivate() {}

	virtual void update(float dtime) {}

	virtual void draw(sf::RenderTarget &target, sf::RenderStates states) = 0;
	virtual void drawHUD(sf::RenderTarget &target, sf::RenderStates states) {}
	virtual void drawPostGUI(
			sf::RenderTarget &target, sf::RenderStates states) {}

	virtual void resize(unsigned int width, unsigned int height) {}

	/// Exit/back was requested, try to deselect.
	///
	/// @return event handled
	[[nodiscard]] virtual bool exit() { return false; }
};

} // namespace client
