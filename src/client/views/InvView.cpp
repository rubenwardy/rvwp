#include "InvView.hpp"
#include "client/Game.hpp"
#include <cassert>

using namespace client;

InvView::InvView(Game *game, EventBus<content::InventoryCommand> *inventoryBus,
		tgui::Gui &gui)
		: game(game), gui(gui),
		  provider(std::bind(&Game::getInventory, game, std::placeholders::_1)),
		  selection(provider, inventoryBus),
		  listener(inventoryBus, [this](auto e) { onInventoryAction(e); }) {}

bool InvView::load() {
	window = tgui::ChildWindow::create("Inventory");
	window->setPosition("&.width / 2 - width / 2", "&.height / 2 - height / 2");
	window->setClientSize({464, 474});
	window->setPositionLocked(true);
	window->setTitleTextSize(17);
	gui.add(window);

	auto close = [&]() {
		game->guiExit();
		return true;
	};
	window->onEscapeKeyPress(close);
	window->onClose(close);

	ilHB = InvListWidget::create(selection, game->getDefMan(),
			game->getResourceManager(), provider, {});
	ilHB->setPosition("50% - width / 2", "10");
	window->add(ilHB);

	ilMain = InvListWidget::create(selection, game->getDefMan(),
			game->getResourceManager(), provider, {});
	ilMain->setPosition("10", "100% - height - 10");
	window->add(ilMain);

	invListWidgets = {ilMain, ilHB};

	window->setVisible(false);

	return true;
}

void InvView::activate() {
	auto inv = game->getInventory("hero");
	assert(inv);

	ilMain->setInventoryList(inv->get("main"));
	ilHB->setInventoryList(inv->get("hotbar"));
	window->setVisible(true);
}

void InvView::deactivate() {
	window->setVisible(false);
}

void InvView::draw(sf::RenderTarget &target, sf::RenderStates states) {
	auto mp = sf::Mouse::getPosition(game->getWindow());
	selection.setPosition(mp.x, mp.y);
}

void InvView::drawPostGUI(sf::RenderTarget &target, sf::RenderStates states) {
	if (!selection.empty()) {
		target.draw(selection);
	}
}

void InvView::update(float delta) {}

void InvView::onInventoryAction(const content::InventoryCommand &action) {
	for (const auto &ils : invListWidgets) {
		ils->rebuild(ils->getInventoryList());
	}
}
