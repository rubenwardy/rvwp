#include "WorldView.hpp"
#include "StandardLayer.hpp"
#include "LightDebugLayer.hpp"
#include "DebugUILayer.hpp"
#include "EntityLayer.hpp"
#include "ParticleLayer.hpp"
#include "client/Game.hpp"
#include <functional>

using namespace client;

WorldView::WorldView(
		Game *game, const sf::Font &font, EventBus<world::WorldEvent> *worldBus)
		: game(game), listener(worldBus,
							  std::bind(&WorldView::onWorldEvent, this,
									  std::placeholders::_1)) {
	renderLayers.emplace_back(std::make_unique<StandardLayer>(worldShader));

	auto lightDebug = std::make_unique<LightDebugLayer>(font);
	lightDebugLayer = lightDebug.get();
	renderLayers.emplace_back(std::move(lightDebug));
	lightDebugLayer->disable();

	auto particles = std::make_unique<ParticleLayer>(
			game->getResourceManager(), game->getWorld());
	particleLayer = particles.get();
	renderLayers.emplace_back(std::move(particles));

	renderLayers.emplace_back(
			std::make_unique<EntityLayer>(game->getResourceManager()));

	auto debug = std::make_unique<DebugUILayer>(font);
	debugLayer = debug.get();
	renderLayers.emplace_back(std::move(debug));

#ifdef NDEBUG
	debugLayer->disable();
#endif
}

bool WorldView::load() {
	auto resources = game->getResourceManager();

	if (!resources->getTileset(TextureType::DIFFUSE)) {
		LogF(ERROR, "Error loading diffuse tileset!");
		return false;
	}

	auto normals = resources->getTileset(TextureType::NORMAL);
	if (!normals) {
		LogF(ERROR, "Error loading normal tileset!");
		return false;
	}

	auto crosshair = resources->getTextureOrLoad("crosshair.png");
	if (!crosshair) {
		LogF(ERROR, "Error loading crosshair!");
		return false;
	}

	if (!worldShader.loadFromFile(
				"assets/shaders/world.vert", "assets/shaders/world.frag")) {
		LogF(ERROR, "Error loading world shader!");
		return false;
	}

	s_crosshair.setTexture(*crosshair);

	return true;
}

inline void drawSelectionBox(
		sf::RenderTarget &target, sf::Vector2f minPos, sf::Vector2f size) {
	minPos = minPos * (float)TILE_SIZE + sf::Vector2f(4, 4);
	size = size * (float)TILE_SIZE - sf::Vector2f(8, 8);

	sf::RectangleShape rectangle(size);
	rectangle.setPosition(minPos);
	rectangle.setOutlineColor(sf::Color(0x00000080));
	rectangle.setOutlineThickness(4);
	rectangle.setFillColor(sf::Color::Transparent);
	target.draw(rectangle);
}

void WorldView::draw(sf::RenderTarget &target, sf::RenderStates states) {
	float daylight = game->getTime().getDaylight();
	assert(daylight >= 0 && daylight <= 1);

	content::DefinitionManager *definitionManager = game->getDefMan();
	ResourceManager *resources = game->getResourceManager();
	assert(definitionManager);
	assert(resources);

	const auto &camera = game->getCamera();

	camera_view.setCenter(sf::Vector2f(floorf(camera.pos.x * (float)TILE_SIZE),
			floorf(camera.pos.y * (float)TILE_SIZE)));

	if (camera.zoom >= 1.f) {
		camera_view.setSize((sf::Vector2f)target.getSize() * camera.zoom);
	} else {
		camera_view.setSize(
				(sf::Vector2f)target.getSize() / (2.f - camera.zoom));
	}

	int camera_z = camera.pos.z;
	int focus_z = camera.focal_z;
	sf::FloatRect camera_rect(
			camera_view.getCenter() - camera_view.getSize() / 2.f,
			camera_view.getSize());

	target.setView(camera_view);

	worldShader.setUniform(
			"normalmap", *resources->getTileset(TextureType::NORMAL));
	worldShader.setUniform("daylight", daylight);

	if (renderMode == 3) {
		states.texture = resources->getTileset(TextureType::NORMAL);
		states.shader = nullptr;
	} else {
		states.texture = resources->getTileset(TextureType::DIFFUSE);
		if (renderMode != 4) {
			states.shader = &worldShader;
		}
	}

	int last_z = -100;
	for (const auto &pair : renderZLevels) {
		const auto &layer = pair.second;
		if (layer.z > camera_z) {
			break;
		}

		assert(layer.z > last_z);
		last_z = layer.z;
		worldShader.setUniform("factor", 0.1f * (float)(focus_z - layer.z));

		for (auto &renderLayer : renderLayers) {
			renderLayer->draw(target, states, layer, camera_rect);
		}
	}

	// Selection box
	if (cursor) {
		SelectionSpec spec = cursor->getInteractionSelection();

		if (spec.type == SelectionSpec::SST_Tile) {
			const auto def = spec.tile
					? definitionManager->getTileDef(spec.tile->cid)
					: nullptr;
			if (def) {
				for (const auto &box : def->collisionBoxes) {
					drawSelectionBox(target,
							spec.pos.floor().floating() + box.minPos, box.size);
				}
			} else {
				drawSelectionBox(target, spec.pos.floor(), {1, 1});
			}
		} else if (spec.type == SelectionSpec::SST_Entity) {
			drawSelectionBox(target,
					spec.entity->getPosition() - V3f(0.5f, 0.5f, 0), {1, 1});
		}

		if (cursor->shouldShowCrosshair()) {
			auto pos = cursor->getCursorPosition();
			if (pos.has_value()) {
				s_crosshair.setPosition(pos.value() * (float)TILE_SIZE);
				target.draw(s_crosshair);
			}
		}
	}
}

void WorldView::update(float dtime) {
	particleLayer->update(dtime);
}

void WorldView::buildMesh(WorldChunk *chunk) {
	SanityCheck(chunk);

	numChunksMeshGenned++;

	// Get or create render data
	RenderZLevel *level;
	auto layer_it = renderZLevels.find(chunk->pos.z);
	if (layer_it == renderZLevels.end()) {
		renderZLevels.emplace(chunk->pos.z, RenderZLevel(chunk->pos.z));
		level = &renderZLevels[chunk->pos.z];
	} else {
		level = &layer_it->second;
	}
	assert(level);
	assert(level->z == chunk->pos.z);

	RenderChunk *data = level->getOrCreate(chunk);
	assert(data->chunk == chunk);
	assert(data->getPos() == chunk->pos);

	// Initialise variables
	content::DefinitionManager *def = game->getDefMan();
	ResourceManager *resources = game->getResourceManager();
	World *world = game->getWorld();

	// Generate meshes
	MeshGen meshGen(def, resources, world);
	meshGen.buildFloorMesh(data->floorMesh, chunk);

	ChunkConnectionMap bits;
	std::fill(bits.begin(), bits.end(), 0);

	meshGen.spreadConnections(bits, &chunk->tiles[0]);
	meshGen.buildTileMesh(
			data->tilesMesh, &chunk->tiles[0], CPosToPos(chunk->pos), bits);
	meshGen.updateLightMapTexture(data->lightMapTexture, chunk);

	for (auto dir : NEIGHBOURS_4) {
		auto neighbourChunk =
				world->getChunk(chunk->pos + V3s(dir.x, dir.y, 0));
		if (!neighbourChunk) {
			continue;
		}

		auto neighbourData = level->get(neighbourChunk);
		if (!neighbourData) {
			continue;
		}

		meshGen.updateLightMapTexture(
				neighbourData->lightMapTexture, neighbourChunk);
	}

	for (auto &layer : renderLayers) {
		layer->build(*data);
	}
}

void WorldView::rebuildLayer(IWorldRenderLayer *layer) {
	for (auto &level : renderZLevels) {
		for (auto &chunk : level.second.all()) {
			layer->build(chunk);
		}
	}
}

V3f WorldView::getWorldPositionFromScreen(sf::RenderWindow *window, V2s pos) {
	sf::Vector2f mp = window->mapPixelToCoords({pos.x, pos.y}, camera_view) /
			(float)TILE_SIZE;
	return {mp.x, mp.y, game->getCamera().focal_z};
}

void WorldView::onWorldEvent(const WorldEvent &event) {
	if (event.type == WorldEvent::CHUNK_ACTIVATED ||
			(event.type == WorldEvent::SET_TILE && event.chunk->isActive())) {
		buildMesh(event.chunk);
	} else if (event.type == WorldEvent::WORK_CREATED ||
			event.type == WorldEvent::WORK_REMOVED) {
		rebuildLayer(getLayer("Architect"));
	}
}

void WorldView::spawnSingleParticle(const V3f &pos, const V3f &velocity,
		const content::Material &material) {
	particleLayer->addSingleParticle(pos, velocity, material);
}
