#pragma once

#include "MeshGen.hpp"

using namespace world;

namespace client {

struct RenderChunk {
	explicit RenderChunk(world::WorldChunk *c) : chunk(c) {
		if (!lightMapTexture.create(LIGHTMAP_SIZE, LIGHTMAP_SIZE)) {
			LogF(ERROR, "[WorldView] Unable to create render texture");
			abort();
		}
	}

	world::WorldChunk *chunk = nullptr;
	sf::VertexArray tilesMesh;
	sf::VertexArray floorMesh;
	sf::VertexArray architectMesh;
	sf::Texture lightMapTexture;

	const V3s &getPos() const { return chunk->pos; }
};

class RenderZLevel {
	std::vector<RenderChunk> render_chunks;

public:
	RenderZLevel() = default;
	explicit RenderZLevel(int z) : z(z) {}
	int z = 0;

	RenderChunk *get(WorldChunk *c) {
		for (auto &r : render_chunks) {
			if (r.getPos() == c->pos) {
				return &r;
			}
		}

		return nullptr;
	}

	/// Warning: will invalidate any pointers to RenderChunks in this layer
	RenderChunk *getOrCreate(WorldChunk *c) {
		RenderChunk *ret = get(c);
		if (ret) {
			ret->chunk = c;
			return ret;
		}

		render_chunks.emplace_back(c);
		return &render_chunks[render_chunks.size() - 1];
	}

	size_t size() const { return render_chunks.size(); }

	std::vector<RenderChunk> &all() { return render_chunks; }
	const std::vector<RenderChunk> &peek() const { return render_chunks; }

	bool operator<(const RenderZLevel &other) const { return z < other.z; }
};

class IWorldRenderLayer {
	bool enabled = true;

protected:
	virtual void draw_impl(sf::RenderTarget &target, sf::RenderStates states,
			const RenderZLevel &level, const sf::FloatRect &cameraRect) = 0;

public:
	const std::string name;

	IWorldRenderLayer(std::string name) : name(name) {}

	IWorldRenderLayer(const IWorldRenderLayer &other) = delete;

	virtual ~IWorldRenderLayer() = default;

	void draw(sf::RenderTarget &target, sf::RenderStates states,
			const client::RenderZLevel &level,
			const sf::FloatRect &cameraRect) {
		if (enabled) {
			draw_impl(target, states, level, cameraRect);
		}
	}

	virtual void build(RenderChunk &chunk) {}

	virtual void enable() { enabled = true; }
	virtual void disable() { enabled = false; }
};

} // namespace client
