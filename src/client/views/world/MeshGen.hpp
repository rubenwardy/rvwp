#pragma once

#include "world/Chunk.hpp"

#include <SFML/Graphics.hpp>
#include <array>

namespace world {
class World;
}

namespace content {
class DefinitionManager;
}

namespace client {

const size_t LIGHTMAP_SIZE = (CHUNK_SIZE + 2) * 2;

using ChunkConnectionMap = std::array<u8, CHUNK_SIZE * CHUNK_SIZE>;

class ResourceManager;
class MeshGen {
	content::DefinitionManager *def;
	ResourceManager *resourceManager;
	world::World *world;

public:
	MeshGen(content::DefinitionManager *def, ResourceManager *resourceManager,
			world::World *world)
			: def(def), resourceManager(resourceManager), world(world) {}

	/// Create lightmap texture for a chunk
	///
	/// @param texture Output texture
	/// @param chunk The chunk
	void updateLightMapTexture(sf::Texture &texture, world::WorldChunk *chunk);

	/// Spread tile connections for the given tiles.
	///
	/// @param out Return array
	/// @param tiles Tile layer
	void spreadConnections(ChunkConnectionMap &out, world::WorldTile *tiles);

	/// Build the floor mesh from the given chunk
	///
	/// @param floorMesh Output
	/// @param chunk The chunk
	void buildFloorMesh(sf::VertexArray &floorMesh, world::WorldChunk *chunk);

	/// Builds a tile mesh from the given tiles
	///
	/// @param out Result
	/// @param tiles The tiles
	/// @param origin Chunk origin position
	/// @param bits Connection map bits, from spreadConnections
	void buildTileMesh(sf::VertexArray &out, world::WorldTile *tiles,
			const V3s &origin, const ChunkConnectionMap &bits);

	/// Set vertices in quads
	void setQuad(sf::Vertex *quad, int x, int y, int tu, int tv);
};

} // namespace client
