#pragma once
#include "../WorldController.hpp"
#include "types/Controller.hpp"
#include "client/input/InputController.hpp"
#include "../CameraController.hpp"
#include "../ICamera.hpp"
#include "world/EntityPhysicsController.hpp"

#include <string>

namespace client {

class WorldController;
class HeroController;
class HeroEntityController : public Controller {
	HeroController *heroController;
	world::World *world;
	IInput *input;
	ICamera *cameraController;
	std::vector<InputConnection> inputConnections;

	world::Entity *entity = nullptr;
	std::optional<world::EntityPhysicsController> physics;

public:
	HeroEntityController(HeroController *heroController, world::World *world,
			IInput *input, ICamera *cameraController);

	void setEntity(world::Entity *e) {
		entity = e;
		physics.reset();

		if (e) {
			enable();
		} else {
			disable();
		}
	}

	world::Entity *getEntity() { return entity; }

	V3f getPosition() const {
		if (entity) {
			return entity->getPosition();
		} else {
			return V3f().makeInvalid();
		}
	}

	void teleportTo(V3f pos);

	void update(float dtime) override;
	void jump();
};

} // namespace client
