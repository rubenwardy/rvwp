#include "ArchitectController.hpp"
#include "client/controllers/CameraController.hpp"

#include <cmath>

using namespace client;
using namespace world;
using namespace content;

world::Plot *ArchitectController::getPlot() {
	return world->getPlot(plotId);
}

void ArchitectController::update(float dtime) {
	if (input->getCurrentActionState() != ActionState::INGAME) {
		return;
	}

	float max_velocity =
			15.f * std::max(1.f, cameraController->getCamera().zoom);
	cameraController->move(input->getMoveVector() * dtime * max_velocity);
}

std::optional<V3f> ArchitectController::getCursorPosition() const {
	auto cameraPos = cameraController->getCamera().pos;
	auto pos = input->getCursorPosition(cameraPos);
	if (pos) {
		return V3f(pos.value().x, pos.value().y, cameraPos.z);
	}

	return {};
}

world::SelectionSpec ArchitectController::getInteractionSelection() const {
	auto o_cursor = getCursorPosition();
	if (!o_cursor.has_value()) {
		return {};
	}

	return world::SelectionSpec::makeTile(nullptr, o_cursor.value());
}

void ArchitectController::cancelWork(V3s from, V3s to) {
	auto plot = getPlot();
	if (!plot) {
		LogF(ERROR, "[Architect] Unable to cancel work as no plot is selected");
		return;
	}

	std::vector<Work *> works;
	plot->getWorkInBounds(works, Rect3s::fromInclusive(from, to));

	for (const auto work : works) {
		u32 workId = work->id;
		if (world->removeWork(workId)) {
			bus->push(ArchitectEvent::removeWork(plot->id, workId));
		}
	}
}

void ArchitectController::addBuildWork(V3s from, V3s to,
		const content::TileDef *def, world::ChunkLayer layer) {
	auto plot = getPlot();
	if (!plot) {
		LogF(ERROR, "[Architect] Unable to create work as no plot is selected");
		return;
	}

	for (int y = from.y; y <= to.y; y++) {
		for (int x = from.x; x <= to.x; x++) {
			auto work = world->addWork(plot,
					std::make_unique<Work>("build",
							Rect3s({x, y, from.z}, {1, 1, 1}), def->material,
							def->name, layer));
			if (work) {
				bus->push(ArchitectEvent::newWork(plotId, work));
			} else {
				LogF(ERROR, "Failed to create work at %d, %d, %d", x, y,
						from.z);
			}
		}
	}
}

void ArchitectController::addDeconstructWork(
		V3s from, V3s to, const ToolSpec &spec) {
	auto plot = getPlot();
	if (!plot) {
		LogF(ERROR, "[Architect] Unable to create work as no plot is selected");
		return;
	}

	for (int y = from.y; y <= to.y; y++) {
		for (int x = from.x; x <= to.x; x++) {
			auto tile = world->get(V3s(x, y, from.z), spec.layer);
			if (!tile || tile->empty()) {
				continue;
			}

			auto work = world->addWork(plot,
					std::make_unique<Work>("deconstruct",
							Rect3s({x, y, from.z}, {1, 1, 1}), spec.material,
							"", spec.layer));
			if (work) {
				bus->push(ArchitectEvent::newWork(plotId, work));
			} else {
				LogF(ERROR, "Failed to create work at %d, %d, %d", x, y,
						from.z);
			}
		}
	}
}
