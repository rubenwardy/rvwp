#pragma once

#include <SFML/System.hpp>
#include "../models/Camera.hpp"

namespace world {
class Entity;
}

namespace client {

class ICamera {
public:
	virtual ~ICamera() = default;

	virtual void follow(world::Entity *e) = 0;
	virtual void move(sf::Vector2f delta) = 0;
	virtual const Camera &getCamera() const = 0;
};

} // namespace client
