#include "InvSelection.hpp"
#include <SFML/Graphics.hpp>

using namespace client;

void InvSelection::clickWithMaterial(const content::StackLocation &location,
		int _count, const ClientMaterial &material) {
	if (!empty()) {
		moveTo(location, _count);
	} else if (_count > 0) {
		select(location, _count);
	}

	if (!empty()) {
		setTexture(*material.texture);
		setTextureRect(material.getRectInTexture());
	}
}

/*

	auto inv = controller->getInventory();
	const auto &item = inv->get(selected_list, selected_idx);
	auto def = controller->getDefMan()->getItemDef(item.cid);
	ClientMaterial material =
   controller->getResourceManager()->getMaterialOrLoad(def->material);

	sf::Vector2i pos = sf::Mouse::getPosition(*rwindow);
	sf::Vector2f size(material.w, material.h);
	material.draw(rwindow, sf::Vector2f(pos.x, pos.y) - size * 0.5f);




	if (selected_idx >= 0) {
		auto inv = controller->getPlayerInventory();
		auto selectedStack = inv->get(selected_list, selected_idx);
		bool isPartialSelection = selected_count != selectedStack.count;

		if (isPartialSelection) {
			if (itemCount > 0) {
				// TODO: move stack out of the way, cancel otherwise
			}
			// TODO: move partial stack to slot
		} else if (controller->doItemSwap(selected_list, selected_idx, listname,
   idx)) { needs_rebuild = true;
		}

		if (itemCount <= 0) {
			stopSelection();
		}
	} else if (itemCount) {
		startSelection(listname, idx, itemCount);
	}


 */
