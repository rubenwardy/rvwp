#include "InvListWidget.hpp"

using namespace client;

void InvListWidget::setInventoryList(
		std::optional<content::InventoryList *> list) {
	if (list) {
		inventoryLocation = list.value()->getParent();
		listName = list.value()->name;
	} else {
		inventoryLocation = content::InventoryLocation();
		listName = "";
	}

	rebuild(list);
}

void InvListWidget::rebuild(
		std::optional<content::InventoryList *> optionalList) {
	removeAllWidgets();

	if (!optionalList) {
		return;
	}

	auto list = optionalList.value();

	int x = 0;
	int y = 0;
	for (size_t i = 0; i < list->items.size(); ++i) {
		auto item = list->items[i];

		auto slot = tgui::BoxLayout::create({64, 64});

		if (!selection.empty() &&
				selection.getSelection() == list->getLocation(x, y)) {
			item.count -= selection.getCount();
		}

		auto button = tgui::Button::create();
		button->setRenderer(tgui::Theme::getDefault()->getRenderer("ItemSlot"));
		button->setSize(64, 64);
		button->onPress(&InvListWidget::onSlotClicked, this, (int)i);

		slot->add(button);

		if (!item.empty()) {
			auto def = defMan->getItemDef(item.name);
			auto cMaterial = resourceManager->getMaterialOrLoad(def->material);

			if (cMaterial.texture) {
				auto rect = tgui::UIntRect(cMaterial.getURectInTexture());
				auto image = tgui::Picture::create(
						tgui::Texture(*cMaterial.texture, rect));
				image->setPosition("5", "5");
				image->setSize("100% - 10", "100% - 10");
				image->ignoreMouseEvents(true);
				slot->add(image);
			}

			button->setToolTip(createToolTip(item, def));

			auto label = tgui::Label::create(std::to_string(item.count));
			label->setPosition("5", "5");
			label->setSize("100% - 10", "100% - 10");
			label->setHorizontalAlignment(
					tgui::Label::HorizontalAlignment::Right);
			label->setVerticalAlignment(tgui::Label::VerticalAlignment::Bottom);
			label->ignoreMouseEvents(true);
			slot->add(label);
		}

		addWidget(slot, y, x, Alignment::Center, {5, 5});

		x++;
		if (x >= list->w) {
			x = 0;
			y++;
		}
	}
}

void InvListWidget::onSlotClicked(int i) {
	auto list = getInventoryList().value();
	if (!list) {
		return;
	}

	auto &stack = list->get(i);
	auto location = list->getLocation(i);
	client::ClientMaterial material{};

	if (!stack.empty()) {
		auto def = defMan->getItemDef(stack.name);
		material = resourceManager->getMaterialOrLoad(def->material);
	}

	selection.clickWithMaterial(location, stack.count, material);
	rebuild(list);
}

InvListWidget::Ptr InvListWidget::create(InvSelection &_selection,
		content::DefinitionManager *_defMan, ResourceManager *_resourceManager,
		content::InventoryProvider _provider,
		std::optional<content::InventoryList *> inventoryList) {
	auto invlist = std::make_shared<InvListWidget>(
			_selection, _defMan, _resourceManager, std::move(_provider));
	invlist->setInventoryList(inventoryList);
	return invlist;
}

tgui::Widget::Ptr InvListWidget::createToolTip(
		const content::ItemStack &stack, const content::ItemDef *def) {
	std::ostringstream os;
	os << def->title << "\n"
	   << "\nname: " << stack.name;

	auto label = tgui::Label::create(os.str());
	label->setRenderer(tgui::Theme::getDefault()->getRenderer("ToolTip"));
	return label;
}
