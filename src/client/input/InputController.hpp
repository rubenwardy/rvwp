#pragma once

#include "types/Controller.hpp"
#include "IInputResolver.hpp"
#include "Action.hpp"
#include "IInput.hpp"

#include <utility>
#include <string>
#include <Thor/Input.hpp>

namespace client {

/// Class to provide a unified input API, independent from input method.
class InputController : public IInputController {
	using CallbackSystem = thor::ActionMap<Action>::CallbackSystem;

	CallbackSystem system;
	std::vector<std::unique_ptr<IInputResolver>> input_resolvers;
	std::function<ActionState()> stateProvider;
	std::vector<std::unique_ptr<InputListener>> inputListeners;

public:
	/// Set the ActionState provider
	///
	/// @param v ActionState provider
	void setActionStateProvider(
			const std::function<ActionState()> &v) override {
		stateProvider = v;
	}

	/// Inject a resolver to handle an input method
	///
	/// @param resolver InputResolver
	void addResolver(std::unique_ptr<IInputResolver> &&resolver) {
		input_resolvers.push_back(std::move(resolver));
	}

	/// Return the action state
	///
	/// @return action state
	ActionState getCurrentActionState() const override {
		return stateProvider();
	}

	/// Get movement vector.
	///
	/// In hero mode, this is the player movement.
	/// In architect mode, this is the camera mode.
	///
	/// @return normalised vector
	V3f getMoveVector() override;

	/// Return the cursor's current position. For keyboard+mouse,
	/// this is just the mouse position. For gamepad, this is relative
	/// to the player position - called "origin" for abstraction here.
	///
	/// This differs from a selection in that raycasting isn't done,
	/// and it is an exact floating-point position rather than integer.
	///
	/// @param origin Usually the player position
	/// @return Optional cursor position, in world co-ordinates
	std::optional<V3f> getCursorPosition(const V3f &origin) override;

	/// @return Whether the cursor is virtual, ie: a gamepad joystick
	bool isVirtualCursor() const override;

	/// Subscribe to an action
	///
	/// @param action The action
	/// @param listener Listener function
	/// @returns InputConnection Store this to avoid the listener from being
	/// de-registered.
	[[nodiscard]] InputConnection connect(
			Action action, const std::function<void()> &listener) override {
		return connectWithThorContext(action, [=](auto context) {
			if (IsActionAllowed(action, getCurrentActionState())) {
				listener();
			}
		});
	}

	/// Subscribe to an action
	///
	/// @param action The action
	/// @param listener Listener function
	/// @returns InputConnection Store this to avoid the listener from being
	/// de-registered.
	[[nodiscard]] InputConnection connectWithThorContext(Action action,
			const std::function<void(thor::ActionContext<Action>)> &listener) {
		auto mylistener =
				std::make_unique<InputListener>(listener, system, action);
		auto ptr = mylistener.get();
		inputListeners.emplace_back(std::move(mylistener));
		return InputConnection(ptr);
	}

	/// Whether an action is active
	///
	/// @param action Action
	/// @return Is pressed
	bool isActive(Action action) override;

	bool onEvent(const sf::Event &event) override;

	void update(float dtime) override;

private:
	IInputResolver *getMostRecentlyActiveResolver() const;

	/// Runs the function on each resolver, in order of resolver priority.
	///
	/// @tparam T
	/// @param func
	/// @return
	template <typename T>
	std::optional<T> firstInResolvers(
			std::function<std::optional<T>(IInputResolver *)> func);
};

} // namespace client
