#pragma once
#include "IInputResolver.hpp"
#include "types/types.hpp"

#include <SFML/Graphics.hpp>

namespace client {

class Game;
class GamepadResolver : public IInputResolver {
	enum JoystickType {
		NONE = 0,
		XBOX360,
		GENERIC,
	};

	JoystickType type = NONE;

	unsigned int joystick_id;
	int timeSinceLastActive = 6000;

	enum JoystickButton {
		BTN_X,
		BTN_A,
		BTN_B,
		BTN_Y,
		BTN_LT,
		BTN_RT,
		BTN_LB,
		BTN_RB,
		BTN_BACK,
		BTN_START,
		BTN_LJS_UP,
		BTN_LJS_DOWN,
		BTN_LJS_CLICK,
		BTN_RJS_CLICK,
		BTN_DPAD_UP,
		BTN_DPAD_LEFT,
		BTN_DPAD_RIGHT,
		BTN_DPAD_DOWN,
		BUTTONS_LENGTH
	};

	int buttons_map[BUTTONS_LENGTH];
	int trigger_lt = -1;
	int trigger_rt = -1;
	int button_was_pressed[BUTTONS_LENGTH] = {false};

public:
	explicit GamepadResolver(unsigned int joystick_id);
	void reconfigure();

	inline int getId() { return joystick_id; }

	int getTimeSinceLastActive() const override { return timeSinceLastActive; }

	void update(float dtime) override;

	V3f getMoveVector() override;
	std::optional<V3f> getCursorPosition(const V3f &origin) override;
	bool isVirtualCursor() const override { return true; }

private:
	bool checkForActivity() const;
};

} // namespace client
