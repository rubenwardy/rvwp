#pragma once

#include <SFML/Graphics.hpp>
#include "IInputResolver.hpp"
#include "types/Controller.hpp"

namespace client {

class Game;
class KeyboardMouseResolver : public IInputResolver {
	const sf::Window &window;
	std::function<V3f(V2s)> mapper;
	int timeSinceLastActive = 1000;
	sf::Vector2i lastMousePos{};

public:
	KeyboardMouseResolver(
			const sf::Window &window, std::function<V3f(V2s)> mapper);

	void update(float dtime) override;
	int getTimeSinceLastActive() const override { return timeSinceLastActive; }
	V3f getMoveVector() override;
	std::optional<V3f> getCursorPosition(const V3f &origin) override;
	bool isVirtualCursor() const override { return false; }
};

} // namespace client
