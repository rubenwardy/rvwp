#pragma once
#include <SFML/Graphics.hpp>
#include <TGUI/TGUI.hpp>
#include <TGUI/Backend/SFML-Graphics.hpp>

#include "world/GameTime.hpp"
#include "network/net.hpp"
#include "content/content.hpp"
#include "types/Controller.hpp"
#include "content/Inventory.hpp"
#include "content/InventoryCommand.hpp"
#include "events/EventBus.hpp"

#include "models/Camera.hpp"
#include "controllers/WorldController.hpp"
#include "controllers/ModeController.hpp"
#include "ResourceManager.hpp"
#include "views/View.hpp"
#include "models/ArchitectEvent.hpp"
#include "models/HeroEvent.hpp"
#include "models/ClientHeroEvent.hpp"

#include "ClientEventListener.hpp"
#include "GameView.hpp"
#include "EntityFootstepPlayer.hpp"

namespace client {

class Client;
class CameraController;
class ArchitectController;
class Game {
	// Event buses
	EventBus<ArchitectEvent> architectEventBus;
	EventBus<HeroEvent> heroEventBus;
	EventBus<ClientHeroEvent> clientHeroEventBus;
	EventBus<world::WorldEvent> worldBus;
	EventBus<content::InventoryCommand> inventoryCommandBus;

	std::string username;
	world::GameTime time;

	sf::RenderWindow *window;	// window is owned in main
	InputController *input_ctr; // owned in main

	sf::View hud_view;

	tgui::Gui &gui;
	std::unique_ptr<Client> client;
	ClientEventListener clientEventListener;
	std::unique_ptr<CameraController> camera_ctr;
	std::unique_ptr<ModeController> modeController;
	std::unique_ptr<content::DefinitionManager> def;
	std::unique_ptr<ResourceManager> resources;
	std::unique_ptr<WorldController> world_ctr;
	std::unique_ptr<EntityFootstepPlayer> entityFootsteps;
	std::unique_ptr<GameView> gameView;
	std::vector<Controller *> controllers;

	std::vector<InputConnection> inputConnections;

	// Debug info
	float debug_dtime_accumulator = 0;
	float debug_graph_update_delta = 0;
	int ticks_since_last_graph_update = 0;
	int packets = 0;

	/// Adds controller to controllers, causes update() to be called.
	void registerController(Controller *ctr) { controllers.push_back(ctr); }

public:
	/// Last render time
	///
	/// Used in HudView.
	float last_rtime = 0;

	Game(sf::RenderWindow *window, tgui::Gui &gui, sf::Font &font,
			InputController *input_ctr);
	Game(const Game &other) = delete;
	~Game();

	bool load(network::Address server, const std::string &username,
			const std::string &password);
	void run();
	void update(float delta);
	bool onEvent(const sf::Event &event);
	void resize(unsigned int width, unsigned int height);

	//
	// Public API
	//  - called by controllers and the client
	//

	inline content::DefinitionManager *getDefMan() const { return def.get(); }

	inline ResourceManager *getResourceManager() const {
		return resources.get();
	}

	inline WorldController *getWorldCtr() const { return world_ctr.get(); }

	inline IInput *getInput() const { return input_ctr; }

	inline world::World *getWorld() const { return world_ctr->getWorld(); }

	const Camera &getCamera() const;

	sf::Window &getWindow() { return *window; }

	inline const world::GameTime &getTime() const { return time; }

	void updateTime(const world::GameTime &v) { time = v; }

	/// Gets an inventory by its name.
	///
	/// WARNING: Do not store pointers to inventories!
	content::Inventory *getInventory(const std::string &name = "hero");

	content::Inventory *getPlayerInventory();

	ModeController *getModeController() { return modeController.get(); }

	/// Push message to chat log. Does not send a packet to the server
	void displayMessageToChat(const std::string &message);

	/// Close any open things, but don't exit the game
	void closeAllOpen();

	/// Escape was pressed. Either exit the current window or exit the game.
	void guiExit();

	/// Sends chat message to server. This may also be a command
	void say(const std::string &message);

	void doCameraZChange(bool is_up);

	void doSwitchDebugMode();
	void doSwitchRenderMode();
	void doSwitchPlayerMode();

	void guiChangeFocus(bool isUp);
	void spawnSingleParticle(const V3f &pos, const V3f &velocity,
			const content::Material &material);
};

} // namespace client
