#pragma once

namespace client {

class ClientLauncher {
public:
	///
	/// @return True for success, false for error
	bool launch();
};

} // namespace client
