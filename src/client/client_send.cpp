#include "Client.hpp"
#include "Game.hpp"
#include "../content/content.hpp"
#include <log.hpp>

using namespace network;
using namespace client;
using namespace world;

void Client::sendChatMessage(const std::string &message) {
	const size_t size = 2 + (int)message.size();

	Packet pkt(server, TOANY_CHAT_MESSAGE, size);
	pkt << message;

	socket.send(pkt);
}

int getSizeOfStackLocation(const content::StackLocation &location) {
	return 2 + (int)location.inv.name.size() + 2 + (int)location.list.size() +
			2;
}

void Client::sendPlayerInventoryCommand(
		const content::InventoryCommand &command) {
	const int size = 1 + getSizeOfStackLocation(command.getSource()) +
			getSizeOfStackLocation(command.getDestination()) + 2;

	Packet pkt(server, TOSERVER_PLAYER_INVENTORY_COMMAND, size);

	pkt << (u8)command.getType();

	pkt << command.getSource().inv.name;
	pkt << command.getSource().list;
	pkt << (u16)command.getSource().idx;

	pkt << command.getDestination().inv.name;
	pkt << command.getDestination().list;
	pkt << (u16)command.getDestination().idx;

	pkt << (u16)command.getCount();

	socket.send(pkt);
}

void Client::sendPlayerPosition() {
	if (!localPlayer) {
		Log("Client", ERROR) << "Client::sendPlayerPosition(): Unable to find "
								"player entity";
		return;
	}

	localPlayer->packet_order_counter++;

	const int size = 3 * 4 + 4 + 4 + 1 + 3 * 4;

	Packet pkt(server, TOSERVER_PLAYER_MOVE, size);
	pkt << localPlayer->getPosition();
	pkt << (f32)localPlayer->getYaw();
	pkt << (u32)localPlayer->packet_order_counter;
	pkt << (u8)0;
	pkt << game->getCamera().pos;
	socket.send(pkt);

	localPlayer->last_sent_position = localPlayer->getPosition();
}

void Client::sendSetTile(
		const V3s &pos, const WorldTile *tile, int hotbar_idx) {
	const int size = 3 * 4 + 4 + 1 + 2 + 4;
	Packet pkt(server, TOANY_WORLD_SET_TILE, size);
	pkt << pos;

	if (tile) {
		pkt << (u32)tile->cid;
	} else {
		pkt << (u32)0;
	}

	pkt << (u8)hotbar_idx;
	pkt << (u16)100; // HP
	pkt << (u32)0;	 // Number of metadata items

	socket.send(pkt);
}

void Client::sendTileInteract(const V3s &pos) {
	const int size = 3 * 4;
	Packet pkt(server, TOSERVER_TILE_INTERACT, size);
	pkt << pos;
	socket.send(pkt);
}

void Client::sendPunchEntity(
		const Entity *entity, u8 hotbar_idx, u16 damage_done) {
	const int size = 4 + 4 + 1 + 2 + 2;
	Packet pkt(server, TOSERVER_PUNCH_ENTITY, size);
	pkt << (u32)entity->id;
	pkt << (u32)entity->packet_order_counter;
	pkt << hotbar_idx;
	pkt << damage_done;
	pkt << (u16)entity->hp;
	socket.send(pkt);
}

void Client::sendWorkCreated(int plotId, world::Work *work) {
	// Don't send if the work was created on the server
	if (workClientToServer.find(work->id) != workClientToServer.end()) {
		LogF(ERROR,
				"Skipping sending work to the server, as it already has it");
		return;
	}

	const int size = 4 + 4 + 2 + work->type.size() + 2 * 3 * 4 + 2 +
			work->itemName.size() + work->material.serialised_size() + 1;
	Packet pkt(server, TOANY_WORLD_NEW_WORK, size);
	pkt << (u32)work->id;
	pkt << (u32)plotId;
	pkt << work->type;
	pkt << work->bounds;
	work->material.serialise(pkt);
	pkt << work->itemName;
	pkt << (u8)work->layer;
	socket.send(pkt);
}

void Client::sendWorkRemoved(u32 workClientId) {
	u32 serverId = workClientToServer[workClientId];

	const int size = 4 + 4;
	Packet pkt(server, TOANY_WORLD_REMOVE_WORK, size);
	pkt << serverId;
	pkt << workClientId;
	socket.send(pkt);
}

void Client::sendFireWeapon(const V3f &pos, float yaw, u8 hotbar_idx) {
	const int size = 3 * 4 + 4 + 1;
	Packet pkt(server, TOSERVER_FIRE_WEAPON, size);
	pkt << pos;
	pkt << (f32)yaw;
	pkt << hotbar_idx;
	socket.send(pkt);
}
