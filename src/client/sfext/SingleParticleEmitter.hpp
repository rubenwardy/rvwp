#pragma once
#include <SFML/Graphics.hpp>
#include <Thor/Math.hpp>
#include <Thor/Graphics.hpp>
#include <Thor/Particles.hpp>

namespace sfext {

class SingleParticleEmitter {
	std::vector<thor::Particle> particles;

public:
	SingleParticleEmitter() = default;
	SingleParticleEmitter(const SingleParticleEmitter &other) = delete;

	void operator()(thor::EmissionInterface &system, sf::Time dt);

	void push(thor::Particle particle);
	void push(sf::Vector2f position, sf::Vector2f velocity, float lifetime,
			float rot = 0, float rotsp = 0);
};

} // namespace sfext
