#include "FancyText.hpp"

using namespace sfext;

FancyText::~FancyText() = default;

void FancyText::draw(sf::RenderTarget &target, sf::RenderStates states) const {
	if (!dropShadow) {
		auto mutable_this = const_cast<FancyText *>(this);
		mutable_this->regenerate();
	}

	target.draw(*dropShadow, states);
}

void FancyText::regenerate() {
	dropShadow = std::make_unique<DropShadow>(
			static_cast<const sf::Drawable &>(text), getSize(),
			sf::Vector2f(2.f, 2.f), sf::Color(0x0000007F));

	text.setPosition(getPosition());
	text.setRotation(getRotation());
	text.setScale(getScale());
	text.setOrigin(getOrigin());
}
