#include "SingleParticleEmitter.hpp"

using namespace sfext;

void SingleParticleEmitter::operator()(
		thor::EmissionInterface &system, sf::Time dt) {
	for (auto particle : particles) {
		system.emitParticle(particle);
	}
	particles.clear();
}

void SingleParticleEmitter::push(thor::Particle particle) {
	particles.push_back(particle);
}

void SingleParticleEmitter::push(sf::Vector2f position, sf::Vector2f velocity,
		float lifetime, float rot, float rotsp) {
	thor::Particle particle(sf::seconds(lifetime));
	particle.position = position;
	particle.velocity = velocity;
	particle.rotation = rot;
	particle.rotationSpeed = rotsp;
	particle.scale = sf::Vector2f(1.f, 1.f);
	particle.color = sf::Color::White;
	particle.textureIndex = 0u;
	push(particle);
}
