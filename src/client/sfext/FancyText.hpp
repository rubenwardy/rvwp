#pragma once

#include <SFML/Graphics.hpp>
#include <memory>

#include "DropShadow.hpp"

namespace sfext {

class DropShadow;
class FancyText : public sf::Drawable, public sf::Transformable {
	std::unique_ptr<DropShadow> dropShadow;
	sf::Text text;

public:
	template <class... Args>
	FancyText(Args &&...args) : text(std::forward<Args>(args)...) {}

	~FancyText();

	void notifyChanged() { dropShadow = nullptr; }

	void setString(const sf::String &string) {
		text.setString(string);
		notifyChanged();
	}

	void setFillColor(sf::Color color) {
		text.setFillColor(color);
		notifyChanged();
	}

	void setCharacterSize(unsigned int size) {
		text.setCharacterSize(size);
		notifyChanged();
	}

	sf::Vector2f getSize() const {
		return {text.getGlobalBounds().width, text.getGlobalBounds().height};
	}

private:
	virtual void draw(
			sf::RenderTarget &target, sf::RenderStates states) const override;

	void regenerate();
};

} // namespace sfext
