#include "DropShadow.hpp"
#include <sanity.hpp>
#include <cmath>
#include <memory>

using namespace sfext;

namespace {
sf::Shader *shadowShader;

sf::Glsl::Vec4 convert255to100(const sf::Color &color) {
	return sf::Glsl::Vec4((float)color.r * 100.f / 255.f,
			(float)color.g * 100.f / 255.f, (float)color.b * 100.f / 255.f,
			(float)color.a * 100.f / 255.f);
}
} // namespace

void DropShadow::setShader(sf::Shader *v) {
	shadowShader = v;
}

void DropShadow::draw(
		sf::RenderTarget &target, sf::RenderStates baseStates) const {
	auto transformable = dynamic_cast<const sf::Transformable *>(&drawable);

	// Draw shadow
	{
		sf::Vector2f pos = transformable->getPosition() -
				transformable->getOrigin() + offset - sf::Vector2f(10, 10);
		pos = {std::round(pos.x), std::round(pos.y)};

		sf::RenderStates states = baseStates;
		sf::Sprite sprite(texture.getTexture());
		shadowShader->setUniform("mask", convert255to100(color));
		shadowShader->setUniform("direction", sf::Vector2f(0.f, pixelSize.y));
		states.shader = shadowShader;
		states.transform.translate(pos);
		target.draw(sprite, states);
	}

	// Draw drawable
	{
		sf::Vector2f pos =
				transformable->getPosition() - transformable->getOrigin();
		pos = {std::round(pos.x), std::round(pos.y)};

		target.draw(drawable, baseStates);
	}

	// LogF(ERROR, "drawing DropShadow - position: %f, %f. origin: %f, %f",
	// 		getPosition().x, getPosition().y, getOrigin().x, getOrigin().y);
}

void DropShadow::regenerate() {
	auto transformable = dynamic_cast<const sf::Transformable *>(&drawable);
	SanityCheck(transformable);

	sf::Vector2i textureSize = {int(size.x) + 20, int(size.y) + 20};

	pixelSize = {1.f / float(textureSize.x), 1.f / float(textureSize.y)};

	// Render font to a single texture
	sf::RenderTexture tmpTexture;
	{
		SanityCheck(tmpTexture.create(textureSize.x, textureSize.y));
		tmpTexture.clear(sf::Color::Transparent);

		sf::RenderStates states = sf::RenderStates::Default;
		states.transform = transformable->getInverseTransform();
		states.transform.translate({10, 10});
		tmpTexture.draw(drawable, states);
		tmpTexture.display();
	}

	// Render horizontal blur
	{
		SanityCheck(texture.create(textureSize.x, textureSize.y));
		texture.clear(sf::Color::Transparent);

		sf::Sprite sprite(tmpTexture.getTexture());

		sf::RenderStates states = sf::RenderStates::Default;
		shadowShader->setUniform("mask", sf::Glsl::Vec4(100, 100, 100, 100));
		shadowShader->setUniform("direction", sf::Vector2f(pixelSize.x, 0.f));
		states.shader = shadowShader;

		texture.draw(sprite, states);
		texture.display();
	}

	// LogF(ERROR,
	// 		"regnerating DropShadow - position: %f, %f origin %f, %f",
	// 		transformable->getPosition().x, transformable->getPosition().y,
	// 		transformable->getOrigin().x, transformable->getOrigin().y);
}
