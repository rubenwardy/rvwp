#include "EntityFootstepPlayer.hpp"
#include <algorithm>

using namespace client;

void EntityFootstepPlayer::update(float dtime) {
	elapsedTime += dtime;

	// Remove finished sounds
	playingSounds.erase(
			std::remove_if(playingSounds.begin(), playingSounds.end(),
					[](auto &sound) {
						return sound->getStatus() == sf::SoundSource::Stopped;
					}),
			playingSounds.end());

	auto &cam = camera->getCamera();
	const V3f &playerPosition = cam.pos;
	sf::Listener::setPosition(playerPosition + V3f(0, 0, cam.zoom * cam.zoom));

	for (auto entity : world->getEntityList()) {
		if (!entity->properties.footsteps) {
			continue;
		}

		auto it = entityTrackerInfo.find(entity->id);
		if (it == entityTrackerInfo.end()) {
			entityTrackerInfo[entity->id] = {entity->getPosition(), 0.f};
			continue;
		}

		auto &info = it->second;

		// Cull far away entities
		if (entity->getPosition().sqDistance(playerPosition) >= 16.f * 16.f) {
			info.lastPlayedPosition = entity->getPosition();
			info.lastPlayedTime = elapsedTime;
			continue;
		}

		// Check last played time and distance
		if (elapsedTime < info.lastPlayedTime + 0.3f ||
				entity->getPosition().sqDistance(info.lastPlayedPosition) <=
						0.7f) {
			continue;
		}

		playFootstep(entity->getPosition());

		info.lastPlayedPosition = entity->getPosition();
		info.lastPlayedTime = elapsedTime;
	}
}

void EntityFootstepPlayer::playFootstep(const V3f &pos) {
	content_id cid = world->getFloorTerrain(pos.floor());
	if (cid == 0) {
		return;
	}

	content::TileDef *def = definitionManager->getTileDef(cid);
	if (!def || def->footstepSound.empty()) {
		return;
	}

	auto source = resourceManager->getSoundOrLoad(def->footstepSound);
	if (source == nullptr) {
		return;
	}

	auto &sound = playingSounds.emplace_back(std::make_unique<sf::Sound>());
	sound->setBuffer(source->getRandom());
	sound->setPosition(pos);
	sound->setVolume(30);
	sound->play();
}
