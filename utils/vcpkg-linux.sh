#!/bin/bash

set -e


if [ ! -d "vcpkg" ]; then
	git clone https://github.com/microsoft/vcpkg
else
	pushd vcpkg
	git pull
	popd
fi

./vcpkg/bootstrap-vcpkg.sh

VCPKG_DEFAULT_TRIPLET=x64-linux ./vcpkg/vcpkg install

popd
