@echo off

git clone --depth 1 https://github.com/rubenwardy/vcpkg.git
cd vcpkg

call ./bootstrap-vcpkg.bat -disableMetrics
set VCPKG_DEFAULT_TRIPLET=x64-windows
.\vcpkg install sfml enet luajit tgui jsoncpp
.\vcpkg install thor --head
.\vcpkg integrate install
