#!/bin/bash

trap "trap - SIGTERM && kill -- -$$" SIGINT SIGTERM EXIT

killall rvwp
./bin/rvwp 0 &

inotifywait -e close_write,moved_to,create -m bin |
while read -r directory events filename; do
  if [ "$filename" = "a" ]; then
    killall a
    echo "========== DETECTED CHANGE =========="
    (./bin/rvwp server; echo $?) &
  fi
done
