#!/bin/bash

if [[ -z "${CLANG_TIDY}" ]]; then
	CLANG_TIDY=clang-tidy
fi

cmake . -DCMAKE_TOOLCHAIN_FILE=./vcpkg/scripts/buildsystems/vcpkg.cmake \
	-DCMAKE_BUILD_TYPE=Debug \
	-DCMAKE_EXPORT_COMPILE_COMMANDS=ON

echo "Performing clang-tidy checks..."
./utils/run-clang-tidy.py -clang-tidy-binary=${CLANG_TIDY} \
	-checks='-*,modernize-use-emplace,modernize-avoid-bind,performance-*' \
	-warnings-as-errors='-*,modernize-use-emplace,performance-type-promotion-in-math-fn,performance-faster-string-find,performance-implicit-cast-in-loop' \
	-extra-arg=-std=c++20 \
	-quiet \
	files 'src/.*'

RET=$?
echo "Clang tidy returned $RET"
exit $RET
