# Ruben's Virtual World Project

[![pipeline status](https://gitlab.com/rubenwardy/rvwp/badges/master/pipeline.svg)](https://gitlab.com/rubenwardy/rvwp/commits/master)
[![coverage report](https://gitlab.com/rubenwardy/rvwp/badges/master/coverage.svg)](https://gitlab.com/rubenwardy/rvwp/commits/master)
![All Rights Reserved](https://img.shields.io/badge/license-All%20Rights%20Reserved-red)

Hybrid topdown shooter and basebuilder, set in a modern dystopian environment

See [the website](https://rubenwardy.com/rvwp/) and [docs/](docs/) for more info.

License: All Rights Reserved.

## Requirements

* Linux or Windows, 64-bit.
* CMake 3+, VCPKG, and C++17.

## Usage

* [Compile RVWP](docs/dev/compiling.md).
* Open the client using `./bin/rvwp`.
* Start server using `./bin/rvwp server` or `./bin/server` (if built).
* Run tests using `./bin/rvwp tests` or `./bin/server tests`.
